import axios from 'axios';
import { prop } from 'ramda';

export const getPublishUrl = url => `${url}/api/charts`;
export const getChartUrl = url => id => `${url}/api/charts/${id}`;

const publish = globalConfig => ({ chart }) =>
  axios.post(getPublishUrl(globalConfig.shareServerUrl), chart).then(prop('data'));
const get = globalConfig => ({ id }) =>
  axios.get(getChartUrl(globalConfig.shareServerUrl)(id)).then(prop('data'));

export default ({ globalConfig }) => ({
  publish: publish(globalConfig),
  get: get(globalConfig),
});
