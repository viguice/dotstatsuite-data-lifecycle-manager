import { get } from 'axios';
import { prop } from 'ramda';

export const getDoitOfflineUrl = url => `${url}/api/doit_offline`;
export const getDoitOnlineUrl = url => `${url}/api/doit_online`;

const doitOffline = (globalConfig, bearer) => async () => {
  const headers = await bearer();
  return get(getDoitOfflineUrl(globalConfig.backendServerUrl), { headers }).then(prop('data'));
};

const doitOnline = (globalConfig, bearer) => async () => {
  const headers = await bearer();
  return get(getDoitOnlineUrl(globalConfig.backendServerUrl), { headers }).then(prop('data'));
};

export default ({ globalConfig, bearer }) => ({
  doit_offline: doitOffline(globalConfig, bearer),
  doit_online: doitOnline(globalConfig, bearer),
});
