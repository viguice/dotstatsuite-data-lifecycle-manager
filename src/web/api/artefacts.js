import fileSaver from 'file-saver';
import * as R from 'ramda';
import {
  getRequestArgs,
  parseArtefactRelatives,
  parseStubLists,
} from '@sis-cc/dotstatsuite-sdmxjs';
import { withAuthHeader } from '../modules/common/withauth';
import { getMsdReference, parseMsdReference } from '../sdmx-lib';
import apiManager from '../apiManager';

export const download = (xml, options) => {
  const { agencyId, code, version, withReferences } = options;
  let blob = new Blob([xml], { type: 'text/html' });
  const filename = `${agencyId}-${code}-${version}${
    withReferences === true ? '-all' : ''
  }.xml`;
  fileSaver.saveAs(blob, filename);
};

export const artefactExportStructure = (options = {}, withReferences) => {
  const { url, headers, params } = getRequestArgs({
    identifiers: R.pick(['agencyId', 'code', 'version'], options),
    datasource: R.assoc(
      'url',
      R.path(['space', 'endpoint'], options),
      options.space,
    ),
    type: options.type,
    withReferences,
    format: 'xml',
    withUrn: false,
  });
  return apiManager
    .get(url, { headers: withAuthHeader(options.space)(headers), params })
    .then(res => res.data)
    .then(structure => download(structure, { ...options, withReferences }));
};

export const requestArtefacts = ({ url, headers, params, parsing = {} }) => {
  return apiManager
    .get(url, { headers, params })
    .then(res => parseStubLists(res.data))
    .then(
      R.pipe(
        R.when(
          R.always(R.prop('fromCategory', parsing)),
          R.omit(['categoryscheme']),
        ),
        R.values,
        R.unnest,
        R.reduce((acc, artefact) => {
          const agencyIds = R.propOr([], 'agencyIds', parsing);
          if (
            !R.isEmpty(agencyIds) &&
            !R.includes(artefact.agencyId, agencyIds)
          ) {
            return acc;
          }
          const space = R.propOr({}, 'space', parsing);
          const id = `${space.id}:${artefact.type}:${artefact.sdmxId}`;
          return R.assoc(id, { id, space, ...artefact }, acc);
        }, {}),
      ),
    );
};

export const requestArtefactChildren = ({
  artefact,
  locale,
  keepArtefact = false,
}) => {
  const space = artefact.space;
  const artefactId = `${artefact.type}:${artefact.sdmxId}`;
  const { url, headers, params } = getRequestArgs({
    datasource: R.assoc('url', space.endpoint, space),
    identifiers: R.pick(['agencyId', 'code', 'version'], artefact),
    type: artefact.type,
    locale,
    overview: true,
    withChildren: true,
  });

  return apiManager
    .get(url, { headers: withAuthHeader(space)(headers), params })
    .then(res => res.data)
    .then(sdmxJson =>
      parseArtefactRelatives({
        sdmxJson,
        artefactId: keepArtefact ? null : artefactId,
      }),
    );
};

export const requestArtefactParents = ({ artefact, locale }) => {
  const space = artefact.space;
  const artefactId = `${artefact.type}:${artefact.sdmxId}`;
  const { url, headers, params } = getRequestArgs({
    datasource: R.assoc('url', space.endpoint, space),
    identifiers: R.pick(['agencyId', 'code', 'version'], artefact),
    type: artefact.type,
    locale,
    overview: true,
    withParents: true,
  });

  return apiManager
    .get(url, { headers: withAuthHeader(space)(headers), params })
    .then(res => res.data)
    .then(sdmxJson => parseArtefactRelatives({ sdmxJson, artefactId }));
};

export const requestLinkedDsds = ({ msd, locale }) => {
  const { url, headers, params } = getRequestArgs({
    datasource: { ...msd.space, url: msd.space.endpoint },
    identifiers: { agencyId: 'all', code: 'all', version: 'all' },
    type: 'datastructure',
    withReferences: false,
    locale,
    overview: true,
  });

  return apiManager
    .get(url, { headers: withAuthHeader(msd.space)(headers), params })
    .then(res => parseStubLists(res.data))
    .then(
      R.pipe(
        R.propOr([], 'datastructure'),
        R.filter(dsd => {
          const msdRef = getMsdReference(dsd.annotations || []);
          const targetMsd = parseMsdReference(msdRef || '');
          return targetMsd?.sdmxId === msd.sdmxId;
        }),
      ),
    );
};
