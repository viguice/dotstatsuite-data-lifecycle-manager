import { always, equals, hasPath, path, when } from 'ramda';
import apiManager from '../apiManager';
import { ERROR, SUCCESS } from '../modules/common/constants';

const getTargetType = referenceType => {
  switch (referenceType) {
    case 'datastructure':
      return 'dsd';
    case 'dataflow':
      return 'mappingsets';
    default:
      return null;
  }
};

export const dataCleanup = ({ transferUrl, dataspace, referenceId, referenceType }) => {
  const formData = new FormData();
  const type = when(equals('datastructure'), always('dsd'))(referenceType);
  formData.append('dataspace', dataspace);
  formData.append(type, referenceId);

  const targetType = getTargetType(referenceType);

  const url = `${transferUrl}/cleanup/${targetType}`;
  const axiosOptions = {
    url,
    method: 'DELETE',
    mode: 'cors',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
    },
    data: formData,
  };
  return apiManager(axiosOptions)
    .catch(error => {
      if (error.response) {
        return { type: ERROR, message: [path(['response', 'data', 'message'], error)] };
      }
      throw error;
    })
    .then(response => {
      if (response.type === ERROR) {
        return response;
      }
      if (hasPath(['data', 'success'], response)) {
        return { type: SUCCESS, message: [path(['data', 'message'], response)] };
      }
      throw Error(path(['data', 'message'], response));
    });
};
