import { SUCCESS, WARNING, ERROR } from '../modules/common/constants';
import { postStructureRequest } from '../sdmx-lib';
import { propOr } from 'ramda';

const fileReaderPromise = file => {
  const reader = new FileReader();
  reader.readAsText(file);

  return new Promise(
    resolve => (reader.onload = () => resolve(reader.result)),
    reject => (reader.onerror = error => reject(error)),
  );
};

export const uploadFromFile = (options = {}) => {
  return fileReaderPromise(options.file)
    .then(raw => postStructureRequest({ ...options, data: raw }))
    .then(log => {
      if (propOr(false, 'isError', log)) {
        return { type: ERROR, message: log.data };
      }
      if (propOr(false, 'isWarning', log)) {
        return { type: WARNING, message: log.data };
      }
      return { type: SUCCESS, message: log.data };
    })
    .catch(error => ({ type: ERROR, message: error.message }));
};
