import * as R from 'ramda';
import fileSaver from 'file-saver';
import apiManager from '../apiManager';
import { withAuthHeader } from '../modules/common/withauth';

const getTimeSelection = params => {
  const start = R.prop('startPeriod', params) || null;
  const end = R.prop('endPeriod', params) || null;
  const formatedStart = start ? `ge:${start}` : null;
  const formatedEnd = end ? `le:${end}` : null;
  const value =
    formatedStart && formatedEnd
      ? R.join('+', [formatedStart, formatedEnd])
      : formatedStart || formatedEnd;
  return value;
};

const parseDataquery = dq => {
  if (R.isNil(dq)) {
    return { dataquery: '' };
  }
  const splited = R.split('?', dq);
  const rawDQ = R.head(splited);
  const params = R.pipe(
    s => (R.length(s) > 1 ? R.last(s) : ''),
    R.split('&'),
    R.reduce((acc, entry) => {
      const parsed = R.split('=', entry);
      if (R.length(parsed) !== 2) {
        return acc;
      }
      return R.assoc(R.head(parsed), R.last(parsed), acc);
    }, {}),
  )(splited);
  const timeSelection = getTimeSelection(params);
  const dataquery = R.pipe(
    R.when(R.startsWith('/'), R.tail),
    R.split('.'),
    R.map(entry => (R.isEmpty(entry) ? '*' : entry)),
    R.join('.'),
    R.when(R.equals('all'), R.always('')),
  )(rawDQ);
  return { dataquery, timeSelection };
};

export const getMetadataRequestArgs = ({ dataquery, dataflow, asZip, asFile }) => {
  const customHeaders = R.propOr({}, 'headersv3', dataflow.space);
  const parsedDataquery = parseDataquery(dataquery);
  const format = R.pipe(
    R.pathOr('application/vnd.sdmx.data+csv;version=2.0', ['metadata', 'csv']),
    f => (asFile ? `${f};file=true` : f),
    f => (asZip ? `${f};zip=true` : f),
  )(customHeaders);
  const time = parsedDataquery.timeSelection
    ? { 'c[TIME_PERIOD]': parsedDataquery.timeSelection }
    : {};
  return {
    headers: withAuthHeader(dataflow.space)({ Accept: format }),
    url: `${dataflow.space.urlv3}/data/dataflow/${dataflow.agencyId}/${dataflow.code}/${dataflow.version}/${parsedDataquery.dataquery}`,
    params: { ...time, attributes: 'msd', measures: 'none' },
  };
};

export const requestMetadata = ({
  dataflow,
  filename,
  dataquery,
  asFile = true,
  asZip = false,
}) => {
  const { url, headers, params } = getMetadataRequestArgs({ dataflow, dataquery, asZip, asFile });
  return apiManager
    .get(url, { headers, params, ...(asZip ? { responseType: 'blob' } : {}) })
    .then(res =>
      asFile
        ? new File([res.data], filename, {
            type: asZip ? 'application/zip;charset=utf-8' : 'text/csv',
          })
        : res.data,
    );
};

export const exportMetadata = ({ dataflow, dataquery, filename }) => {
  return requestMetadata({ dataflow, dataquery, filename }).then(csv => {
    let blob = new Blob([csv], { type: 'text/csv' });
    fileSaver.saveAs(blob, filename);
  });
};
