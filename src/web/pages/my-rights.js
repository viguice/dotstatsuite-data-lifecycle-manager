import React from 'react';
import { UserRightsList } from '../components/user-rights/UserRightsList';

export default () => <UserRightsList queryKey="my-rights" />;
