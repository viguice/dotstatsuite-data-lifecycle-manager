import React from 'react';
import { compose, withProps } from 'recompose';
import { Classes } from '@blueprintjs/core';
import { FormattedMessage } from '../modules/i18n';
import numeral from 'numeral';
import { withUploadArtefact } from '../modules/upload-artefact';
import Upload from '../components/upload';

export default compose(
  withUploadArtefact,
  withProps(({ maxSize }) => ({
    scope: 'artefact',
    headerProps: {
      title: <FormattedMessage id="upload.artefact.title" />,
      iconName: 'folder-shared',
      description: (
        <em className={Classes.TEXT_MUTED}>
          <FormattedMessage
            id="upload.artefact.help"
            values={{ size: numeral(maxSize).format('0.00 b') }}
          />
        </em>
      ),
    },
  })),
)(Upload);
