import React from 'react';
import * as R from 'ramda';
import { Classes } from '@blueprintjs/core';
import { FormattedMessage } from '../modules/i18n';
import glamorous from 'glamorous';
import { withUploadData } from '../modules/upload-data';
import Upload from '../components/upload';
import messages from '../components/messages';
import { withDataValidation } from '../components/common/withDataValidation';
import { injectIntl } from 'react-intl';

const Wrapper = glamorous.div({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
});

const UploadData = props => (
  <Upload
    {...props}
    headerProps={{
      iconName: 'document-share',
      title: <FormattedMessage id="upload.data.title" />,
      description: (
        <Wrapper>
          <em className={Classes.TEXT_MUTED}>
            <FormattedMessage {...messages['upload.data.help.sdmx']} />
          </em>
        </Wrapper>
      ),
    }}
    scope="data"
    validation={R.dissoc('onChangeContentType', props.validation)}
  />
);

export default R.compose(
  injectIntl,
  withUploadData,
  withDataValidation,
)(UploadData);
