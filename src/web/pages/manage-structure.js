import React from 'react';
import { ArtefactsMain } from '../components/artefacts/paginated-selectable';
import Filters from '../components/filters';
import ContentWithSide, { Content, Side } from '../components/page/content-with-side';

const ManageStructure = () => {
  return (
    <ContentWithSide>
      <Side>
        <Filters />
      </Side>
      <Content>
        <ArtefactsMain />
      </Content>
    </ContentWithSide>
  );
};

ManageStructure.propTypes = {};

export default ManageStructure;
