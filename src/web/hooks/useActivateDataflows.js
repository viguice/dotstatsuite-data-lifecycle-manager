import { useMutation } from 'react-query';
import { useDispatch } from 'react-redux';
import * as R from 'ramda';
import { initDataflow } from '../api/initDataflow';
import { activatingDataflow, activatedDataflow } from '../modules/artefacts/action-creators';

export const useActivateDataflows = (artefacts, callback) => {
  const dispatch = useDispatch();
  const mutation = useMutation(
    dataflow => {
      dispatch(activatingDataflow(dataflow.id));
      return initDataflow(dataflow);
    },
    {
      onSuccess: log => {
        dispatch(activatedDataflow(log));
      },
    },
  );
  const areAllInternalDataflows = R.all(
    ({ type, space }) =>
      type === 'dataflow' && !R.isNil(space.transferUrl) && !R.isEmpty(space.transferUrl),
    artefacts,
  );

  if (!areAllInternalDataflows) {
    return { activate: null };
  }

  return {
    activate: dataflows => {
      R.forEach(dataflow => mutation.mutate(dataflow), dataflows);
      if (R.is(Function, callback)) {
        callback();
      }
    },
  };
};
