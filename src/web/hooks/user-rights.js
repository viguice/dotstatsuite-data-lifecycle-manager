import { useQueries, useMutation, useQueryClient } from 'react-query';
import { useSelector } from 'react-redux';
import * as R from 'ramda';
import { getUserRights, deleteUserRight, postUserRight } from '../api/user-rights';
import { artefactTypeIds } from '../lib/permissions';
import { getSpaces, getTypes } from '../modules/config';

const getAuthzs = spaces =>
  R.reduce(
    (acc, space) => {
      const authzServerUrl = R.prop('authzServerUrl', space);
      if (!authzServerUrl) {
        return acc;
      }
      return R.pipe(
        R.set(R.lensPath([authzServerUrl, 'url']), authzServerUrl),
        R.set(R.lensPath([authzServerUrl, 'spaces', space.id]), space),
      )(acc);
    },
    {},
    spaces,
  );

const refinePermissions = (authzs, types) =>
  R.filter(({ dataSpace, artefactType, authzServerUrl }) => {
    const spaces = R.path([authzServerUrl, 'spaces'], authzs);
    const isValidSpace = dataSpace === '*' || R.has(dataSpace, spaces);
    const type = R.nth(artefactType, artefactTypeIds);
    const isValidType = type === 'any' || R.has(type, types);
    return isValidSpace && isValidType;
  });

export const useGetUserRights = ({ queryKey }) => {
  const spaces = useSelector(getSpaces());
  const types = useSelector(getTypes());
  const authzs = getAuthzs(R.values(spaces));
  const onlyMine = R.equals('my-rights', queryKey);
  const results = useQueries(
    R.pipe(
      R.values,
      R.filter(authz => !R.isNil(authz.url)),
      R.map(({ url }) => ({
        queryKey: [queryKey, url],
        queryFn: () => getUserRights(url, onlyMine),
      })),
    )(authzs),
  );
  const isLoading = R.find(R.prop('isLoading'), results);
  const rights = R.pipe(
    R.pluck('data'),
    R.reject(R.isNil),
    R.unnest,
    refinePermissions(authzs, types),
  )(results);
  return { isEnabled: !R.isEmpty(authzs), isLoading, rights };
};

export const useDeleteUserRight = rights => {
  const queryClient = useQueryClient();
  const authzUrlsIndexedByRights = R.reduce(
    (acc, { id, authzServerUrl }) => R.assoc(id, authzServerUrl, acc),
    {},
    rights || [],
  );
  const mutation = useMutation(
    id => {
      const authzServerUrl = R.prop(id, authzUrlsIndexedByRights);
      return deleteUserRight(authzServerUrl, id);
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries(['all-rights']);
        queryClient.invalidateQueries(['my-rights']);
      },
    },
  );

  return mutation;
};

export const useSubmitUserRight = () => {
  const queryClient = useQueryClient();
  const mutation = useMutation(
    ({ authzServerUrl, permission }) => postUserRight(authzServerUrl, permission),
    {
      onSuccess: () => {
        queryClient.invalidateQueries(['all-rights']);
        queryClient.invalidateQueries(['my-rights']);
      },
    },
  );

  return mutation;
};
