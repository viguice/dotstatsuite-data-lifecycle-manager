import { format } from 'date-fns';
import * as R from 'ramda';
import { useDispatch, useSelector } from 'react-redux';
import { getSelectedInternalSpaces } from '../modules/filters/selectors';
import {
  getSubmissionPeriod,
  getStructureId,
  getRequestId,
  getExecutionOutcomeId,
  getExecutionStatusId,
  getSelectedUserEmails,
} from '../modules/logs/selectors';
import { useMemo } from 'react';
import { useQuery } from 'react-query';
import { getLogs } from '../api/transfer-logs';
import { setUserEmails } from '../modules/logs/action-creators';

export default () => {
  const dispatch = useDispatch();

  const spaces = useSelector(getSelectedInternalSpaces);
  const selectedUserEmails = useSelector(getSelectedUserEmails);
  const submissionPeriod = useSelector(getSubmissionPeriod);
  const structureId = useSelector(getStructureId);
  const requestId = useSelector(getRequestId);
  const executionOutcomeId = useSelector(getExecutionOutcomeId);
  const executionStatusId = useSelector(getExecutionStatusId);

  const submissionRequestArgs = R.ifElse(
    R.isNil,
    R.always({}),
    R.pipe(
      R.map(date => (R.isNil(date) ? null : format(date, 'dd-MM-yyyy HH:mm:ss'))),
      R.zipObj(['submissionStart', 'submissionEnd']),
    ),
  )(submissionPeriod);

  const requestArgs = {
    spaces,
    artefact: structureId,
    requestId,
    executionOutcomeId,
    executionStatusId,
    ...submissionRequestArgs,
  };

  const queryKey = `@@logs/transfer --> ${JSON.stringify(requestArgs)}`;
  const queryParams = {
    enabled: !R.isEmpty(spaces),
    onSuccess: data => {
      const dataEmails = R.pluck('userEmail', R.defaultTo(data));
      const emails = Array.from(selectedUserEmails);
      dispatch(setUserEmails(R.intersection(dataEmails, emails)));
    },
  };

  const { isLoading, data, error, isError, isRefetching, refetch } = useQuery(
    queryKey,
    () => getLogs({ requestArgs }),
    queryParams,
  );

  const logs = useMemo(() => {
    return R.isNil(data) ? [] : data;
  }, [data]);

  return { isLoading, isRefetching, logs, error, isError, refetch };
};
