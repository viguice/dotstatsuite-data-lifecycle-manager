import * as R from 'ramda';

export const artefactTypeIds = [
  'any',
  'agencyscheme',
  'agency',
  'dataproviderscheme',
  'dataprovider',
  'dataconsumerscheme',
  'dataconsumer',
  'organisationunitscheme',
  'organisationunit',
  'codelist',
  'code',
  'hierarchicalcodelist',
  'hierarchy',
  'hierarchicalcode',
  'categorisation',
  'categoryscheme',
  'category',
  'conceptscheme',
  'concept',
  'datastructure',
  'dataattribute',
  'attributedescriptor',
  'dataflow',
  'dimension',
  'group',
  'measuredimension',
  'timedimension',
  'metadatastructure',
  'reportstructure',
  'metadataattribute',
  'process',
  'processstep',
  'transition',
  'provisionagreement',
  'registration',
  'subscription',
  'attachmentconstraint',
  'contentconstraint',
  'structureset',
  'structuremap',
  'reportingtaxonomymap',
  'representationmap',
  'categorymap',
  'categoryschememap',
  'conceptschememap',
  'codemap',
  'codelistmap',
  'componentmap',
  'conceptmap',
  'organisationmap',
  'organisationschememap',
  'hybridcodelistmap',
  'hybridcode',
  'metadatatargetregion',
  'organisation',
  'organisationscheme',
  'primarymeasure',
];

export const permissions = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048];

export const artefactTypeRightsDefinition = R.addIndex(R.reduce)(
  (acc, type, index) => R.assoc(type, { id: index, type }, acc),
  {},
  artefactTypeIds,
);

const isArtefactScope = artefact =>
  R.allPass([
    R.anyPass([
      // DATASPACE
      R.propEq('dataSpace', '*'),
      R.propEq('dataSpace', R.path(['space', 'id'], artefact)),
    ]),
    R.anyPass([
      // TYPE
      R.propEq('artefactType', 0),
      R.pipe(
        R.prop('artefactType'),
        ind => R.nth(ind, artefactTypeIds),
        R.equals(R.prop('type', artefact)),
      ),
    ]),
    R.anyPass([
      // AGENCY
      R.propEq('artefactAgencyId', '*'),
      R.propEq('artefactAgencyId', R.prop('agencyId', artefact)),
    ]),
    R.anyPass([
      // CODE
      R.propEq('artefactId', '*'),
      R.propEq('artefactId', R.prop('code', artefact)),
    ]),
    R.anyPass([
      // VERSION
      R.propEq('artefactVersion', '*'),
      R.propEq('artefactVersion', R.prop('version', artefact)),
    ]),
  ]);

const getArtefactPermissions = artefact => userRights =>
  R.filter(isArtefactScope(artefact), userRights);

const hasPermission = permissionId => userRights =>
  R.pipe(
    R.find(R.propSatisfies(p => (p & permissionId) === permissionId, 'permission')),
    R.isNil,
    R.not,
  )(userRights);

export const hasDeleteArtefactPermission = hasPermission(512);

export const hasEditArtefactPermission = hasPermission(128);

export const hasManagePermission = hasPermission(64);

export const isArtefactDeleteAuthorized = (artefact, userRights) =>
  R.anyPass([
    R.anyPass([R.isNil, R.isEmpty]),
    R.pipe(getArtefactPermissions(artefact), hasDeleteArtefactPermission),
  ])(userRights);
