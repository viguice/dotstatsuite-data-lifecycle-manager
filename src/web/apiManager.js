import axios from 'axios';
import { getToken } from './modules/oidc/selectors';

let store;
export const injectStoreInApi = _store => {
  store = _store;
};

const apiManager = axios.create();

export const XNOAUTH = 'X-NO-AUTH';
export const XBASICAUTH = 'X-BASIC-AUTH';
const authHeaderInterceptor = config => {
  const xnoauth = config.headers[XNOAUTH];
  const xbasicauth = config.headers[XBASICAUTH];

  // remove tmp header to avoid cors issue with unknown field
  delete config.headers[XNOAUTH];
  delete config.headers[XBASICAUTH];

  if (xbasicauth) {
    config.headers['Authorization'] = xbasicauth;
    return config;
  }
  if (xnoauth) {
    return config;
  }

  const token = getToken(store.getState());
  config.headers['Authorization'] = `Bearer ${token}`;
  return config;
};

apiManager.interceptors.request.use(authHeaderInterceptor);

export default apiManager;
