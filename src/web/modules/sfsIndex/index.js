import reducerFactory from '../../store/reducer-factory';
import handlers from './action-creators';

export const sfsIndexReducer = reducerFactory({}, handlers);
export { default as sfsIndexSaga } from './saga';
