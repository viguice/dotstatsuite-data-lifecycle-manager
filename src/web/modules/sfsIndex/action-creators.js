export const SFS_INDEX = 'SFS_INDEX';
export const SFS_INDEX_SELECTION = 'SFS_INDEX_SELECTION';
export const SFS_INDEX_CALLBACK = 'SFS_INDEX_CALLBACK';

export const sfsIndex = id => ({
  type: SFS_INDEX,
  payload: { id },
});

export const sfsIndexSelection = ids => ({ type: SFS_INDEX_SELECTION, payload: { ids } });

export const sfsIndexCallback = (id, log) => ({
  type: SFS_INDEX_CALLBACK,
  payload: { id, log },
});

const sfsIndexHandler = (state, action) => ({
  ...state,
  [action.payload.id]: {
    id: action.payload.id,
    isIndexing: true,
    log: null,
  },
});

const sfsIndexCallbackHandler = (state, action) => ({
  ...state,
  [action.payload.id]: {
    id: action.payload.id,
    isIndexing: false,
    log: action.payload.log,
  },
});

export default {
  [SFS_INDEX]: sfsIndexHandler,
  [SFS_INDEX_CALLBACK]: sfsIndexCallbackHandler,
};
