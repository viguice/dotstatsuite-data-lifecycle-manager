import { createSelector } from 'reselect';
import * as R from 'ramda';

const getState = R.prop('oidc');
export const getUser = createSelector(getState, R.prop('user'));
export const getToken = createSelector(getState, R.prop('token'));
