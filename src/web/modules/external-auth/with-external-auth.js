import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { closeSpaceAuthMenu, setSpaceExtAuthOptions } from './action-creators';
import { getMenuSpaceId, getFailedSpaceIds } from './selectors';
import { getSpaces } from '../config/selectors';

export default Component => {
  const mapDispatchToProps = {
    closeSpaceAuthMenu,
    setSpaceExtAuthOptions,
  };

  const mapStateToProps = createStructuredSelector({
    menuSpaceId: getMenuSpaceId,
    failedSpaceIds: getFailedSpaceIds,
    spaces: getSpaces(),
  });

  return connect(mapStateToProps, mapDispatchToProps)(Component);
};
