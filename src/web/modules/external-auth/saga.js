import { put, select, takeEvery } from 'redux-saga/effects';
import * as R from 'ramda';
import { openSpaceAuthMenu, setSpaceExtAuthOptions } from './action-creators';
import { FILTERS_SPACE_TOGGLE } from '../filters/action-creators';
import { getSpaces } from '../config/selectors';

function* externalAuthWorker(action) {
  const spaceId = action.payload.id;
  const spaces = yield select(getSpaces());

  const space = R.prop(spaceId, spaces);
  if (!space.hasExternalAuth) {
    yield put(setSpaceExtAuthOptions(spaceId, null));
  } else if (!R.isNil(space.extAuthOptions)) {
    yield put(setSpaceExtAuthOptions(spaceId, space.extAuthOptions));
  } else {
    yield put(openSpaceAuthMenu(spaceId));
  }
}

export default function* saga() {
  yield takeEvery(FILTERS_SPACE_TOGGLE, externalAuthWorker);
}
