import { assoc, dissoc } from 'ramda';
import { USER_SIGNED_OUT } from '../oidc/action-creators';

export const model = {
  menuSpaceId: null,
  failedIds: {},
};

export const OPEN_SPACE_AUTH_MENU = 'OPEN_SPACE_AUTH_MENU';
export const openSpaceAuthMenu = spaceId => ({
  type: OPEN_SPACE_AUTH_MENU,
  payload: { spaceId },
});
export const openSpaceAuthMenuHandler = (state, action) => ({
  menuSpaceId: action.payload.spaceId,
});

export const CLOSE_SPACE_AUTH_MENU = 'CLOSE_SPACE_AUTH_MENU';
export const closeSpaceAuthMenu = id => ({
  type: CLOSE_SPACE_AUTH_MENU,
  payload: { id },
});
export const closeSpaceAuthMenuHandler = (state, action) => ({
  menuSpaceId: null,
  failedIds: dissoc(action.payload.id, state.failedIds),
});

export const SET_SPACE_EXT_AUTH_OPTIONS = 'SET_SPACE_EXT_AUTH_OPTIONS';
export const setSpaceExtAuthOptions = (spaceId, options) => ({
  type: SET_SPACE_EXT_AUTH_OPTIONS,
  payload: { spaceId, options },
});
export const setSpaceExtAuthOptionsHandler = (state, action) => ({
  menuSpaceId: null,
  failedIds: dissoc(action.payload.spaceId, state.failedIds),
});

export const SPACE_EXT_AUTH_FAILURE = 'SPACE_EXT_AUTH_FAILURE';
export const spaceExtAuthFailure = spaceId => ({
  type: SPACE_EXT_AUTH_FAILURE,
  payload: { spaceId },
});
export const spaceExtAuthFailureHandler = (state, action) => ({
  ...state,
  failedIds: assoc(action.payload.spaceId, action.payload.spaceId, state.failedIds),
});

export const RESET_SPACES_AUTH_OPTIONS = 'RESET_SPACES_AUTH_OPTIONS';
export const resetSpacesAuthOptions = { type: RESET_SPACES_AUTH_OPTIONS };
export const resetSpacesAuthOptionsHandler = () => model;

export default {
  [OPEN_SPACE_AUTH_MENU]: openSpaceAuthMenuHandler,
  [CLOSE_SPACE_AUTH_MENU]: closeSpaceAuthMenuHandler,
  [SET_SPACE_EXT_AUTH_OPTIONS]: setSpaceExtAuthOptionsHandler,
  [SPACE_EXT_AUTH_FAILURE]: spaceExtAuthFailureHandler,
  [USER_SIGNED_OUT]: () => model,
};
