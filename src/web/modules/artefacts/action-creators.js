import { MODULE } from './constants';
import { DELETE_RELATIVE_ARTEFACT_DONE } from '../relatives';
import { CHANGE_LOCALE } from '../i18n';
import { ERROR, SUCCESS } from '../common/constants';
import * as R from 'ramda';
import model from './model';
import { USER_SIGNED_OUT } from '../oidc/action-creators';

//--------------------------------------------------------------------------------------------------
export const ARTEFACT_FETCH = `${MODULE}_ARTEFACT_FETCH`;
export const artefactFetch = id => ({
  type: ARTEFACT_FETCH,
  payload: { id },
});
export const artefactFetchHandler = (state, action) => ({
  ...state,
  artefacts: {
    ...state.artefacts,
    [action.payload.id]: {
      ...state.artefacts[action.payload.id],
      isUpdating: true,
    },
  },
});

export const ARTEFACT_FETCH_SUCCESS = `${MODULE}_ARTEFACT_FETCH_SUCCESS`;
export const artefactFetchSuccess = (id, artefact) => ({
  type: ARTEFACT_FETCH_SUCCESS,
  payload: { id, artefact },
});
export const artefactFetchSuccessHandler = (state, action) => ({
  ...state,
  artefacts: {
    ...state.artefacts,
    [action.payload.id]: {
      ...state.artefacts[action.payload.id],
      isUpdating: false,
    },
    [action.payload.artefact.id]: {
      ...action.payload.artefact,
      isNew: true,
    },
  },
});

export const ARTEFACT_FETCH_ERROR = `${MODULE}_ARTEFACT_FETCH_ERROR`;
export const artefactFetchError = (id, error) => ({
  type: ARTEFACT_FETCH_ERROR,
  payload: { id, error },
});
export const artefactFetchErrorHandler = (state, action) => ({
  ...state,
  artefacts: {
    ...state.artefacts,
    [action.payload.id]: {
      ...state.artefacts[action.payload.id],
      error: { type: 'fetch', ...action.payload.error },
      isUpdating: false,
    },
  },
});

//--------------------------------------------------------------------------------------------------
export const ARTEFACTS_CLEAR = 'ARTEFACTS_CLEAR';
export const artefactsClear = () => ({ type: ARTEFACTS_CLEAR });
export const artefactsClearHandler = state => ({
  ...state,
  artefacts: {},
});

export const ARTEFACTS_FETCH = 'ARTEFACTS_FETCH';
export const artefactsFetch = () => ({ type: ARTEFACTS_FETCH });
export const artefactsFetchHandler = state => ({
  ...state,
  isFetching: true,
});

export const ARTEFACTS_FETCH_SUCCESS = 'ARTEFACTS_FETCH_SUCCESS';
export const artefactsFetchSuccess = artefacts => ({
  type: ARTEFACTS_FETCH_SUCCESS,
  payload: { artefacts },
});
export const artefactsFetchSuccessHandler = (state, action) => ({
  ...state,
  artefacts: {
    ...state.artefacts,
    ...action.payload.artefacts,
  },
});

export const ARTEFACTS_FETCH_ERROR = 'ARTEFACTS_FETCH_ERROR';
export const artefactsFetchError = error => ({
  type: ARTEFACTS_FETCH_ERROR,
  payload: { error },
});
export const artefactsFetchErrorHandler = (state, action) => ({
  ...state,
  error: action.payload.error,
});

export const ARTEFACTS_FETCH_DONE = 'ARTEFACTS_FETCH_DONE';
export const artefactsFetchDone = () => ({
  type: ARTEFACTS_FETCH_DONE,
});
export const artefactsFetchDoneHandler = state => ({
  ...state,
  isFetching: false,
});

//--------------------------------------------------------------------------------------------------
export const ARTEFACT_DELETE = `${MODULE}_ARTEFACT_DELETE`;
export const artefactDelete = id => ({ type: ARTEFACT_DELETE, payload: { id } });
export const artefactDeleteHandler = (state, action) => ({
  ...state,
  artefacts: {
    ...state.artefacts,
    [action.payload.id]: {
      ...state.artefacts[action.payload.id],
      isDeleting: true,
    },
  },
});

export const ARTEFACT_DELETE_SUCCESS = `${MODULE}_ARTEFACT_DELETE_SUCCESS`;
export const artefactDeleteSuccess = (id, message, space) => ({
  type: ARTEFACT_DELETE_SUCCESS,
  payload: { id, message, space },
});
export const artefactDeleteSuccessHandler = (state, action) => ({
  ...state,
  artefacts: R.omit([action.payload.id], state.artefacts),
  deleteLog: R.assoc(action.payload.id, {
    id: action.payload.id,
    type: SUCCESS,
    message: action.payload.message,
    space: action.payload.space,
  })(state.deleteLog),
});

export const CHANGE_SPOTLIGHT = `${MODULE}_CHANGE_SPOTLIGHT`;
export const changeSpotlight = spotlight => ({ type: CHANGE_SPOTLIGHT, payload: { spotlight } });
export const changeSpotlightHandler = (state, action) => ({
  ...state,
  spotlight: {
    ...state.spotlight,
    ...action.payload.spotlight,
  },
});
export const resetSpotlightHandler = state => ({
  ...state,
  spotlight: {},
});
export const RESET_DELETE_LOG = `${MODULE}_RESET_DELETE_LOG`;
export const resetLog = id => ({ type: RESET_DELETE_LOG, payload: { id } });
export const resetLogHandler = (state, action) => ({
  ...state,
  deleteLog: R.dissoc(action.payload.id, state.deleteLog),
});

export const ARTEFACT_DELETE_ERROR = `${MODULE}_ARTEFACT_DELETE_ERROR`;
export const artefactDeleteError = (id, error) => ({
  type: ARTEFACT_DELETE_ERROR,
  payload: { id, error },
});
export const artefactDeleteErrorHandler = (state, action) => ({
  ...state,
  artefacts: R.set(R.lensPath([action.payload.id, 'isDeleting']), false, state.artefacts),
  deleteLog: {
    ...state.deleteLog,
    [action.payload.id]: { id: action.payload.id, ...action.payload.error, type: ERROR },
  },
});
export const ARTEFACT_CLEANUP = `${MODULE}_ATREFACT_CLEANUP`;
export const artefactCleanUp = ({ id, message, space, type }) => ({
  type: ARTEFACT_CLEANUP,
  payload: { id, message, space, type },
});
export const artefactCleanUpHandler = (state, action) => ({
  ...state,
  deleteLog: {
    ...state.deleteLog,
    [action.payload.id]: action.payload,
  },
});

//--------------------------------------------------------------------------------------------------
export const ARTEFACT_EXPORT_STRUCTURE = `${MODULE}_ARTEFACT_EXPORT_STRUCTURE`;
export const artefactExportStructure = (id, withReferences) => ({
  type: ARTEFACT_EXPORT_STRUCTURE,
  payload: { id, withReferences },
});
export const artefactExportStructureHandler = (state, action) => ({
  ...state,
  artefacts: {
    ...state.artefacts,
    [action.payload.id]: {
      ...state.artefacts[action.payload.id],
      isExporting: true,
    },
  },
});

export const ARTEFACT_EXPORT_STRUCTURE_SUCCESS = `${MODULE}_ARTEFACT_EXPORT_STRUCTURE_SUCCESS`;
export const artefactExportStructureSuccess = id => ({
  type: ARTEFACT_EXPORT_STRUCTURE_SUCCESS,
  payload: { id },
});
export const artefactExportStructureSuccessHandler = (state, action) => ({
  ...state,
  artefacts: {
    ...state.artefacts,
    [action.payload.id]: {
      ...state.artefacts[action.payload.id],
      isExporting: false,
    },
  },
});

export const ARTEFACT_EXPORT_STRUCTURE_ERROR = `${MODULE}_ARTEFACT_EXPORT_STRUCTURE_ERROR`;
export const artefactExportStructureError = (id, error) => ({
  type: ARTEFACT_EXPORT_STRUCTURE_ERROR,
  payload: { id, error },
});
export const artefactExportStructureErrorHandler = (state, action) =>
  R.over(
    R.lensPath(['artefacts', action.payload.id]),
    R.pipe(
      R.assocPath(['error', 'exportArtefact'], { type: 'ERROR', message: action.payload.error }),
      R.assoc('isExporting', false),
    ),
    state,
  );

//--------------------------------------------------------------------------------------------------

export const REFRESH_ARTEFACTS = `${MODULE}_REFRESH_ARTEFACTS`;
export const REFRESH_ARTEFACT = `${MODULE}_REFRESH_ARTEFACT`;

export const relativeArtefactDeleteHandler = (state, action) => {
  const { id, log } = action.payload;
  if (log.type === SUCCESS) {
    return R.over(R.lensProp('artefacts'), R.dissoc(id), state);
  }
  return state;
};

export const ACTIVATING_DATAFLOW = `${MODULE}_ACTIVATING_DATAFLOW`;
export const ACTIVATED_DATAFLOW = `${MODULE}_ACTIVATED_DATAFLOW`;

export const activatingDataflow = id => ({
  type: ACTIVATING_DATAFLOW,
  payload: { id },
});

export const activatedDataflow = log => ({
  type: ACTIVATED_DATAFLOW,
  payload: { id: log.artefactId, log },
});

const activatingDataflowHandler = (state, action) => ({
  ...state,
  artefacts: R.over(R.lensProp(action.payload.id), R.assoc('isActivating', true))(state.artefacts),
});

const activatedDataflowHandler = (state, action) => ({
  ...state,
  artefacts: R.over(
    R.lensProp(action.payload.id),
    R.pipe(R.assoc('isActivating', false), R.assoc('activateLog', action.payload.log)),
  )(state.artefacts),
});

export default {
  [ARTEFACTS_FETCH]: artefactsFetchHandler,
  [ARTEFACTS_FETCH_SUCCESS]: artefactsFetchSuccessHandler,
  [ARTEFACTS_FETCH_ERROR]: artefactsFetchErrorHandler,
  [ARTEFACTS_FETCH_DONE]: artefactsFetchDoneHandler,
  [ARTEFACTS_CLEAR]: artefactsClearHandler,
  [ARTEFACT_FETCH]: artefactFetchHandler,
  [ARTEFACT_FETCH_SUCCESS]: artefactFetchSuccessHandler,
  [ARTEFACT_FETCH_ERROR]: artefactFetchErrorHandler,
  [ARTEFACT_DELETE]: artefactDeleteHandler,
  [ARTEFACT_DELETE_SUCCESS]: artefactDeleteSuccessHandler,
  [ARTEFACT_DELETE_ERROR]: artefactDeleteErrorHandler,
  [ARTEFACT_EXPORT_STRUCTURE]: artefactExportStructureHandler,
  [ARTEFACT_EXPORT_STRUCTURE_SUCCESS]: artefactExportStructureSuccessHandler,
  [ARTEFACT_EXPORT_STRUCTURE_ERROR]: artefactExportStructureErrorHandler,
  [RESET_DELETE_LOG]: resetLogHandler,
  [ARTEFACT_CLEANUP]: artefactCleanUpHandler,
  [DELETE_RELATIVE_ARTEFACT_DONE]: relativeArtefactDeleteHandler,
  [CHANGE_SPOTLIGHT]: changeSpotlightHandler,
  [CHANGE_LOCALE]: resetSpotlightHandler,
  [USER_SIGNED_OUT]: () => model(),
  [ACTIVATING_DATAFLOW]: activatingDataflowHandler,
  [ACTIVATED_DATAFLOW]: activatedDataflowHandler,
};
