// export function: model can be configurable and won't be modified if recalled
const spotlightFields = {
  label: { id: 'label', accessor: 'label', isSelected: true },
  code: { id: 'code', accessor: 'code', isSelected: true },
};

export default () => ({
  // available artefacts => { artefactId: { id, label, type } }
  artefacts: {},

  // error about artefacts
  error: null,

  // fetch in progress about artefacts
  isFetching: false,

  // after a successful delete, artefact is removed from the state, store the feedback message here
  deleteLog: null,

  // spotlight artefact to filter results
  spotlight: { value: '', fields: spotlightFields },
});
