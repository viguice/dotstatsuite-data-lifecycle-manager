import { createSelector } from 'reselect';
import { pickBy, prop, values } from 'ramda';
import { getIsFinal } from '../filters';

const getState = state => state.artefacts;

export const getArtefacts = () => createSelector(getState, state => state.artefacts);
export const getArtefact = id => createSelector([getArtefacts()], artefacts => prop(id, artefacts));
export const getIsFetching = () => createSelector(getState, state => state.isFetching);
export const getSpotlight = createSelector(getState, state => state.spotlight);

export const getFilteredArtefacts = () =>
  createSelector([getArtefacts(), getIsFinal()], (artefacts, isFinal) =>
    pickBy(artefact => !isFinal || artefact.isFinal, artefacts),
  );

export const getDeleteLog = () => createSelector(getState, state => values(state.deleteLog || {}));
