import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { getFilteredArtefacts, getIsFetching, getDeleteLog, getSpotlight } from './selectors';
import {
  artefactDelete,
  artefactExportStructure,
  resetLog,
  changeSpotlight,
} from './action-creators';

export default Component => {
  const mapDispatchToProps = {
    artefactDelete,
    artefactExportStructure,
    resetLog,
    changeSpotlight,
  };

  const mapStateToProps = createStructuredSelector({
    artefacts: getFilteredArtefacts(),
    deleteLog: getDeleteLog(),
    isFetching: getIsFetching(),
    spotlight: getSpotlight,
  });

  return connect(mapStateToProps, mapDispatchToProps)(Component);
};
