import { pick } from 'ramda';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

export default (actions, selectors) => id => Component => {
  const mapDispatchToProps = pick(['transfer'], actions);

  const mapStateToProps = createStructuredSelector({
    isTransfering: selectors.getIsTransfering(id),
    isUpdating: selectors.getIsUpdating(id),
    log: selectors.getLog(id),
    destinationSpace: selectors.getSpace(id),
    destinationSpaces: selectors.getDestinationSpaces(id),
    sourceSpace: selectors.getSourceSpace(id),
  });

  return connect(mapStateToProps, mapDispatchToProps)(Component);
};
