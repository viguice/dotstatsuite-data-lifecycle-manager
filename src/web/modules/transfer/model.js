// export function: model can be configurable and won't be modified if recalled
export default () => ({
  // {artefactId: transfer()}
});

export const transferFactory = (id, space) => ({
  // key redundancy
  id,

  // destination space => {id, type, label, endpoint, color}
  space,

  // transfering status
  isTransfering: false,

  // parsed response of the WS => {type, message, error}
  log: null,
});
