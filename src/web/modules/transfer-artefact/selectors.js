import { createSelector } from 'reselect';
import { prop } from 'ramda';
import { getArtefact, getArtefactOptions } from '../common/selectors';

export default selectors => {
  return {
    ...selectors,
    getArtefactOptions,
    getIsUpdating: id =>
      createSelector([getArtefact(id)], artefact => prop('isUpdating', artefact)),
  };
};
