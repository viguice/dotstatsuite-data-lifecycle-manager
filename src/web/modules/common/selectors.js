import { createSelector } from 'reselect';
import { prop } from 'ramda';

// temporary avoiding circular dependency issue
const getArtefactsState = state => state.artefacts;
const getArtefacts = () => createSelector(getArtefactsState, state => state.artefacts);
export const getArtefact = id => createSelector([getArtefacts()], artefacts => prop(id, artefacts));
export const getArtefactOptions = id =>
  createSelector([getArtefact(id)], artefact => ({
    agencyId: artefact.agencyId,
    type: artefact.type,
    version: artefact.version,
    code: artefact.code,
    sdmxId: artefact.sdmxId,
    space: artefact.space,
  }));
const getTransfer = id => state => state.transferArtefact[id];
export const getTransferArtefactOptions = id =>
  createSelector([getArtefact(id), getTransfer(id)], (artefact, transfer) => {
    return {
      agencyId: artefact.agencyId,
      type: artefact.type,
      version: artefact.version,
      code: artefact.code,
      space: transfer.space,
    };
  });
// temporary avoiding circular dependency issue
