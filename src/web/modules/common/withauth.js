import * as R from 'ramda';
import { XBASICAUTH, XNOAUTH } from '../../apiManager';

// inject X-NO-AUTH to let the interceptor know if the Bearer token should be used
// inject X-BASIC-AUTH to the interceptor know if the Bearer token should be used
// note: no transfer url means external space
export const withAuthHeader = space => headers => {
  if (
    space.hasExternalAuth &&
    !R.isEmpty(space.extAuthOptions || {}) &&
    !R.path(['extAuthOptions', 'isAnonymous'], space)
  ) {
    const credentials = btoa(`${space.extAuthOptions.user}: ${space.extAuthOptions.password}`);
    return R.assoc(XBASICAUTH, `Basic ${credentials}`, headers);
  }
  const transferUrl = space?.transferUrl;
  const authenticateToRemoteURL = space?.authenticateToRemoteURL;
  return R.assoc(XNOAUTH, R.isNil(transferUrl) && !authenticateToRemoteURL, headers);
};
