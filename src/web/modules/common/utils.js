import { ascend, isEmpty, map, prop, sortWith, values } from 'ramda';

export const withSort = fields => spaces =>
  sortWith(
    isEmpty(fields)
      ? [ascend(prop('order')), ascend(prop('label'))]
      : map(field => ascend(prop(field)), fields),
    values(spaces),
  );
