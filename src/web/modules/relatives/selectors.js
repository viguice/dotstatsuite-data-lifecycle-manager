import { createSelector } from 'reselect';
import * as R from 'ramda';

const getState = R.prop('relatives');

export const getIsOpenRelatives = createSelector(
  getState,
  R.complement(R.isEmpty),
);

export const getMode = createSelector(getState, R.propOr(null, 'mode'));

export const getIsFetching = createSelector(getState, R.prop('isFetching'));

const _getArtefact = createSelector(getState, R.prop('artefact'));

const _getAncestors = createSelector(getState, R.propOr([], 'ancestors'));

const _getDescendants = createSelector(
  getState,
  R.pipe(R.propOr([], 'descendants'), artefacts => {
    const artefactIds = R.pluck('id', artefacts);
    return R.map(artefact => {
      const [parentsInTree, otherParents] = R.pipe(
        R.propOr([], 'sdmxParents'),
        R.partition(parent => R.includes(parent.id, artefactIds)),
      )(artefact);

      return {
        ...artefact,
        sdmxParents: otherParents,
        parentsInTree,
      };
    }, artefacts);
  }),
);

export const getSelection = createSelector(getState, R.propOr({}, 'selection'));

const getDeleteLogs = createSelector(getState, R.propOr({}, 'deleteLogs'));

const getCleanUpLogs = createSelector(getState, R.propOr({}, 'cleanUpLogs'));

export const getDeletingIds = createSelector(
  getState,
  R.propOr({}, 'deleting'),
);

const injectSelectedState = selection => artefact =>
  R.when(
    R.complement(R.isNil),
    R.always({
      ...artefact,
      isSelected: R.has(R.prop('id', artefact), selection),
      parentsInTree: R.reject(
        parent => R.has(parent.id, selection),
        R.propOr([], 'parentsInTree', artefact),
      ),
    }),
  )(artefact);

const injectDeleteLog = logs => artefact =>
  R.when(
    R.complement(R.isNil),
    R.assoc(
      'deleteLog',
      R.pipe(R.prop(R.prop('id', artefact)), log =>
        R.when(
          R.complement(R.isNil),
          R.assoc(
            'key',
            `artefact.delete.${R.toLower(R.propOr('', 'type', log))}`,
          ),
        )(log),
      )(logs),
    ),
  )(artefact);

const injectCleanUpLog = logs => artefact =>
  R.when(
    R.complement(R.isNil),
    R.assoc(
      'cleanUpLog',
      R.pipe(R.prop(R.prop('id', artefact)), log =>
        R.when(
          R.complement(R.isNil),
          R.assoc(
            'key',
            `artefact.cleanup.${R.toLower(R.propOr('', 'type', log))}`,
          ),
        )(log),
      )(logs),
    ),
  )(artefact);

const injectIsSelectable = mode => artefact => {
  return R.when(
    R.complement(R.isNil),
    R.assoc(
      'isSelectable',
      R.equals('delete', mode) &&
        !R.propOr(false, 'isError', artefact) &&
        R.isEmpty(R.propOr([], 'sdmxParents', artefact)) &&
        !R.propOr(false, 'isMsdLinked', artefact) &&
        R.isEmpty(R.propOr([], 'parentsInTree', artefact)) &&
        R.isNil(R.prop('deleteLog', artefact)),
    ),
  )(artefact);
};

const injectIsDeleting = deletingIds => artefact =>
  R.when(
    R.complement(R.isNil),
    R.assoc('isDeleting', R.has(R.prop('id', artefact), deletingIds)),
  )(artefact);

const getArtefact = createSelector(
  _getArtefact,
  getSelection,
  getDeleteLogs,
  getCleanUpLogs,
  getDeletingIds,
  getMode,
  (artefact, selection, deleteLogs, cleanUpLogs, deletingIds, mode) =>
    R.pipe(
      injectSelectedState(selection),
      injectDeleteLog(deleteLogs),
      injectCleanUpLog(cleanUpLogs),
      injectIsDeleting(deletingIds),
      injectIsSelectable(mode),
    )(artefact),
);

const getAncestors = createSelector(
  _getAncestors,
  getSelection,
  getDeleteLogs,
  getCleanUpLogs,
  getDeletingIds,
  getMode,
  (ancestors, selection, deleteLogs, cleanUpLogs, deletingIds, mode) =>
    R.map(
      R.pipe(
        injectSelectedState(selection),
        injectDeleteLog(deleteLogs),
        injectCleanUpLog(cleanUpLogs),
        injectIsDeleting(deletingIds),
        injectIsSelectable(mode, selection),
      ),
      ancestors,
    ),
);

const getDescendants = createSelector(
  _getDescendants,
  getSelection,
  getDeleteLogs,
  getCleanUpLogs,
  getDeletingIds,
  getMode,
  (descendants, selection, deleteLogs, cleanUpLogs, deletingIds, mode) =>
    R.map(
      R.pipe(
        injectSelectedState(selection),
        injectDeleteLog(deleteLogs),
        injectCleanUpLog(cleanUpLogs),
        injectIsDeleting(deletingIds),
        injectIsSelectable(mode, selection),
      ),
      descendants,
    ),
);

export const getIsError = createSelector(getState, state =>
  R.complement(R.isNil)(state.error),
);

export const getErrors = createSelector(getState, R.prop('errors'));

const getTreeChildren = (parentId, groupedChildren) =>
  R.pipe(
    R.propOr([], parentId),
    R.map(artefact => {
      const children = getTreeChildren(artefact.id, groupedChildren);
      const nonSelectableChild = R.find(child => !child.isSelectable, children);
      return {
        ...artefact,
        children,
        isSelectable: R.isNil(nonSelectableChild)
          ? artefact.isSelectable
          : false,
        nonSelectableChild: R.isNil(nonSelectableChild)
          ? undefined
          : nonSelectableChild.id,
      };
    }),
  )(groupedChildren);

const addParentsToTree = (tree, groupedParents) => {
  const child = R.head(tree);
  const childId = child.id;
  const parents = R.propOr([], childId, groupedParents);
  if (R.isEmpty(parents)) {
    return tree;
  }
  const isChildSelectable = child.isSelectable;
  let _parents = parents;
  if (!isChildSelectable) {
    _parents = R.map(
      parent => ({
        ...parent,
        isSelectable: false,
        nonSelectableChild: childId,
      }),
      parents,
    );
  }
  const enhancedTree = R.over(
    R.lensIndex(-1),
    R.assoc('children', tree),
  )(_parents);

  return addParentsToTree(enhancedTree, groupedParents);
};

export const getArtefactRelativesTree = createSelector(
  getArtefact,
  getAncestors,
  getDescendants,
  (artefact, ancestors, descendants) => {
    if (R.any(R.isNil)([artefact, ancestors, descendants])) {
      return null;
    }
    const groupedAncestors = {
      ...R.groupBy(R.prop('childId'), ancestors),
      '#ROOT': [{ ...artefact, isMainArtefact: true }],
    };
    const groupedDescendants = R.groupBy(R.prop('parentId'), descendants);
    const ancestorsTree = getTreeChildren('#ROOT', groupedAncestors);

    return addParentsToTree(ancestorsTree, groupedDescendants);
  },
);

export const getGroupedSelectedArtefacts = createSelector(
  getArtefact,
  getAncestors,
  getDescendants,
  (artefact, ancestors, descendants) =>
    R.pipe(
      R.unnest,
      R.filter(R.prop('isSelected')),
      R.groupBy(R.prop('depth')),
    )([ancestors, [{ ...artefact, depth: 0 }], descendants]),
);
