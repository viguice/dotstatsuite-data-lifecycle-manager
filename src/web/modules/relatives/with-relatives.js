import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  getArtefactRelativesTree,
  getDeletingIds,
  getIsFetching,
  getIsOpenRelatives,
  getIsError,
  getMode,
  getSelection,
} from './selectors';
import {
  cleanArtefactRelatives,
  deleteRelativesArtefacts,
  deselectArtefacts,
  cancelRelativesDelete,
  selectArtefacts,
} from './action-creators';

export default Component => {
  const mapDispatchToProps = {
    cleanArtefactRelatives,
    deleteRelativesArtefacts,
    deselectArtefacts,
    cancelRelativesDelete,
    selectArtefacts,
  };

  const mapStateToProps = createStructuredSelector({
    artefactsTree: getArtefactRelativesTree,
    deletingIds: getDeletingIds,
    isFetching: getIsFetching,
    isOpenRelatives: getIsOpenRelatives,
    isError: getIsError,
    mode: getMode,
    selection: getSelection,
  });

  return connect(mapStateToProps, mapDispatchToProps)(Component);
};
