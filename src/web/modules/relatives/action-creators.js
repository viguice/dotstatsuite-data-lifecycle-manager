import * as R from 'ramda';
import model from './model';
import { USER_SIGNED_OUT } from '../oidc/action-creators';

export const GET_ARTEFACT_RELATIVES = 'GET_ARTEFACT_RELATIVES';
export const GET_ARTEFACT_RELATIVES_SUCCESS = 'GET_ARTEFACT_RELATIVES_SUCCESS';
export const GET_ARTEFACT_RELATIVES_ERROR = 'GET_ARTEFACT_RELATIVES_ERROR';
export const GET_ARTEFACT_CHILDREN_ERROR = 'GET_ARTEFACT_CHILDREN_ERROR';
export const GET_ARTEFACT_PARENTS_ERROR = 'GET_ARTEFACT_PARENTS_ERROR';
export const PARSE_MSD_REFERENCE_ERROR = 'GET_ARTEFACT_PARENTS_ERROR';
export const CLEAN_ARTEFACT_RELATIVES = 'DELETE_ARTEFACT_RELATIVES';

export const getArtefactRelatives = ({ artefactId, mode }) => ({
  type: GET_ARTEFACT_RELATIVES,
  payload: { artefactId, mode },
});
export const getArtefactRelativesSuccess = relatives => ({
  type: GET_ARTEFACT_RELATIVES_SUCCESS,
  payload: relatives,
});
export const getArtefactRelativesError = error => ({
  type: GET_ARTEFACT_RELATIVES_ERROR,
  payload: { error },
});
export const getArtefactChildrenError = artefact => ({
  type: GET_ARTEFACT_CHILDREN_ERROR,
  payload: { artefact },
});
export const getArtefactParentsError = artefact => ({
  type: GET_ARTEFACT_PARENTS_ERROR,
  payload: { artefact },
});
export const parseMsdReferenceError = (artefact, msdRef) => ({
  type: PARSE_MSD_REFERENCE_ERROR,
  payload: { artefact, msdRef },
});
export const cleanArtefactRelatives = () => ({
  type: CLEAN_ARTEFACT_RELATIVES,
});

export const getArtefactRelativesHandler = (state, action) => ({
  ...state,
  mode: action.payload.mode,
  isFetching: true,
});

export const getArtefactRelativesSuccessHandler = (state, action) => ({
  ...state,
  isFetching: false,
  artefact: action.payload.artefact,
  ancestors: action.payload.ancestors,
  descendants: action.payload.descendants,
  selection: action.payload.selection,
});

export const getArtefactRelativesErrorHandler = (state, action) => ({
  ...state,
  errors: R.append(action.payload.error, state.errors || []),
  isFetching: false,
});
export const getArtefactParentsErrorHandler = (state, action) => ({
  ...state,
  errors: R.append(
    { artefact: action.payload.artefact, operation: 'getParents' },
    state.errors || [],
  ),
});
export const getArtefactChildrenErrorHandler = (state, action) => ({
  ...state,
  errors: R.append(
    { artefact: action.payload.artefact, operation: 'getChildren' },
    state.errors || [],
  ),
});
export const parseMsdReferenceErrorHandler = (state, action) => ({
  ...state,
  errors: R.append(
    {
      artefact: action.payload.artefact,
      msdRef: action.payload.msdRef,
      operation: 'parseMsdReference',
    },
    state.errors || [],
  ),
});

export const cleanArtefactRelativesHandler = () => model();

export const SELECT_ARTEFACT_RELATIVES = 'SELECT_ARTEFACT_RELATIVES';
export const DESELECT_ARTEFACT_RELATIVES = 'DESELECT_ARTEFACT_RELATIVES';

export const selectArtefacts = ids => ({
  type: SELECT_ARTEFACT_RELATIVES,
  payload: { ids },
});

export const deselectArtefacts = ids => ({
  type: DESELECT_ARTEFACT_RELATIVES,
  payload: { ids },
});

export const selectArtefactsHandler = (state, action) =>
  R.over(
    R.lensProp('selection'),
    R.merge(R.indexBy(R.identity, action.payload.ids)),
  )(state);

export const deselectArtefactsHandler = (state, action) =>
  R.over(R.lensProp('selection'), R.omit(action.payload.ids))(state);

export const DELETE_RELATIVES_ARTEFACTS = 'DELETE_RELATIVES_ARTEFACTS';
export const deleteRelativesArtefacts = () => ({
  type: DELETE_RELATIVES_ARTEFACTS,
});

export const CANCEL_RELATIVES_DELETE = 'CANCEL_RELATIVES_DELETE';
export const cancelRelativesDelete = () => ({ type: CANCEL_RELATIVES_DELETE });
export const cancelRelativesDeleteHandler = state => ({
  ...state,
  deleting: {},
  selection: {},
});

export const DELETE_RELATIVE_ARTEFACT = 'DELETE_RELATIVE_ARTEFACT';
export const deleteRelativeArtefact = id => ({
  type: DELETE_RELATIVE_ARTEFACT,
  payload: { id },
});
export const deleteRelativeArtefactHandler = (state, action) => ({
  ...state,
  deleting: R.assoc(
    action.payload.id,
    action.payload.id,
    R.propOr({}, 'deleting', state),
  ),
});

export const DELETE_RELATIVE_ARTEFACT_DONE = 'DELETE_RELATIVE_ARTEFACT_DONE';
export const deleteRelativeArtefactDone = ({ id, log }) => ({
  type: DELETE_RELATIVE_ARTEFACT_DONE,
  payload: { id, log },
});
export const deleteRelativeArtefactDoneHandler = (state, action) => ({
  ...state,
  deleting: R.dissoc(action.payload.id, R.propOr({}, 'deleting', state)),
  selection: R.dissoc(action.payload.id, R.propOr({}, 'selection', state)),
  deleteLogs: R.assoc(
    action.payload.id,
    action.payload.log,
    R.propOr({}, 'deleteLogs', state),
  ),
});

export const CLEANUP_RELATIVE_ARTEFACT = 'CLEANUP_RELATIVES_ARTEFACT';
export const cleanUpRelativeArtefact = ({ id, log }) => ({
  type: CLEANUP_RELATIVE_ARTEFACT,
  payload: { id, log },
});
export const cleanUpRelativeArtefactHandler = (state, action) => ({
  ...state,
  cleanUpLogs: R.assoc(
    action.payload.id,
    action.payload.log,
    R.propOr({}, 'cleanUpLogs', state),
  ),
});

export default {
  [GET_ARTEFACT_RELATIVES]: getArtefactRelativesHandler,
  [GET_ARTEFACT_RELATIVES_SUCCESS]: getArtefactRelativesSuccessHandler,
  [GET_ARTEFACT_RELATIVES_ERROR]: getArtefactRelativesErrorHandler,
  [CLEAN_ARTEFACT_RELATIVES]: cleanArtefactRelativesHandler,
  [SELECT_ARTEFACT_RELATIVES]: selectArtefactsHandler,
  [DESELECT_ARTEFACT_RELATIVES]: deselectArtefactsHandler,
  [CANCEL_RELATIVES_DELETE]: cancelRelativesDeleteHandler,
  [DELETE_RELATIVE_ARTEFACT]: deleteRelativeArtefactHandler,
  [DELETE_RELATIVE_ARTEFACT_DONE]: deleteRelativeArtefactDoneHandler,
  [CLEANUP_RELATIVE_ARTEFACT]: cleanUpRelativeArtefactHandler,
  [USER_SIGNED_OUT]: () => model(),
  [GET_ARTEFACT_CHILDREN_ERROR]: getArtefactChildrenErrorHandler,
  [GET_ARTEFACT_PARENTS_ERROR]: getArtefactParentsErrorHandler,
  [PARSE_MSD_REFERENCE_ERROR]: parseMsdReferenceErrorHandler,
};
