import { prop } from 'ramda';
import reducerFactory from '../../store/reducer-factory';
import {
  model as modelFactory,
  actions as actionsFactory,
  saga as sagaFactory,
  selectors as uploadSelectorsFactory,
  withUpload,
} from '../upload';
import selectorsFactory from './selectors';
import { uploadFromFile } from '../../api/upload-artefact';
import { MIME_TYPE_XML } from '../upload';

const MODULE = 'UPLOAD_ARTEFACT';
const NAMESPACE = 'uploadArtefact';
const dropzoneConfig = {
  accept: MIME_TYPE_XML,
  disableClick: true,
};
const model = modelFactory({ dropzoneConfig });
const actions = actionsFactory(MODULE, model);
const getState = state => prop(NAMESPACE, state);
const selectors = selectorsFactory(uploadSelectorsFactory(getState));
const apiCall = uploadFromFile;

export const uploadArtefactReducer = reducerFactory(model, actions.handlers);
export const uploadArtefactSaga = sagaFactory(actions, selectors, apiCall);
export const withUploadArtefact = withUpload(actions, selectors);
