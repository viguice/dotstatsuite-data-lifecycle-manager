import { all, call, put, select, take, takeLatest } from 'redux-saga/effects';
import { isEmpty } from 'ramda';
import { CHANGE_LOCALE } from './action-creators';
import {
  AGENCIES_FETCH_SUCCESS,
  AGENCIES_FETCH_ERROR,
  REFRESH_AGENCIES,
} from '../agencies/action-creators';
import { getArtefacts } from '../artefacts';
import { ARTEFACTS_FETCH, REFRESH_ARTEFACTS } from '../artefacts/action-creators';
import { CATEGORIES_FETCH_DONE, REFRESH_CATEGORIES } from '../categories/action-creators';
import { getSelectionIds } from '../filters';

function* agenciesWatcher() {
  const selectedAgencyIds = yield select(getSelectionIds('agencyIds'));
  if (isEmpty(selectedAgencyIds)) {
    yield put({ type: REFRESH_AGENCIES });
  } else {
    yield put({ type: REFRESH_AGENCIES });
    yield take([AGENCIES_FETCH_SUCCESS, AGENCIES_FETCH_ERROR]);
  }
}

function* categoriesWatcher() {
  const selectedCategoryIds = yield select(getSelectionIds('categoryIds'));
  if (isEmpty(selectedCategoryIds)) {
    yield put({ type: REFRESH_CATEGORIES });
  } else {
    yield put({ type: REFRESH_CATEGORIES });
    yield take(CATEGORIES_FETCH_DONE);
  }
}

function* changeLocaleWorker() {
  const artefacts = yield select(getArtefacts());
  if (!isEmpty(artefacts)) {
    yield put({ type: ARTEFACTS_FETCH });
  }
  yield all([call(agenciesWatcher), call(categoriesWatcher)]);
  yield put({ type: REFRESH_ARTEFACTS });
}

export default function* saga() {
  yield takeLatest(CHANGE_LOCALE, changeLocaleWorker);
}
