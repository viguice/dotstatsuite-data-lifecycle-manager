import { prop } from 'ramda';
import reducerFactory from '../../store/reducer-factory';
import {
  model as modelFactory,
  actions as actionsFactory,
  selectors as transferSelectorsFactory,
  withTransfer,
} from '../transfer';
import selectorsFactory from './selectors';
import sagaFactory from './saga';

const MODULE = 'TRANSFER_DATA';
const NAMESPACE = 'transferData';
const model = modelFactory();
const actions = actionsFactory(MODULE, modelFactory);
const getState = state => prop(NAMESPACE, state);
const selectors = selectorsFactory(transferSelectorsFactory(getState));

export const transferDataReducer = reducerFactory(model, actions.handlers);
export const transferDataSaga = sagaFactory(actions, selectors);
export const withTransferData = withTransfer(actions, selectors);
export const TRANSFER_DATA = actions.TRANSFER;
export const TRANSFER_DATA_SUCCESS = actions.TRANSFER_SUCCESS;
export const transferData = actions.transfer;
