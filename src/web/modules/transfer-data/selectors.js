import { createSelector } from 'reselect';
import * as R from 'ramda';
import { getArtefact } from '../common/selectors';

export default selectors => {
  return {
    ...selectors,
    getTransferOptions: id =>
      createSelector([getArtefact(id), selectors.getTransfer(id)], (dataflow, transfer) => ({
        space: R.prop('space', transfer),
        dataflow,
      })),
  };
};
