import { always, when } from 'ramda';
import { USER_SIGNED_IN, USER_SIGNED_OUT } from '../oidc/action-creators';

const model = () => ({
  isLogged: false,
  isInitialLogin: false,
});

export default (state = model(), action = {}) => {
  switch (action.type) {
    case USER_SIGNED_IN:
      return {
        ...state,
        isLogged: true,
        isInitialLogin: when(always(state.isLogged), always(false))(true),
      };
    case USER_SIGNED_OUT:
      return model();
    default:
      return state;
  }
};
