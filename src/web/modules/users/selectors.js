import { createSelector } from 'reselect';

const getState = state => state.users;

export const getIsInitialLogin = () => createSelector(getState, state => state.isInitialLogin);
