import { put, takeLatest } from 'redux-saga/effects';
import { closeTransfer } from './action-creators';
import { TRANSFER_DATA } from '../transfer-data';

export function* resetSelectionWorker() {
  yield put(closeTransfer());
}

export default function* saga() {
  yield takeLatest(TRANSFER_DATA, resetSelectionWorker);
}
