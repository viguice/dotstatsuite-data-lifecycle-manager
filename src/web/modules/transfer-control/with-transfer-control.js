import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  closeTransfer,
  toggleDestinationSpace,
  toggleHasDataQuery,
  updateDataQuery,
} from './action-creators';
import {
  getDataQuery,
  getDestinationSpace,
  getHasDataQuery,
  getIsOpenMenu,
  getTransferType,
  getArtefacts,
} from './selectors';
import { transferData } from '../transfer-data';
import { getInternalSpaces } from '../config';

export default Component => {
  const mapDispatchToProps = {
    closeTransfer,
    transferData,
    toggleDestinationSpace,
    toggleHasDataQuery,
    updateDataQuery,
  };

  const mapStateToProps = createStructuredSelector({
    artefacts: getArtefacts,
    dataQuery: getDataQuery(),
    destinationSpace: getDestinationSpace(),
    hasDataQuery: getHasDataQuery(),
    isOpenMenu: getIsOpenMenu(),
    spaces: getInternalSpaces(),
    transferType: getTransferType(),
  });

  return connect(mapStateToProps, mapDispatchToProps)(Component);
};
