import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  getCategoriesTree,
  getIsOpenCategorisationMenu,
  getIsCategorizing,
  getLogs,
} from './selectors';
import {
  categorize,
  closeArtefactCategorisation,
  expandToggleCategory,
  toggleCategory,
} from './action-creators';

export const withCategorisation = Component => {
  const mapStateToProps = createStructuredSelector({
    categories: getCategoriesTree,
    isOpenCategorisationMenu: getIsOpenCategorisationMenu,
  });

  const mapDispatchToProps = {
    categorize,
    closeArtefactCategorisation,
    toggleCategory,
    expandToggleCategory,
  };
  return connect(mapStateToProps, mapDispatchToProps)(Component);
};

export const withCategorisationState = id => Component => {
  const mapStateToProps = createStructuredSelector({
    isCategorizing: getIsCategorizing(id),
    categorisationLogs: getLogs(id),
  });

  return connect(mapStateToProps, {})(Component);
};
