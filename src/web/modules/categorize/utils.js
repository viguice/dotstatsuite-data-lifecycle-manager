import * as R from 'ramda';

const charEscapes = {
  '"': '&quot;',
  "'": '&apos;',
  '<': '&lt;',
  '>': '&gt;',
  '&': '&amp;',
};

const xmlEscape = R.replace(/["'<>&]/g, R.flip(R.prop)(charEscapes));

export const getXmlDataflowCategorisation = (dataflow, category, locale) => {
  const cScheme = R.propOr({}, 'scheme', category);
  const alter = id => R.replace(/\./g, '_', id);
  const categorisationId = `CAT_${alter(dataflow.agencyId)}__${
    dataflow.code
  }__${alter(dataflow.version)}@${alter(cScheme.agencyId)}__${
    cScheme.code
  }__${alter(cScheme.version)}__${alter(category.hierarchicalCode)}`;
  return `<structure:Categorisation id="${categorisationId}" agencyID="${
    dataflow.agencyId
  }" version="${dataflow.version}" isFinal="false">
      <common:Name xml:lang="${locale}">${xmlEscape(
    dataflow.label,
  )}</common:Name>
      <structure:Source>
        <Ref id="${dataflow.code}" version="${dataflow.version}" agencyID="${
    dataflow.agencyId
  }" package="datastructure" class="Dataflow" />
      </structure:Source>
      <structure:Target>
        <Ref id="${category.hierarchicalCode}" maintainableParentID="${
    cScheme.code
  }" maintainableParentVersion="${cScheme.version}" agencyID="${
    cScheme.agencyId
  }" package="categoryscheme" class="Category" />
      </structure:Target>
    </structure:Categorisation>`;
};

export const getXmlCategorisations = (
  dataflow,
  selection,
  locale,
) => `<?xml version="1.0" encoding="utf-8"?>
  <message:Structure xmlns:message="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message" xmlns:structure="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/structure" xmlns:common="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common">
    <message:Header>
      <message:ID>IDREF4371</message:ID>
      <message:Test>false</message:Test>
      <message:Prepared>2021-03-29T06:20:39.5022415+00:00</message:Prepared>
      <message:Sender id="Unknown" />
      <message:Receiver id="Unknown" />
    </message:Header>
    <message:Structures>
      <structure:Categorisations>
        ${R.pipe(
          R.map(category =>
            getXmlDataflowCategorisation(dataflow, category, locale),
          ),
          R.join('\n'),
        )(selection)}
      </structure:Categorisations>
    </message:Structures>
  </message:Structure>
`;
