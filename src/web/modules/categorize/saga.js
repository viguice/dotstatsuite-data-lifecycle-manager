import { all, takeLatest, call, put, select } from 'redux-saga/effects';
import * as R from 'ramda';
import { getLocale } from '../i18n';
import { postStructureRequest } from '../../sdmx-lib';
import {
  CATEGORIZE,
  categorizeArtefact,
  categorizeArtefactDone,
  categorizeDone,
} from './action-creators';
import {
  getArtefactId,
  getArtefactIds,
  getSelection,
  getSpace,
  getIsArtefactSelection,
} from './selectors';
import { getArtefact } from '../common/selectors';
import { ERROR, SUCCESS, WARNING } from '../common/constants';
import { getXmlCategorisations } from './utils';
import { requestDetails } from '../dataflow-details/action-creators';

function* categorizeDataflowWorker(dataflowId) {
  try {
    const locale = yield select(getLocale());
    const selection = yield select(getSelection);
    const space = yield select(getSpace);
    yield put(categorizeArtefact(dataflowId));
    const dataflow = yield select(getArtefact(dataflowId));
    const xml = getXmlCategorisations(dataflow, R.values(selection), locale);
    const data = new Blob([xml], { type: 'text/xml' });
    const logs = yield call(postStructureRequest, { space, locale, data });
    yield put(
      categorizeArtefactDone(dataflowId, {
        type: logs.isSuccess ? SUCCESS : logs.isWarning ? WARNING : ERROR,
        message: logs.data,
      }),
    );
    yield put(requestDetails(dataflowId));
  } catch (error) {
    yield put(categorizeArtefactDone(dataflowId, { type: ERROR, message: error.message }));
  }
}

function* categorizeWorker() {
  const isDataflowSelection = yield select(getIsArtefactSelection);
  if (isDataflowSelection) {
    const selectedDataflowIds = yield select(getArtefactIds);
    yield all(
      R.pipe(
        R.keys,
        R.map(dataflowId => call(categorizeDataflowWorker, dataflowId)),
      )(selectedDataflowIds),
    );
    yield put(categorizeDone());
  } else {
    const dataflowId = yield select(getArtefactId);
    yield call(categorizeDataflowWorker, dataflowId);
    yield put(categorizeDone());
  }
}

export default function* saga() {
  yield takeLatest(CATEGORIZE, categorizeWorker);
}
