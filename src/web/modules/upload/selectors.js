import { createSelector } from 'reselect';
import { isEmpty, length, propOr, values } from 'ramda';
import { getUploadSizeLimit } from '../config';

export default getState => {
  const selectorHelper = key => () => createSelector(getState, state => propOr({}, key, state));
  const getFiles = selectorHelper('files');
  const getSpaces = selectorHelper('spaces');
  const getAcceptedFiles = getFiles;
  const getDropzoneConfig = selectorHelper('dropzoneConfig');
  const getLog = selectorHelper('log');

  return {
    getState,
    getDropzoneConfig,
    getIsUploading: selectorHelper('isUploading'),
    getIsFlushable: selectorHelper('flushable'),
    getLog,
    getSpaces,
    getFiles,
    getAcceptedFiles, // default: all files are accepted
    getRejectedFiles: () => () => [], // default: no rejection
    getHasNoFile: () => createSelector([getFiles()], files => isEmpty(files)),
    hasUploadInAllSpace: () =>
      createSelector(
        [getLog(), getSpaces()],
        (log, spaces) => length(values(log)) === length(values(spaces)),
      ),
    getHasDataflowSelection: selectorHelper('hasDataflowSelection'),
    getHasPathOption: () => () => false,
    getMaxSize: () => createSelector(getUploadSizeLimit, sizeLimit => sizeLimit),
  };
};
