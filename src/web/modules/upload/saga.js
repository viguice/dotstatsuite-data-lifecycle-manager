import { all, call, put, select, takeLatest } from 'redux-saga/effects';
import { map } from 'ramda';
import { ERROR } from '../common/constants';

export default (actions, selectors, apiCall) => {
  function* uploadWorker(space, actionOptions) {
    try {
      const options = yield select(selectors.getUploadOptions());
      const log = yield call(apiCall, { ...options, ...actionOptions, space });
      if (log.type === ERROR) {
        throw log;
      }
      yield put(actions.uploadSuccess(log, space));
    } catch (log) {
      yield put(
        actions.uploadError(
          {
            type: ERROR,
            message: log.message,
          },
          space,
        ),
      );
    }
  }

  function* uploadFromAllSpacesWorker(action) {
    const spaces = yield select(selectors.getSpaces());
    const actionOptions = action.payload.options;
    yield all(map(space => call(uploadWorker, space, actionOptions), spaces));
  }

  function* resetWorker() {
    const isFlushable = yield select(selectors.getIsFlushable());
    if (isFlushable) {
      yield put(actions.formReset());
    }
  }

  function* hasUploadInAllSpaceWorker() {
    const isValid = yield select(selectors.hasUploadInAllSpace());
    if (isValid) {
      yield put(actions.hasUploadInAllSpace());
    }
  }

  return function* saga() {
    yield takeLatest(actions.UPLOAD, uploadFromAllSpacesWorker);
    yield takeLatest(
      [actions.UPLOAD_SUCCESS, actions.UPLOAD_ERROR],
      hasUploadInAllSpaceWorker,
    );
    yield takeLatest('@@router/LOCATION_CHANGE', resetWorker);
  };
};
