import { dissoc, reject, find } from 'ramda';
import { ERROR } from '../common/constants';
import { USER_SIGNED_OUT } from '../oidc/action-creators';

const uploaded = type => (log, space) => ({ type, payload: { log, space } });
const uploadedHandler = (state, action) => ({
  ...state,
  log: {
    ...state.log,
    [action.payload.space.id]: {
      ...action.payload.log,
      space: action.payload.space,
    },
  },
});

export default (module, model) => {
  const FILES_CHANGE = `${module}_FILES_CHANGE`;
  const filesChange = files => ({ type: FILES_CHANGE, payload: { files } });
  const filesChangeHandler = (state, action) => ({
    ...state,
    files: [...state.files, ...action.payload.files],
    flushable: true,
  });

  const FILE_REMOVE = `${module}_FILE_REMOVE`;
  const fileRemove = file => ({ type: FILE_REMOVE, payload: { file } });
  const fileRemoveHandler = (state, action) => ({
    ...state,
    files: reject(file => file === action.payload.file, state.files),
    flushable: true,
  });

  const SELECT_SPACE = `${module}_SELECT_SPACE`;
  const selectSpace = space => ({ type: SELECT_SPACE, payload: { space } });
  const selectSpaceHandler = (state, action) => ({
    ...state,
    spaces: {
      ...state.spaces,
      [action.payload.space.id]: action.payload.space,
    },
    flushable: true,
  });

  const REMOVE_SPACE = `${module}_REMOVE_SPACE`;
  const removeSpace = space => ({ type: REMOVE_SPACE, payload: { space } });
  const removeSpaceHandler = (state, action) => ({
    ...state,
    spaces: dissoc(action.payload.space.id, state.spaces),
    flushable: true,
  });

  const UPLOAD = `${module}_UPLOAD`;
  const upload = (options = {}) => ({ type: UPLOAD, payload: { options } });
  const uploadHandler = state => ({
    ...state,
    isUploading: true,
    log: {},
    flushable: false,
  });

  const UPLOAD_SUCCESS = `${module}_UPLOAD_SUCCESS`;
  const uploadSuccess = uploaded(UPLOAD_SUCCESS);

  const UPLOAD_ERROR = `${module}_UPLOAD_ERROR`;
  const uploadError = uploaded(UPLOAD_ERROR);

  const FORM_RESET = `${module}_FORM_RESET`;
  const formReset = () => ({ type: FORM_RESET });
  const formResetHandler = state => Object.assign({}, state, model);

  const HAS_UPLOAD_IN_ALL_SPACE = `${module}_HAS_UPLOAD_IN_ALL_SPACE`;
  const hasUploadInAllSpace = () => ({ type: HAS_UPLOAD_IN_ALL_SPACE });
  const hasUploadInAllSpaceHandler = state => ({
    ...state,
    isUploading: false,
    flushable: !find(log => log.type === ERROR, state.log),
  });

  return {
    FILES_CHANGE,
    filesChange,
    FILE_REMOVE,
    fileRemove,
    SELECT_SPACE,
    selectSpace,
    REMOVE_SPACE,
    removeSpace,
    UPLOAD,
    upload,
    UPLOAD_SUCCESS,
    uploadSuccess,
    UPLOAD_ERROR,
    uploadError,
    FORM_RESET,
    formReset,
    HAS_UPLOAD_IN_ALL_SPACE,
    hasUploadInAllSpace,
    handlers: {
      [FILES_CHANGE]: filesChangeHandler,
      [FILE_REMOVE]: fileRemoveHandler,
      [SELECT_SPACE]: selectSpaceHandler,
      [REMOVE_SPACE]: removeSpaceHandler,
      [UPLOAD]: uploadHandler,
      [UPLOAD_SUCCESS]: uploadedHandler,
      [UPLOAD_ERROR]: uploadedHandler,
      [FORM_RESET]: formResetHandler,
      [HAS_UPLOAD_IN_ALL_SPACE]: hasUploadInAllSpaceHandler,
      [USER_SIGNED_OUT]: () => model,
    },
  };
};
