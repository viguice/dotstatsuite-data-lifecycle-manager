import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { changeFilePath, toggleByPath } from './action-creators';

export const withUploadPathFactory = selectors => Component => {
  const mapDispatchToProps = {
    changeFilePath,
    toggleByPath,
  };

  const mapStateToProps = createStructuredSelector({
    byPath: selectors.getByPath(),
    filepath: selectors.getFilePath(),
    hasPathOption: selectors.getHasPathOption(),
  });

  return connect(mapStateToProps, mapDispatchToProps)(Component);
};
