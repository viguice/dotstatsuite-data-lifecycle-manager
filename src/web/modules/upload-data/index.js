import { prop } from 'ramda';
import reducerFactory from '../../store/reducer-factory';
import {
  model as modelFactory,
  actions as actionsFactory,
  saga as sagaFactory,
  selectors as uploadSelectorsFactory,
  withUpload,
} from '../upload';
import selectorsFactory from './selectors';
import { upload } from '../../api/upload-data';
import handlers from './action-creators';
import { withUploadPathFactory } from './with-upload-data';

const MODULE = 'UPLOAD_DATA';
const NAMESPACE = 'uploadData';
const uploadDataModel = {
  dropzoneConfig: {
    disableClick: false,
  },
  byPath: false,
  filePath: undefined,
};
const model = modelFactory(uploadDataModel);
const actions = actionsFactory(MODULE, model);
const getState = state => prop(NAMESPACE, state);
const selectors = selectorsFactory(uploadSelectorsFactory(getState));
const apiCall = upload;

export const uploadDataReducer = reducerFactory(model, { ...actions.handlers, ...handlers });
export const uploadDataSaga = sagaFactory(actions, selectors, apiCall);
export const withUploadData = withUpload(actions, selectors);
export const withUploadPath = withUploadPathFactory(selectors);
