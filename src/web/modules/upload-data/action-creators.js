import { dissoc } from 'ramda';
const MODULE = 'UPLOAD_DATA';
const REMOVE_SPACE = `${MODULE}_REMOVE_SPACE`;

export const changeModeHandler = (state, action) => ({
  ...state,
  flushable: true,
  mode: action.payload.mode,
});

export const removeSpaceHandler = (state, action) => ({
  ...state,
  spaces: dissoc(action.payload.space.id, state.spaces),
  flushable: true,
});

export const TOGGLE_BY_PATH = `${MODULE}_TOGGLE_BY_PATH`;
export const toggleByPath = () => ({
  type: TOGGLE_BY_PATH,
});
export const toggleByPathHandler = state => ({
  ...state,
  byPath: !state.byPath,
  filepath: undefined,
  flushable: true,
});

export const CHANGE_FILEPATH = `${MODULE}_CHANGE_FILEPATH`;
export const changeFilePath = filepath => ({
  type: CHANGE_FILEPATH,
  payload: { filepath },
});
export const changeFilePathHandler = (state, action) => ({
  ...state,
  filepath: action.payload.filepath,
  flushable: true,
});

export default {
  [REMOVE_SPACE]: removeSpaceHandler,
  [TOGGLE_BY_PATH]: toggleByPathHandler,
  [CHANGE_FILEPATH]: changeFilePathHandler,
};
