import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { exportData } from './action-creators';
import { getIsExportingData, getExportDataLog } from './selectors';

export default id => Component => {
  const mapDispatchToProps = {
    exportData,
  };

  const mapStateToProps = createStructuredSelector({
    isExportingData: getIsExportingData(id),
    error: getExportDataLog(id),
  });

  return connect(mapStateToProps, mapDispatchToProps)(Component);
};
