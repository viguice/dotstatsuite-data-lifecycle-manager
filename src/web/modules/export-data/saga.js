import { all, takeEvery, call, put, select } from 'redux-saga/effects';
import { dataflowExportData } from '../../api/export-data';
import { exportMetadata } from '../../api/metadata';
import { getArtefactOptions } from '../common/selectors';
import {
  EXPORT_DATA,
  exportDataflowData,
  exportDataflowDataSuccess,
  exportDataflowDataError,
  exportDataflowDataEmpty,
  exportDataflowMetadata,
  exportDataflowMetadataSuccess,
  exportDataflowMetadataError,
  exportDataflowMetadataEmpty,
} from './action-creators';
import { has, map, path } from 'ramda';

const getFormat = contentType => (contentType === 'dataXml' ? 'xml' : 'csv');

export function* exportDataWorker({ id, contentType, dataquery }) {
  const format = getFormat(contentType);
  yield put(exportDataflowData(id));

  try {
    const dataflowOptions = yield select(getArtefactOptions(id));
    yield call(dataflowExportData, dataflowOptions, format, dataquery);
    yield put(exportDataflowDataSuccess(id));
  } catch (error) {
    if (has('response', error) && path(['response', 'status'], error) === 404) {
      yield put(exportDataflowDataEmpty(id));
    } else if (has('response', error)) {
      yield put(exportDataflowDataError(id, { message: error.response.data }));
    }
    yield put(exportDataflowDataError(id, error));
  }
}

export function* exportMetadataWorker({ id, dataquery }) {
  yield put(exportDataflowMetadata(id));
  try {
    const dataflow = yield select(getArtefactOptions(id));
    const filename = `${dataflow.agencyId}-${dataflow.code}-${dataflow.version}-metadata.csv`;
    yield call(exportMetadata, { dataflow, dataquery, filename });
    yield put(exportDataflowMetadataSuccess(id));
  } catch (error) {
    if (has('response', error) && path(['response', 'status'], error) === 404) {
      yield put(exportDataflowMetadataEmpty(id));
    } else if (has('response', error)) {
      yield put(exportDataflowMetadataError(id, { message: error.response.data }));
    } else {
      yield put(exportDataflowMetadataError(id, error));
    }
  }
}

export function* exportDataflowWorker({ id, contentType, dataquery }) {
  if (contentType !== 'metadataCsv') {
    yield call(exportDataWorker, { id, contentType, dataquery });
  }
  if (contentType === 'bothCsv' || contentType === 'metadataCsv') {
    yield call(exportMetadataWorker, { id, dataquery });
  }
}

export function* exportDataFromAllSelectionWorker(action) {
  const { selection, contentType, dataquery } = action.payload;
  yield all(map(id => call(exportDataflowWorker, { id, contentType, dataquery }), selection));
}

export default function* saga() {
  yield takeEvery(EXPORT_DATA, exportDataFromAllSelectionWorker);
}
