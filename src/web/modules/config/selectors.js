import { createSelector } from 'reselect';
import {
  pipe,
  path,
  map,
  indexBy,
  pickBy,
  prop,
  toPairs,
  values,
  pathOr,
  pluck,
  uniq,
  length,
  lt,
  assoc,
} from 'ramda';
import { withSort } from '../common/utils';

const getState = state => state.config;

export const getUploadSizeLimit = createSelector(getState, path(['sdmx', 'uploadSizeLimit']));

export const getSpaces = () =>
  createSelector(
    getState,
    pipe(
      pathOr({}, ['sdmx', 'spaces']),
      toPairs,
      map(([id, { url, ...rest }]) => {
        const res = { id, endpoint: url, ...rest };
        if (prop('allowPermissionMgmt', rest) && !prop('authzServerUrl', rest))
          return assoc('authzServerUrl', window.CONFIG.authzServerUrl, res);
        return res;
      }),
      indexBy(prop('id')),
    ),
  );

export const getSdmxTypeIds = createSelector(getState, path(['sdmx', 'typeIds']));

export const getTypes = () =>
  createSelector(
    getState,
    pipe(
      path(['sdmx', 'typeIds']),
      map(id => ({ id, label: id })),
      indexBy(prop('id')),
    ),
  );

export const getInternalSpaces = () =>
  createSelector([getSpaces()], spaces => pickBy(space => !!prop('transferUrl', space), spaces));

export const getExternalSpaces = () =>
  createSelector([getSpaces()], spaces => pickBy(space => !prop('transferUrl', space), spaces));

export const getSpacesForApi = () =>
  createSelector([getSpaces()], spaces => indexBy(prop('endpoint'), values(spaces)));

export const getSortedSpaces = (fields = []) => createSelector([getSpaces()], withSort(fields));

export const getSortedInternalSpaces = (fields = []) =>
  createSelector([getInternalSpaces()], withSort(fields));

export const getSortedExternalSpaces = (fields = []) =>
  createSelector([getExternalSpaces()], withSort(fields));

export const getHasMultipleAuthZ = createSelector(
  getInternalSpaces(),
  pipe(values, pluck('authzServerUrl'), uniq, length, lt(1)),
);
