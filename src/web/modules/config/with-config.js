import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { getSortedInternalSpaces, getInternalSpaces } from './selectors';

export default Component => {
  const mapDispatchToProps = {};

  const mapStateToProps = createStructuredSelector({
    internalSpaces: getSortedInternalSpaces(),
    internalSpacesById: getInternalSpaces(),
  });

  return connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Component);
};
