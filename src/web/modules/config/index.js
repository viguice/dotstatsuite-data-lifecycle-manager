import reducerFactory from '../../store/reducer-factory';
import handlers from './action-creators';

export default reducerFactory({}, handlers);
export { default as configSaga } from './saga';
export { default as withConfig } from './with-config';
export {
  getSpaces,
  getTypes,
  getSpacesForApi,
  getUploadSizeLimit,
  getInternalSpaces,
  getExternalSpaces,
} from './selectors';
