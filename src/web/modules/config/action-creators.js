import * as R from 'ramda';
import { SET_SPACE_EXT_AUTH_OPTIONS } from '../external-auth/action-creators';

const setSpaceExtAuthOptionsHandler = (state, action) =>
  R.over(
    R.lensPath(['sdmx', 'spaces']),
    R.map(space => {
      if (space.id === action.payload.spaceId) {
        return { ...space, extAuthOptions: action.payload.options };
      }
      return space;
    }),
  )(state);

export const REGISTER_AUTHZ_URLS = 'REGISTER_AUTHZ_URLS';

export const registerAuthzUrls = authzUrlsBySpace => ({
  type: REGISTER_AUTHZ_URLS,
  payload: { authzUrlsBySpace },
});

const registerAuthzUrlsHandler = (state, action) =>
  R.over(
    R.lensPath(['sdmx', 'spaces']),
    R.map(space => {
      if (R.has(space.id, action.payload.authzUrlsBySpace)) {
        return { ...space, authzServerUrl: action.payload.authzUrlsBySpace[space.id] };
      }
      return space;
    }),
  )(state);

export default {
  [SET_SPACE_EXT_AUTH_OPTIONS]: setSpaceExtAuthOptionsHandler,
  [REGISTER_AUTHZ_URLS]: registerAuthzUrlsHandler,
};
