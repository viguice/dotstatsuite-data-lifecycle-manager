import { all, takeLatest, call, put, select } from 'redux-saga/effects';
import { parseTreeResultsAsList } from '@sis-cc/dotstatsuite-sdmxjs';
import * as R from 'ramda';
import {
  categoriesFetch,
  categoriesFetchDone,
  categoriesFetchError,
  categoriesFetchSuccess,
  REFRESH_CATEGORIES,
} from './action-creators';
import { getCategoriesRequestArgs } from './selectors';
import { FILTERS_SPACE_TOGGLE, getSelection } from '../filters';
import { withAuthHeader } from '../common/withauth';
import apiManager from '../../apiManager';

export function* fetchCategoriesFromSpaceWorker(space) {
  try {
    const { url, headers, params } = yield select(getCategoriesRequestArgs(space));
    const response = yield call(apiManager.get, url, {
      headers: withAuthHeader(space)(headers),
      params,
    });
    const categories = R.pipe(
      parseTreeResultsAsList({ lists: 'categorySchemes', listValues: 'categories' }),
      R.values,
      R.reduce((acc, cat) => {
        const id = `${space.id}:${cat.id}`;
        return {
          ...acc,
          [id]: {
            ...cat,
            id,
            spaceId: space.id,
            parentId: cat.parentId ? `${space.id}:${cat.parentId}` : '#root',
            topParentId: cat.topParentId ? `${space.id}:${cat.topParentId}` : '#root',
            isExpanded: false,
          },
        };
      }, {}),
    )(response.data);
    yield put(categoriesFetchSuccess(categories));
  } catch (error) {
    yield put(categoriesFetchError(space.id, error));
  }
}

export function* fetchCategoriesWorker() {
  yield put(categoriesFetch());
  const { spaces } = yield select(getSelection());
  yield all(
    R.pipe(
      R.values,
      R.map(space => call(fetchCategoriesFromSpaceWorker, space)),
    )(spaces),
  );
  yield put(categoriesFetchDone());
}

export default function* saga() {
  yield takeLatest([FILTERS_SPACE_TOGGLE, REFRESH_CATEGORIES], fetchCategoriesWorker);
}
