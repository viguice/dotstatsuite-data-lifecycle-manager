import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { getCategoriesTreeBySpace, getIsFetching } from './selectors';
import { categoryExpandToggle } from './action-creators';

export const withCategories = Component => {
  const mapStateToProps = createStructuredSelector({
    isFetching: getIsFetching(),
  });

  return connect(mapStateToProps)(Component);
};

export const withSpacedCategories = spaceId => Component => {
  const mapDispatchToProps = {
    categoryExpandToggle,
  };

  const mapStateToProps = createStructuredSelector({
    categories: getCategoriesTreeBySpace(spaceId),
  });

  return connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Component);
};
