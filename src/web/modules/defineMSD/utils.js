import * as R from 'ramda';
import xmlMapping from 'xml-mapping';

export const isMSDLinkPossible = selection => {
  const onSameSpace = R.pipe(
    R.map(R.path(['space', 'id'])),
    R.uniq,
    R.length,
    R.equals(1),
  )(R.values(selection));
  if (!onSameSpace) {
    return false;
  }
  return R.all(artefact => {
    if (artefact.type !== 'datastructure') {
      return false;
    }
    if (R.isNil(R.path(['space', 'transferUrl'], artefact))) {
      return false;
    }
    const dsdRef = R.find(annot => annot.type === 'METADATA', artefact.annotations || []);
    return R.isNil(dsdRef);
  }, R.values(selection));
};

export const getXmlDefinedMsd = xml => {
  const json = xmlMapping.load(xml, { nested: true });
  const annotations = R.path(
    [
      'message$Structure',
      'message$Structures',
      'structure$DataStructures',
      'structure$DataStructure',
      'common$Annotations',
      'common$Annotation',
    ],
    json,
  );
  if (R.isNil(annotations)) {
    return undefined;
  }
  const msdRef = R.pipe(
    R.when(a => !R.is(Array, a), R.of),
    R.find(R.pathEq(['common$AnnotationType', '$t'], 'METADATA')),
  )(annotations);
  if (R.isNil(msdRef)) {
    return undefined;
  }
  const url = R.path(['common$AnnotationTitle', '$t'], msdRef);
  return R.pipe(R.split('='), R.last)(url);
};

export const getMsdAttachedXml = (xml, msd) => {
  const msdRefAnnotation = {
    common$AnnotationTitle: {
      $t: `urn:sdmx:org.sdmx.infomodel.metadatastructure.MetadataStructure=${msd.agencyId}:${msd.code}(${msd.version})`,
    },
    common$AnnotationType: { $t: 'METADATA' },
  };

  const json = xmlMapping.load(xml, { nested: true });

  const evolvedJson = R.over(
    R.lensPath([
      'message$Structure',
      'message$Structures',
      'structure$DataStructures',
      'structure$DataStructure',
    ]),
    structure => {
      if (!R.has('common$Annotations', structure || {})) {
        return R.mergeRight(
          { common$Annotations: { common$Annotation: msdRefAnnotation } },
          structure || {},
        );
      }
      return R.over(R.lensPath(['common$Annotations', 'common$Annotation']), annotations =>
        R.is(Array, annotations)
          ? R.append(msdRefAnnotation, annotations)
          : [annotations, msdRefAnnotation],
      )(structure);
    },
  )(json);
  const evolvedXml = xmlMapping.dump(evolvedJson);
  return evolvedXml;
};
