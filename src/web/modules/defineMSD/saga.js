import { all, takeLatest, call, put, select, takeEvery } from 'redux-saga/effects';
import * as R from 'ramda';
import * as SDMXJS from '@sis-cc/dotstatsuite-sdmxjs';
import apiManager from '../../apiManager';
import { initDataflow } from '../../api/initDataflow';
import { withAuthHeader } from '../common/withauth';
import { getLocale } from '../i18n';
import { REFRESH_ARTEFACT } from '../artefacts/action-creators';
import {
  FETCH_ALL_MSD,
  LINK_ALL_MSD,
  LINK_MSD_CALLBACK,
  linkMsd,
  linkMsdCallback,
  fetchAllMSDSuccess,
  fetchAllMSDError,
  initDataflows,
  initDataflowsCallback,
} from './action-creators';
import { getDsds } from './selectors';
import { getMsdAttachedXml, getXmlDefinedMsd } from './utils';
import { postStructureRequest } from '../../sdmx-lib/requests';
import { getArtefact } from '../common/selectors';

function* fetchMSDsWorker(action) {
  const dsds = action.payload.dsds;
  const space = R.pipe(R.values, R.head, R.prop('space'))(dsds);
  const locale = yield select(getLocale());

  const { url, headers, params } = SDMXJS.getRequestArgs({
    datasource: R.assoc('url', R.prop('endpoint', space), space),
    identifiers: { agencyId: 'all', code: 'all', version: 'all' },
    type: 'metadatastructure',
    overview: true,
    withReferences: false,
    locale,
  });

  try {
    const response = yield call(apiManager.get, url, {
      headers: withAuthHeader(space)(headers),
      params,
    });
    const artefacts = R.pipe(
      R.prop('data'),
      SDMXJS.parseStubLists,
      R.prop('metadatastructure'),
      R.map(msd => R.assoc('id', `${space.id}:${msd.agencyId}:${msd.code}:${msd.version}`, msd)),
    )(response);
    yield put(fetchAllMSDSuccess(artefacts));
  } catch (error) {
    yield put(fetchAllMSDError(error));
  }
}

function* getDSDXML(dsd) {
  const { url, headers, params } = SDMXJS.getRequestArgs({
    datasource: R.assoc('url', R.prop('endpoint', dsd.space), dsd.space),
    identifiers: dsd,
    type: 'datastructure',
    withReferences: false,
    format: 'xml',
  });

  const response = yield call(apiManager.get, url, {
    headers: withAuthHeader(dsd.space)(headers),
    params,
  });
  return response.data;
}

const getDataflowsRefs = dsd => {
  const { url, headers, params } = SDMXJS.getRequestArgs({
    identifiers: { ...dsd },
    type: 'datastructure',
    datasource: R.assoc('url', R.prop('endpoint', dsd.space), dsd.space),
    params: { references: 'dataflow' },
  });

  return apiManager
    .get(url, { headers, params })
    .then(res => R.pipe(SDMXJS.parseStubLists, R.prop('dataflow'))(res.data))
    .catch(R.always([]));
};

function* initDataflowsWorker({ payload }) {
  if (R.path(['log', 'type'], payload) === 'ERROR') {
    return;
  }
  const dsd = yield select(getArtefact(payload.id));
  const dataflows = yield call(getDataflowsRefs, dsd);
  if (R.isEmpty(dataflows)) {
    return;
  }
  yield put(initDataflows(payload.id));
  const logs = yield all(
    R.map(dataflow => call(initDataflow, { ...dataflow, space: dsd.space }), dataflows),
  );
  yield put(initDataflowsCallback(payload.id, logs));
}

function* linkMSDtoDSDWorker({ msd, dsd }) {
  try {
    yield put(linkMsd(dsd.id));
    const dsdXML = yield call(getDSDXML, dsd);
    const msdAttached = yield call(getMsdAttachedXml, dsdXML, msd);
    const msdAlreadyAttched = getXmlDefinedMsd(dsdXML);
    if (!R.isNil(msdAlreadyAttched)) {
      yield put(
        linkMsdCallback(dsd.id, {
          type: 'ERROR',
          ref: msdAlreadyAttched,
        }),
      );
    } else {
      const log = yield call(postStructureRequest, {
        space: dsd.space,
        data: msdAttached,
        locale: 'en',
      });
      yield put(
        linkMsdCallback(dsd.id, {
          ...log,
          type: log.isSuccess ? 'SUCCESS' : 'ERROR',
          message: log.data,
        }),
      );
      if (log.isSuccess) {
        yield put({ type: REFRESH_ARTEFACT, payload: { ...dsd, space: dsd.space } });
      }
    }
  } catch (error) {
    yield put(linkMsdCallback(dsd.id, { type: 'ERROR', message: error.message }));
  }
}

function* linkMSDWorker(action) {
  const msd = action.payload.msd;
  const dsds = yield select(getDsds);

  yield all(R.map(dsd => call(linkMSDtoDSDWorker, { msd, dsd }), R.values(dsds)));
}

export default function* saga() {
  yield takeLatest(FETCH_ALL_MSD, fetchMSDsWorker);
  yield takeLatest(LINK_ALL_MSD, linkMSDWorker);
  yield takeEvery([LINK_MSD_CALLBACK], initDataflowsWorker);
}
