import { replace, startsWith, reduce } from 'ramda';

// sdmx relies on a non-existent code to perform real request,
// instead of using code for agencies, we have to use hierarchicalCode
// (which is a computed field)
// but there is an exception: since SDMX is the root, 'SDMX.' has to be ignored
export const getHierarchicalCodeHelper = (agency, prefix = 'SDMX.') => {
  return startsWith(prefix, agency.hierarchicalCode)
    ? replace(prefix, '', agency.hierarchicalCode)
    : agency.hierarchicalCode;
};

// we fetch agencies and schemes for the sdmx space
// to avoid dup within the agency tree, we merge the agencies by path
// path is the compouted field hierarchicalCode
// the rest of the app rely on id to identify artefacts
const AGENCY = 'agency';
const SCHEME = 'agencyscheme';
export const setAgencies = agenciesAndSchemes => {
  const { agencies, schemes } = reduce(
    (memo, aors) => {
      const type = aors.type;

      if (type === AGENCY) {
        return { ...memo, agencies: { ...memo.agencies, [aors.hierarchicalCode]: aors } };
      }

      if (type === SCHEME) {
        return { ...memo, schemes: { ...memo.schemes, [aors.id]: aors } };
      }

      return memo;
    },
    { agencies: {}, schemes: {} },
    agenciesAndSchemes,
  );

  return {
    agencies: reduce((m, a) => ({ ...m, [a.id]: a }), {}, agencies),
    schemes,
  };
};
