import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { getAgenciesTree, getIsFetching } from './selectors';
import { agencyToggleNode } from './action-creators';

export default Component => {
  const mapDispatchToProps = {
    agencyExpandNode: agencyToggleNode(true),
    agencyCollapseNode: agencyToggleNode(false),
  };

  const mapStateToProps = createStructuredSelector({
    agencies: getAgenciesTree(),
    isFetching: getIsFetching(),
  });

  return connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Component);
};
