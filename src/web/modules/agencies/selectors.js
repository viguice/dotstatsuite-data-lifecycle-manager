import { createSelector } from 'reselect';
import { groupBy, map, propOr, reduce, values } from 'ramda';
import { getAgencies as getAgenciesFromFilters } from '../filters';
import { css } from 'glamor';

const getState = state => state.agencies;

export const getIsFetching = () => createSelector(getState, state => state.isFetching);
export const getAgencies = () => createSelector(getState, state => state.agencies);
export const getExpanded = () => createSelector(getState, state => state.expanded);

export const ROOT_AGENCY_SCHEME_ID = 'SDMX';

const CodeLabelStyle = css({ color: 'grey', fontStyle: 'italic' });

export const getAgenciesFlatFilterItems = () =>
  createSelector([getAgencies(), getExpanded()], (agencies, expanded) =>
    reduce(
      (memo, agency) => {
        const isExpanded = !!propOr(false, agency.id, expanded);
        let _agency = {};
        if (!propOr(null, 'name', agency)) {
          _agency = { label: agency.code, className: `${CodeLabelStyle}` };
        }
        return {
          ...memo,
          [agency.id]: {
            ...agency,
            ..._agency,
            label: `(${agency.code}) ${agency.name}`,
            isExpanded,
          },
        };
      },
      {},
      values(agencies),
    ),
  );

const recurse = (agencies, agencyId, codeSelector = 'id') =>
  map(agency => {
    if (
      propOr(null, agency[codeSelector], agencies) &&
      agency[codeSelector] !== ROOT_AGENCY_SCHEME_ID
    ) {
      return {
        ...agency,
        childNodes: recurse(agencies, agency[codeSelector]),
      };
    }
    return agency;
  }, agencies[agencyId] || []);

export const getAgenciesTree = () =>
  createSelector([getAgenciesFromFilters()], agencies => {
    const groupedItems = groupBy(item => item.parent, values(agencies));
    return recurse(groupedItems, ROOT_AGENCY_SCHEME_ID, 'code');
  });
