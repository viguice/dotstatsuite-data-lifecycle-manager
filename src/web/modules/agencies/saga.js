import * as R from 'ramda';
import { call, all, put, select, takeLatest } from 'redux-saga/effects';
import {
  agenciesFetch,
  agenciesFetchSuccess,
  agenciesFetchError,
  REFRESH_AGENCIES,
} from './action-creators';
import { getSpacesForApi } from '../config';
import { getLocale } from '../i18n';
import { getIsInitialLogin } from '../users';
import { parseAgencies, getRequestArgs } from '@sis-cc/dotstatsuite-sdmxjs';
import { withAuthHeader } from '../common/withauth';
import { USER_SIGNED_IN } from '../oidc/action-creators';
import apiManager from '../../apiManager';

const getAgenciesFromSpace = ({ locale, space }) => {
  const { url, headers, params } = getRequestArgs({
    datasource: { url: space.endpoint, ...space },
    locale,
    type: 'agencyscheme',
    identifiers: { version: '1.0', agencyId: 'all', code: 'all' },
  });
  return apiManager
    .get(url, { headers: withAuthHeader(space)(headers), params })
    .catch(error => {
      const log = error.response ? error.response.data : error.request.data;
      return { isError: true, log };
    })
    .then(res => {
      if (res.isError) {
        return res;
      }
      return {
        agencies: parseAgencies({ data: R.path(['data', 'data'], res) }),
        log: null,
      };
    });
};

export function* fetchAgenciesWorker(action) {
  const isInitialLogin = yield select(getIsInitialLogin());
  if (action.type === USER_SIGNED_IN && !isInitialLogin) {
    return;
  }
  const spaces = yield select(getSpacesForApi());
  const locale = yield select(getLocale());
  yield put(agenciesFetch());

  try {
    const results = yield all(
      R.map(space => call(getAgenciesFromSpace, { locale, space }), spaces),
    );
    const agencies = R.pipe(R.values, R.pluck('agencies'), R.mergeAll)(results);
    yield put(agenciesFetchSuccess(agencies));
  } catch (error) {
    yield put(agenciesFetchError(error));
  }
}

export default function* saga() {
  yield takeLatest([USER_SIGNED_IN, REFRESH_AGENCIES], fetchAgenciesWorker);
}
