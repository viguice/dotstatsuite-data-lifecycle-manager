// export function: model can be configurable and won't be modified if recalled
export default () => ({
  // available agencies => { agencyId: { id, label, type } }
  agencies: {},

  // error about agencies
  error: null,

  // fetch in progress about agencies
  isFetching: false,
});
