import { MODULE } from './constants';

export const EXPORT_ARTEFACTS = `${MODULE}_EXPORT_ARTEFACTS`;
export const exportArtefacts = (ids, withReferences) => ({
  type: EXPORT_ARTEFACTS,
  payload: { ids, withReferences },
});

export const DELETE_ARTEFACTS = `${MODULE}_DELETE_ARTEFACTS`;
export const deleteArtefacts = ids => ({
  type: DELETE_ARTEFACTS,
  payload: { ids },
});

export const TRANSFER_ARTEFACTS = `${MODULE}_TRANSFER_ARTEFACTS`;
export const transferArtefacts = (ids, space, withReferences) => ({
  type: TRANSFER_ARTEFACTS,
  payload: { ids, space, withReferences },
});
