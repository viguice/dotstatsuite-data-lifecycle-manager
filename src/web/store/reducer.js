import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { i18nReducer } from '../modules/i18n';
import configReducer from '../modules/config';
import { usersReducer } from '../modules/users';
import { filtersReducer } from '../modules/filters';
import { agenciesReducer } from '../modules/agencies';
import { artefactsReducer } from '../modules/artefacts';
import { uploadArtefactReducer } from '../modules/upload-artefact';
import { uploadDataReducer } from '../modules/upload-data';
import { transferArtefactReducer } from '../modules/transfer-artefact';
import { transferDataReducer } from '../modules/transfer-data';
import { exportDataReducer } from '../modules/export-data';
//import { selectionReducer } from '../modules/selection';
import { transferControlReducer } from '../modules/transfer-control';
import { dataflowDetailsReducer } from '../modules/dataflow-details';
import { categoriesReducer } from '../modules/categories';
import { dumpReducer } from '../modules/dump';
import { relativesReducer } from '../modules/relatives';
import { categorizeReducer } from '../modules/categorize';
import { externalAuthReducer } from '../modules/external-auth';
import { oidcReducer } from '../modules/oidc';
import { defineMsdReducer } from '../modules/defineMSD';
import { sfsIndexReducer } from '../modules/sfsIndex';
import { logsReducer } from '../modules/logs';

export const createReducer = () => {
  return combineReducers({
    routing: routerReducer, // key name is a convention used by syncHistoryWithStore
    i18n: i18nReducer,
    users: usersReducer,
    oidc: oidcReducer,
    config: configReducer,
    filters: filtersReducer,
    agencies: agenciesReducer,
    artefacts: artefactsReducer,
    uploadArtefact: uploadArtefactReducer,
    uploadData: uploadDataReducer,
    transferArtefact: transferArtefactReducer,
    transferData: transferDataReducer,
    exportData: exportDataReducer,
    //selection: selectionReducer,
    transferControl: transferControlReducer,
    dataflowDetails: dataflowDetailsReducer,
    categories: categoriesReducer,
    dump: dumpReducer,
    relatives: relativesReducer,
    categorize: categorizeReducer,
    externalAuth: externalAuthReducer,
    defineMsd: defineMsdReducer,
    sfsIndex: sfsIndexReducer,
    logs: logsReducer,
  });
};
