export default (model, handlers) => (state = model, action) => {
  const handler = handlers[action.type];
  return handler ? handler(state, action) : state;
};
