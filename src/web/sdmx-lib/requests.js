import * as R from 'ramda';
import { sdmxXmlErrorParser } from './parsers';
import { withAuthHeader } from '../modules/common/withauth';
import apiManager from '../apiManager';

export const XML_STRUCTURE_FORMAT = 'application/vnd.sdmx.structure+xml;version=2.1';
export const JSON_STRUCTURE_FORMAT = 'application/vnd.sdmx.structure+json;version=1.0;urn=true';

export const XML_DATA_FORMAT = 'application/vnd.sdmx.genericdata+xml;version=2.1';
export const JSON_DATA_FORMAT = 'application/vnd.sdmx.data+json;version=1.0.0-wd';
export const CSV_DATA_FORMAT = 'application/vnd.sdmx.data+csv';

export const ALL_STUBS_PARAM = '?detail=allstubs';
export const ALL_REFS_PARAM = '?references=all';
export const PARENT_REFS_PARAM = '?references=parentsandsiblings';

export const SUCCESS_DELETE_CODES = [200];
export const WARNING_DELETE_CODES = [202];

export const SUCCESS_POST_CODES = [200, 201];
export const WARNING_POST_CODES = [207, 407];

//---------------------------------------------------------------------------------options

export const getRequestOptions = (space, format) => ({
  mode: 'cors',
  headers: withAuthHeader(space)({ Accept: format }),
});

export const deleteRequestOptions = () => ({
  mode: 'cors',
});

export const postRequestOptions = locale => ({
  mode: 'cors',
  headers: { 'Accept-Language': locale },
});

//----------------------------------------------------------------------structure requests

export const withLog = id => request =>
  request.catch(error => {
    const log = error.response ? error.response.data : error.request.data;
    return { data: null, log: { [id]: log } };
  });

export const getStructureRequest = (options = {}) => {
  const { agency, code, format, queryParam, space, type, version } = options;
  const url = `${space.endpoint}/${type}/${agency}/${code}/${version}/${
    queryParam ? queryParam : ''
  }`;
  return apiManager.get(url, getRequestOptions(space, format));
  //.then(res => res.data)
};

export const withParsedLog = (successCodes, warningCodes) => request =>
  request
    .catch(error => {
      if (error.response) {
        return sdmxXmlErrorParser(error.response.data).then(data => ({ isError: true, data }));
      }
      throw Error(R.path(['response', 'status'], error));
    })
    .then(res => {
      if (res.isError) {
        return res;
      }
      if (R.contains(res.status, successCodes)) {
        return sdmxXmlErrorParser(res.data).then(data => ({ isSuccess: true, data }));
      }
      if (R.contains(res.status, warningCodes)) {
        return sdmxXmlErrorParser(res.data).then(data => ({ isWarning: true, data }));
      }
      throw Error('');
    });

export const postStructureRequest = (options = {}) => {
  const { space, data, locale } = options;
  const url = `${space.endpoint}/structure/`;
  return withParsedLog(
    SUCCESS_POST_CODES,
    WARNING_POST_CODES,
  )(apiManager.post(url, data, postRequestOptions(locale)));
};

export const deleteStructureRequest = (options = {}) => {
  const { agency, code, space, type, version } = options;
  const url = `${space.endpoint}/${type}/${agency}/${code}/${version}/`;
  return withParsedLog(
    SUCCESS_DELETE_CODES,
    WARNING_DELETE_CODES,
  )(apiManager.delete(url, deleteRequestOptions()));
};

export const getArtefactStructure = ({ withReferences, ...rest }) =>
  getStructureRequest({
    ...rest, // { agency, code, space, type, version }
    format: XML_STRUCTURE_FORMAT,
    queryParam: withReferences ? ALL_REFS_PARAM : null,
  }).then(res => res.data);

export const transferStructure = (artefactOptions, destinationSpace) =>
  getArtefactStructure(artefactOptions).then(structure =>
    postStructureRequest({
      locale: R.prop('locale', artefactOptions),
      space: destinationSpace,
      data: structure,
    }),
  );
