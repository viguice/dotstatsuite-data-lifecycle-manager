import sax from 'sax';
import { isEmpty, isNil, trim } from 'ramda';

export const sdmxXmlErrorParser = xml => {
  const strict = false;
  const options = { lowercase: true };
  const parser = sax.parser(strict, options);
  let errors = [];

  return new Promise((resolve, reject) => {
    parser.onerror = reject;
    parser.ontext = text => {
      const message = trim(text);
      if (!isEmpty(message) && !isNil(message)) {
        errors.push(message);
      }
    };
    parser.onend = () => resolve(errors);
    parser.write(xml).close();
  });
};
