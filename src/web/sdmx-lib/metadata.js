import * as R from 'ramda';

export const getMsdReference = annotations =>
  R.pipe(
    R.find(R.propEq('type', 'METADATA')),
    R.propOr(null, 'title'),
  )(annotations);

export const parseMsdReference = ref => {
  const matched = R.match(
    /MetadataStructure=([\w@_.]+):([\w@_.]+)\(([\d.]+)\)/,
    ref,
  );
  if (R.isEmpty(matched)) {
    return null;
  }
  const [, agencyId, code, version] = matched;
  return {
    agencyId,
    code,
    version,
    sdmxId: `${agencyId}:${code}(${version})`,
    type: 'metadatastructure',
  };
};
