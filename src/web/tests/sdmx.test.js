import { getMsdReference, parseMsdReference } from '../sdmx-lib';

describe('sdmx --- msd references tests', () => {
  it('getMsdReference test', () => {
    expect(getMsdReference([{ type: 'random', title: 'test' }])).toEqual(null);
    expect(
      getMsdReference([
        { type: 'random', title: 'test' },
        { type: 'METADATA', title: 'msdRef' },
      ]),
    ).toEqual('msdRef');
  });
  it('parseMsdReference test', () => {
    expect(parseMsdReference('msdRef')).toEqual(null);
    expect(
      parseMsdReference(
        'urn:sdmx:org.sdmx.infomodel.metadatastructure.MetadataStructure=OECD.SDD.SDPS:MSD_SNA_TABLE1(1.0)',
      ),
    ).toEqual({
      sdmxId: 'OECD.SDD.SDPS:MSD_SNA_TABLE1(1.0)',
      agencyId: 'OECD.SDD.SDPS',
      code: 'MSD_SNA_TABLE1',
      version: '1.0',
      type: 'metadatastructure',
    });
  });
});
