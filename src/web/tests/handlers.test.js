import {
  requestCategorisationsSuccessHandler,
  requestCategorisationsErrorHandler,
  requestObservationsSuccessHandler,
  requestObservationsErrorHandler,
  deleteDetailsHandler,
} from '../modules/dataflow-details/action-creators';

describe('dataflow-details tests', () => {
  it('requestCategorisationsSuccessHandler test', () => {
    const state = {
      id: {
        test: 'test',
        isFetchingCategories: true,
      },
    };
    const action = { payload: { id: 'id', categories: ['0', '1'] } };
    const expected = {
      id: {
        test: 'test',
        categories: ['0', '1'],
        isFetchingCategories: false,
      },
    };
    expect(requestCategorisationsSuccessHandler(state, action)).toEqual(expected);
  });
  it('requestCategorisationsErrorHandler test', () => {
    const state = {
      id: {
        test: 'test',
        isFetchingCategories: true,
      },
    };
    const action = { payload: { id: 'id', error: { message: 'error' } } };
    const expected = {
      id: {
        test: 'test',
        categoriesError: { message: 'error' },
        isFetchingCategories: false,
      },
    };
    expect(requestCategorisationsErrorHandler(state, action)).toEqual(expected);
  });
  it('requestObservationsSuccessHandler test', () => {
    const state = {
      id: {
        test: 'test',
        isFetchingObservations: true,
      },
    };
    const action = { payload: { id: 'id', observations: ['0', '1'], series: ['0', '1'] } };
    const expected = {
      id: {
        test: 'test',
        observations: ['0', '1'],
        series: ['0', '1'],
        isFetchingObservations: false,
      },
    };
    expect(requestObservationsSuccessHandler(state, action)).toEqual(expected);
  });
  it('requestObservationsErrorHandler test', () => {
    const state = {
      id: {
        test: 'test',
        isFetchingObservations: true,
      },
    };
    const action = { payload: { id: 'id', error: { message: 'error' } } };
    const expected = {
      id: {
        test: 'test',
        observationsError: { message: 'error' },
        isFetchingObservations: false,
      },
    };
    expect(requestObservationsErrorHandler(state, action)).toEqual(expected);
  });
  it('deleteDetailsHandler test', () => {
    const state = {
      id1: {
        test: 'test',
        categories: ['0', '1'],
        observations: ['0', '1'],
        series: ['0', '1'],
        isFetchingObservations: false,
        isFetchingCategories: false,
      },
      id2: {
        random: 'random',
        categories: ['a', 'b'],
        observations: ['c', 'd'],
        series: ['e', 'f'],
        isFetchingObservations: false,
        isFetchingCategories: false,
      },
    };
    const action = { payload: { id: 'id1' } };
    const expected = {
      id2: {
        random: 'random',
        categories: ['a', 'b'],
        observations: ['c', 'd'],
        series: ['e', 'f'],
        isFetchingObservations: false,
        isFetchingCategories: false,
      },
    };
    expect(deleteDetailsHandler(state, action)).toEqual(expected);
  });
});
