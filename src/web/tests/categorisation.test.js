import { getXmlDataflowCategorisation } from '../modules/categorize/utils';

describe('getXmlDataflowXategorisation tests', () => {
  it('xml escaping', () => {
    const dataflow = {
      code: 'DF',
      agencyId: 'OECD',
      version: '1.0',
      label: `"lots" of 'bad' <things> & stuff`,
    };
    const category = {
      scheme: { code: 'SCHEME', agencyId: 'ILO', version: '2.0' },
      hierarchicalCode: 'C',
    };
    expect(getXmlDataflowCategorisation(dataflow, category, 'en')).toEqual(
      `<structure:Categorisation id="CAT_OECD__DF__1_0@ILO__SCHEME__2_0__C" agencyID="OECD" version="1.0" isFinal="false">
      <common:Name xml:lang="en">&quot;lots&quot; of &apos;bad&apos; &lt;things&gt; &amp; stuff</common:Name>
      <structure:Source>
        <Ref id="DF" version="1.0" agencyID="OECD" package="datastructure" class="Dataflow" />
      </structure:Source>
      <structure:Target>
        <Ref id="C" maintainableParentID="SCHEME" maintainableParentVersion="2.0" agencyID="ILO" package="categoryscheme" class="Category" />
      </structure:Target>
    </structure:Categorisation>`,
    );
  });
});
