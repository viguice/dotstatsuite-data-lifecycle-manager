import React from 'react';
import PropTypes from 'prop-types';
import {
  Classes,
  InputGroup,
  Popover,
  Position,
  Button,
} from '@blueprintjs/core';
import { injectIntl } from 'react-intl';
import { withState, withHandlers, compose, withProps } from 'recompose';
import {
  any,
  append,
  assoc,
  dissoc,
  filter,
  gt,
  includes,
  is,
  isEmpty,
  length,
  map,
  or,
  pipe,
  prop,
  reduce,
  toLower,
  values,
} from 'ramda';
import glamorous from 'glamorous';
import classnames from 'classnames';
import { formatMessage } from '../../modules/i18n';
import messages from '../messages';

const Wrapper = glamorous.div({ padding: 10 });

export const SpotlightFields = injectIntl(({ fields, fieldChange, intl }) => {
  return (
    <Popover
      position={Position.BOTTOM}
      target={<Button className={Classes.MINIMAL} iconName="filter-list" />}
      content={
        <Wrapper
          className={classnames(
            Classes.BUTTON_GROUP,
            Classes.MINIMAL,
            Classes.VERTICAL,
            Classes.ALIGN_LEFT,
          )}
        >
          {map(
            field => (
              <Button
                key={field.id}
                className={Classes.MINIMAL}
                text={formatMessage(intl)(messages[`artefact.${field.id}`])}
                onClick={fieldChange(field)}
                iconName={field.isSelected ? 'tick' : 'blank'}
              />
            ),
            values(fields),
          )}
        </Wrapper>
      }
    />
  );
});

SpotlightFields.propTypes = {
  fieldChange: PropTypes.func,
  fields: PropTypes.objectOf(
    PropTypes.shape({
      id: PropTypes.string,
      isSelected: PropTypes.bool,
    }),
  ),
};

export const Spotlight = injectIntl(
  ({ value, valueChange, fields, fieldChange, intl }) => {
    const clearAll = (
      <Button
        className={Classes.MINIMAL}
        iconName="eraser"
        onClick={valueChange}
      />
    );
    const spotlightFields = gt(length(values(fields)), 1) ? (
      <div>
        <SpotlightFields fields={fields} fieldChange={fieldChange} />
        {clearAll}
      </div>
    ) : (
      clearAll
    );

    return (
      <InputGroup
        leftIconName="search"
        onChange={valueChange}
        placeholder={formatMessage(intl)(messages['spotlight.placeholder'])}
        value={value}
        rightElement={spotlightFields}
      />
    );
  },
);

Spotlight.propTypes = {
  value: PropTypes.string,
  valueChange: PropTypes.func,
  fields: PropTypes.object,
  fieldChange: PropTypes.func,
};

const spotlightHandler = spotlight => item => {
  return any(field => {
    const itemValue = is(Function, field.accessor)
      ? field.accessor(item)
      : prop(field.accessor, item);
    return includes(toLower(spotlight.value), toLower(itemValue));
  }, filter(prop('isSelected'), values(spotlight.fields)));
};

export const setListHandler = spotlight => list => ({
  items: filter(spotlightHandler(spotlight), list.items),
});
export const setTreeHandler = spotlight => list => {
  const recurse = nodes =>
    reduce(
      (acc, node) => {
        let res = node;
        const nodeIsValid = spotlightHandler(spotlight)(node);
        const childNodes = recurse(node.childNodes || []);
        const childrenAreValid = is(Array, childNodes) && !isEmpty(childNodes);
        const isExpanded =
          (!isEmpty(spotlight.value) && childrenAreValid) || node.isExpanded;
        if (!isEmpty(childNodes)) {
          res = pipe(
            assoc('childNodes', childNodes),
            assoc('isExpanded', isExpanded),
          )(res);
        } else {
          res = pipe(dissoc('childNodes'))(res);
        }
        return or(nodeIsValid, childrenAreValid) ? append(res, acc) : acc;
      },
      [],
      nodes,
    );
  return { items: recurse(list.items) };
};

export const withSpotlight = fields =>
  compose(
    withState('spotlight', 'setSpotlight', {
      value: '',
      fields: reduce(
        (memo, field) => ({ ...memo, [field.id]: { ...field } }),
        {},
        fields,
      ),
    }),
    withProps(state => {
      const handler = state.list.isTree ? setTreeHandler : setListHandler;
      return {
        list: handler(state.spotlight)(state.list),
      };
    }),
    withHandlers({
      spotlightValueChange: ({ setSpotlight }) => event => {
        event.preventDefault();
        const value = event.target.value;
        setSpotlight(state => ({ ...state, value }));
      },
      spotlightFieldChange: ({ setSpotlight }) => field => event => {
        event.preventDefault();
        setSpotlight(state => ({
          ...state,
          fields: {
            ...state.fields,
            [field.id]: { ...field, isSelected: !field.isSelected },
          },
        }));
      },
    }),
  );

export const withControlledSpotlight = (fields = []) =>
  compose(
    withProps(props => ({
      spotlight: {
        value: '',
        fields: reduce(
          (memo, field) => ({ ...memo, [field.id]: { ...field } }),
          {},
          fields,
        ),
        ...props.spotlight,
      },
    })),
    withProps(props => {
      const handler = props.list.isTree ? setTreeHandler : setListHandler;
      return {
        list: handler(props.spotlight)(props.list),
      };
    }),
    withHandlers({
      spotlightValueChange: ({ changeSpotlight, spotlight }) => event => {
        event.preventDefault();
        const value = event.target.value;
        changeSpotlight({ ...spotlight, value });
      },
      spotlightFieldChange: ({
        changeSpotlight,
        spotlight,
      }) => field => event => {
        event.preventDefault();
        changeSpotlight({
          ...spotlight,
          fields: {
            ...spotlight.fields,
            [field.id]: { ...field, isSelected: !field.isSelected },
          },
        });
      },
    }),
  );
