import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import glamorous from 'glamorous';
import { Classes } from '@blueprintjs/core';
import { Pagination, PaginationStatus } from './';

const StyledNav = glamorous.nav({
  padding: '0 !important',
  boxShadow: 'none !important',
  zIndex: '0 !important',
  backgroundColor: 'transparent  !important',
});

const PaginationBlock = ({ pagination, pageChange }) => {
  const { pages } = pagination;
  if (pages < 2) {
    return null;
  }
  return (
    <StyledNav className={classnames(Classes.NAVBAR)}>
      <div className={classnames(Classes.NAVBAR_GROUP, Classes.ALIGN_LEFT)}>
        <Pagination {...pagination} pageChange={pageChange} />
      </div>
      <div className={classnames(Classes.NAVBAR_GROUP, Classes.ALIGN_RIGHT)}>
        <PaginationStatus pages={pages} />
      </div>
    </StyledNav>
  );
};
PaginationBlock.propTypes = {
  pagination: PropTypes.object,
  pageChange: PropTypes.func,
};

export default PaginationBlock;
