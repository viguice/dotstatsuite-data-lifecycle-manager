import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import glamorous from 'glamorous';
import { withState, withProps, compose } from 'recompose';
import { is, map, values } from 'ramda';

export {
  Spotlight,
  withSpotlight,
  withControlledSpotlight,
  setListHandler as spotlightHandler,
} from './spotlight';
export { Pagination, withPagination, PaginationStatus, setPaginationHandler } from './pagination';
export { default as PaginationBlock } from './pagination-block';
export { Status, withStatus } from './status';
export {
  Sort,
  withSort,
  getSortDirection,
  getSortFields,
  setListHandler as sortListHandler,
} from './sort';

const ListWrapper = glamorous.div({
  display: 'flex',
  flexDirection: 'column',
});

export const List = ({ items, itemRenderer, handlers, accessors }) => {
  const idAccessor = R.defaultTo(R.prop('id'), R.prop('id', accessors));

  return (
    <ListWrapper>
      {map(item => {
        const Renderer = item.renderer || itemRenderer;
        if (!Renderer) {
          console.log('no renderer found', 'item.id', item.id); // eslint-disable-line no-console
          return null;
        }
        return (
          <Renderer key={idAccessor(item)} {...item} handlers={handlers} accessors={accessors} />
        );
      }, items)}
    </ListWrapper>
  );
};

List.propTypes = {
  items: PropTypes.array,
  itemRenderer: PropTypes.func,
  handlers: PropTypes.objectOf(PropTypes.func),
  accessors: PropTypes.objectOf(PropTypes.func),
};

export const withList = ({ isTree } = {}) =>
  compose(
    withState('list', 'setList', { isTree }),
    withProps(({ items, list }) => {
      const _items = is(Object, items) ? values(items) : items;
      return { list: { ...list, items: _items, _items } };
    }),
  );
