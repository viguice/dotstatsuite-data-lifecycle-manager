import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { Intent, Toast } from '@blueprintjs/core';
import { formatMessage } from '../../modules/i18n';

export const ActionLog = ({ className, log, resetLog, intl }) => {
  if (R.isNil(log)) {
    return null;
  }
  const intent = Intent[log.type === 'ERROR' ? 'DANGER' : log.type];
  const title = formatMessage(intl)({ id: `user.right.${log.action}.${R.toLower(log.type)}` });

  return (
    <Toast
      className={className}
      intent={intent}
      onDismiss={() => resetLog()}
      timeout={0}
      message={
        <div>
          <div>{title}</div>
          {R.isNil(log.message) ? '' : log.message}
        </div>
      }
    />
  );
};

ActionLog.propTypes = {
  className: PropTypes.string,
  intl: PropTypes.object,
  log: PropTypes.object,
  resetLog: PropTypes.func,
};
