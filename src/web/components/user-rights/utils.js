import * as R from 'ramda';
import { formatMessage } from '../../modules/i18n';
import { artefactTypeIds, permissions as granularPermissions } from '../../lib/permissions';
import messages from '../messages';

export const getPermissionLabel = (intl, permission) => {
  if (R.has(`permission.${permission}`, messages)) {
    return formatMessage(intl)(R.prop(`permission.${permission}`, messages));
  }
  const permissionGroups = R.pathOr([], ['sdmx', 'permissionGroups'], window.SETTINGS);
  const permissionGroup = R.find(
    ({ permissions }) => R.equals(R.sum(permissions), permission),
    permissionGroups,
  );
  if (permissionGroup) {
    const id = permissionGroup.id;
    return formatMessage(intl)({ id: `permission.group.${id}` });
  }
  return formatMessage(intl)(R.prop('user.right.custom.permissions', messages));
};

export const getArtefactTypeLabel = (typeIndex, intl) => {
  const typeId = R.nth(typeIndex, artefactTypeIds);
  return R.has(`artefact.type.${typeId}`, messages)
    ? formatMessage(intl)(R.prop(`artefact.type.${typeId}`, messages))
    : typeId;
};

export const isPredefinedPermission = permission => {
  if (R.includes(permission, granularPermissions)) {
    return true;
  }
  const permissionGroups = R.pathOr([], ['sdmx', 'permissionGroups'], window.SETTINGS);
  const matchingGroup = R.find(
    ({ permissions }) => R.equals(R.sum(permissions), permission),
    permissionGroups,
  );
  return !!matchingGroup;
};

export const getLabel = (intl, id) => {
  if (R.equals('*', id)) {
    return formatMessage(intl)(R.prop('user.right.scope.any', messages));
  }
  return id;
};

const validator = (whitelist, exceptValue = null) => value =>
  R.isEmpty(whitelist) || R.has(value, whitelist) || R.equals(value, exceptValue);
export const filterRights = (rights, selection) =>
  R.filter(
    R.where({
      dataSpace: validator(selection.spaces, '*'),
      artefactType: validator(selection.types, 0),
      userMask: validator(selection.users, '*'),
      artefactAgencyId: validator(selection.agencies, '*'),
      artefactId: validator(selection.artefacts, '*'),
      artefactVersion: validator(selection.versions, '*'),
      permission: validator(selection.permissionTypes),
    }),
    rights,
  );

const filter = (validator, list) =>
  R.pipe(
    R.reduce((acc, value) => {
      if (!validator(value)) {
        return acc;
      }
      return R.assoc(value, { id: value }, acc);
    }, {}),
    R.values,
  )(list);
export const getFilterOptions = rights =>
  R.pipe(
    R.map(
      R.props([
        'dataSpace',
        'artefactType',
        'userMask',
        'artefactAgencyId',
        'artefactId',
        'artefactVersion',
        'permission',
      ]),
    ),
    R.transpose,
    transposed => {
      return {
        spaces: filter(v => !R.equals('*', v), R.nth(0, transposed) || []),
        types: filter(v => !R.equals(0, v), R.nth(1, transposed) || []),
        users: filter(v => !R.equals('*', v), R.nth(2, transposed) || []),
        agencies: filter(v => !R.equals('*', v), R.nth(3, transposed) || []),
        artefacts: filter(v => !R.equals('*', v), R.nth(4, transposed) || []),
        versions: filter(v => !R.equals('*', v), R.nth(5, transposed) || []),
        permissionTypes: filter(isPredefinedPermission, R.nth(6, transposed) || []),
      };
    },
  )(rights);
