import React, { useState } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { Radio, RadioGroup } from '@blueprintjs/core';
import { UserRightForm as UserRightFormUI } from '@sis-cc/dotstatsuite-visions';
import { makeStyles } from '@material-ui/styles';
import Dialog from '@material-ui/core/Dialog';
import EditIcon from '@material-ui/icons/Edit';
import SecurityIcon from '@material-ui/icons/Security';
import { getSdmxTypeIds } from '../../modules/config/selectors';
import { artefactTypeRightsDefinition, permissions } from '../../lib/permissions';
import { useSelector } from 'react-redux';
import { formatMessage } from '../../modules/i18n';
import messages from '../messages';
import { useIntl } from 'react-intl';

const useStyles = makeStyles(() => ({
  header: {
    padding: '5px 16px',
    borderBottom: '1px solid grey',
    '& .MuiSvgIcon-root': {
      color: 'grey',
    },
  },
}));

export const UserRightForm = ({
  onClose,
  onSubmit,
  permission,
  isOpen,
  spaces,
  isPartialEdition,
}) => {
  const intl = useIntl();
  const classes = useStyles();
  const getDefaultMode = R.cond([
    [R.has('id'), R.ifElse(R.propEq('isGroup', true), R.always('group'), R.always('user'))],
    [R.T, R.always('group')],
  ]);
  const [mode, setMode] = useState(getDefaultMode(permission));
  const handleChange = val => setMode(val);

  let titleKey = R.isNil(permission.id) ? 'user.right.add' : 'user.right.edit';
  let icon = <EditIcon />;
  if (!R.is(Function, onSubmit)) {
    titleKey = 'user.right.view';
    icon = <SecurityIcon />;
  }
  const title = formatMessage(intl)(R.prop(titleKey, messages), { id: permission.id });
  const labels = {
    title: formatMessage(intl)(R.prop('user.right.permissions', messages)),
    user: formatMessage(intl)(R.prop('user.right.user', messages)),
    dataspace: formatMessage(intl)(R.prop('user.right.space', messages)),
    artefactType: formatMessage(intl)(R.prop('user.right.artefact.type', messages)),
    maintenanceAgencyID: formatMessage(intl)(R.prop('user.right.artefact.agency', messages)),
    artefactID: formatMessage(intl)(R.prop('user.right.artefact.id', messages)),
    artefactVersion: formatMessage(intl)(R.prop('user.right.artefact.version', messages)),
    permission: formatMessage(intl)(R.prop('user.right.permission', messages)),
    definition: formatMessage(intl)(R.prop('user.right.permission.definition', messages)),
    cancel: formatMessage(intl)(R.prop('action.cancel', messages)),
    submit: formatMessage(intl)(
      R.prop(R.isNil(permission.id) ? 'action.add' : 'action.edit', messages),
    ),
    customPermissions: formatMessage(intl)(R.prop('user.right.custom.permissions', messages)),
    customPermissionsDefinition: formatMessage(intl)(
      R.prop('user.right.custom.permissions.definition', messages),
    ),
    group: formatMessage(intl)(R.prop('user.right.group', messages)),
  };
  const modes = [
    {
      value: 'group',
      label: R.prop('group')(labels),
      disabled: isPartialEdition,
    },
    {
      value: 'user',
      label: R.prop('user')(labels),
      disabled: isPartialEdition,
    },
  ];
  const artefactTypesIds = useSelector(getSdmxTypeIds);
  const artefactTypes = R.pipe(
    R.prepend('any'),
    R.map(typeId => {
      const artefactType = R.propOr({}, typeId, artefactTypeRightsDefinition);
      if (R.isEmpty(artefactType)) {
        return {};
      }
      const label = R.has(`artefact.type.${typeId}`, messages)
        ? formatMessage(intl)(R.prop(`artefact.type.${typeId}`, messages))
        : typeId;

      return { label, value: String(artefactType.id) };
    }),
    R.reject(R.isEmpty),
  )(artefactTypesIds);

  const spacesOptions = R.pipe(
    R.propOr([], R.prop('authzServerUrl', permission)),
    R.map(({ id, label }) => ({ value: id, label })),
    R.prepend({ value: '*', label: '*' }),
  )(spaces);

  const permissionsOptions = R.map(
    id => ({
      id,
      isSelected: (R.prop('permission', permission) & id) === id,
      label: formatMessage(intl)(messages[`permission.${id}`]),
      definition: formatMessage(intl)(messages[`permission.${id}.definition`]),
    }),
    permissions,
  );

  const permissionGroupOptions = R.pipe(
    R.pathOr([], ['sdmx', 'permissionGroups']),
    R.map(({ id, permissions }) => ({
      id,
      label: formatMessage(intl)({ id: `permission.group.${id}` }),
      definition: formatMessage(intl)({ id: `permission.group.${id}.definition` }),
      options: permissions,
    })),
  )(window.SETTINGS);

  const handleOnSubmit = ({ checkedPermissions, ...rest }) => {
    if (!R.is(Function, onSubmit)) {
      return null;
    }
    const formattedPermission = R.sum(checkedPermissions);
    const authzServerUrl = R.prop('authzServerUrl', permission);
    if (isPartialEdition) {
      const _perm = R.pipe(
        R.assoc('permission', formattedPermission),
        R.dissoc('authzServerUrl'),
      )(permission);
      onSubmit({ authzServerUrl, permission: _perm });
    } else {
      onSubmit({
        authzServerUrl,
        permission: {
          permission: formattedPermission,
          userMask: rest.user,
          artefactAgencyId: rest.maintenanceAgencyID,
          artefactId: rest.artefactID,
          artefactType: Number(rest.artefactType),
          artefactVersion: rest.artefactVersion,
          dataSpace: rest.dataspace,
          isGroup: rest.isGroup,
        },
      });
    }
  };
  return (
    <Dialog open={isOpen} onClose={onClose}>
      <div className={classes.header}>
        {icon}
        {title}
      </div>
      <UserRightFormUI
        onSubmit={R.is(Function, onSubmit) ? handleOnSubmit : null}
        onCancel={onClose}
        onChange={handleChange}
        mode={mode}
        modes={modes}
        artefactTypes={artefactTypes}
        permissionTabs={{
          options: {
            id: 'granular',
            label: formatMessage(intl)(R.prop('user.right.granular.permissions', messages)),
            data: permissionsOptions,
          },
          permissions: {
            id: 'groups',
            label: formatMessage(intl)(R.prop('user.right.group.permissions', messages)),
            data: permissionGroupOptions,
          },
        }}
        dataSpaces={spacesOptions}
        dataspace={{ value: R.prop('dataSpace', permission), disabled: isPartialEdition }}
        labels={labels}
        user={{
          value: R.prop('userMask', permission),
          disabled: isPartialEdition,
        }}
        artefactType={{
          value: String(R.prop('artefactType', permission)),
          disabled: isPartialEdition,
        }}
        maintenanceAgencyID={{
          value: R.prop('artefactAgencyId', permission),
          disabled: isPartialEdition,
        }}
        artefactID={{ value: R.prop('artefactId', permission), disabled: isPartialEdition }}
        artefactVersion={{
          value: R.prop('artefactVersion', permission),
          disabled: isPartialEdition,
        }}
      />
    </Dialog>
  );
};

UserRightForm.propTypes = {
  isOpen: PropTypes.bool,
  isPartialEdition: PropTypes.bool,
  onClose: PropTypes.func,
  onSubmit: PropTypes.func,
  permission: PropTypes.object,
  spaces: PropTypes.object,
};

const ManageForm = ({ onClose, onSubmit, permission, isOpen, spaces, simpleView }) => {
  const intl = useIntl();
  const [selectedAuthz, setSelectedAuthz] = useState(null);
  const hasMultipleAuthz = R.pipe(R.keys, R.length, R.lt(1))(spaces);
  if (!isOpen) {
    return null;
  }
  if (!R.has('id', permission) && hasMultipleAuthz && R.isNil(selectedAuthz)) {
    let temp = null;
    return (
      <Dialog
        applyLabel={formatMessage(intl)(R.prop('action.select', messages))}
        title={formatMessage(intl)(R.prop('user.right.add', messages))}
        isOpen={true}
        onClose={onClose}
        onApply={setSelectedAuthz}
      >
        <RadioGroup onChange={e => (temp = e.target.value)} selectedValue={temp}>
          {R.map(
            authz => (
              <Radio key={authz} label={authz} value={authz} />
            ),
            R.keys(spaces),
          )}
        </RadioGroup>
      </Dialog>
    );
  }
  const handleOnSubmit = props => {
    setSelectedAuthz(null);
    onSubmit(props);
  };
  return (
    <UserRightForm
      permission={{
        ...permission,
        authzServerUrl: selectedAuthz || R.head(R.keys(spaces)),
      }}
      isPartialEdition={R.has('id', permission)}
      isOpen={true}
      spaces={spaces}
      onClose={onClose}
      onSubmit={simpleView ? null : handleOnSubmit}
    />
  );
};

ManageForm.propTypes = {
  isOpen: PropTypes.bool,
  isPartialEdition: PropTypes.bool,
  onClose: PropTypes.func,
  onSubmit: PropTypes.func,
  permission: PropTypes.object,
  simpleView: PropTypes.bool,
  spaces: PropTypes.object,
};

export default ManageForm;
