import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import { Checkbox, Classes, Colors, EditableText, Radio, RadioGroup } from '@blueprintjs/core';
import Layers from '@material-ui/icons/Layers';
import * as R from 'ramda';
import Dialog from './Dialog';
import { formatMessage } from '../../modules/i18n';
import { exportData } from '../../modules/export-data';
import messages from '../messages';

const Item = glamorous.div({
  marginBottom: 10,
  padding: '0 !important',
});

const StyledCheckbox = glamorous(Checkbox)({
  lineHeight: '20px !important',
  marginBottom: '0px !important',
  minHeight: '20px !important',
  '& .pt-control-indicator': {
    top: '2px !important',
  },
});

const Header = glamorous.div({
  display: 'flex',
  alignItems: 'center',
});

const Content = glamorous.div({
  padding: 12,
});

const ContentIcon = glamorous(Layers)({
  color: Colors.GRAY1,
});

const InputContainer = glamorous.div(
  {
    display: 'flex',
    flexDirection: 'column',
    marginTop: 10,
  },
  ({ hasDataQuery }) => {
    if (!hasDataQuery) {
      return { display: 'none' };
    }
    return {};
  },
);

const Input = glamorous(EditableText)({
  backgroundColor: 'white',
  minWidth: 100,
  height: 20,
});

const QueryInfo = glamorous.span({
  marginBottom: 10,
});

const InvalidDataqueryError = glamorous.span({
  backgroundColor: 'red',
  color: 'white',
  maxWidth: '100% !important',
  marginTop: 10,
  padding: 10,
});

const METADATA_CONTENT_TYPES = ['bothCsv', 'metadataCsv'];

const ExportDataDialog = ({ onClose, selection, exportCallback }) => {
  const intl = useIntl();
  const dispatch = useDispatch();
  const [dataquery, setDataquery] = useState({ isActive: false, value: null });
  const [contentType, setContentType] = useState('bothCsv');

  const checkDataquery = () =>
    R.pipe(R.evolve({ isActive: R.not, value: R.always(null) }), setDataquery)(dataquery);

  const changeDataquery = value =>
    R.pipe(R.set(R.lensProp('value'), R.isEmpty(value) ? null : value), setDataquery)(dataquery);

  let isValidDataquery = true;
  if (R.includes(contentType, METADATA_CONTENT_TYPES) && dataquery.isActive) {
    const hasPlus = R.includes('+', dataquery.value || '');
    isValidDataquery = !hasPlus;
  }

  const onExport = () => {
    onClose();
    dispatch(exportData({ selection, contentType, dataquery: dataquery.value }));
    if (R.is(Function, exportCallback)) {
      exportCallback();
    }
  };

  return (
    <Dialog
      applyLabel="export"
      iconName="download"
      isOpen={true}
      onApply={isValidDataquery ? onExport : null}
      onClose={onClose}
      title={formatMessage(intl)(messages['export.data.metadata'])}
    >
      <Item className={Classes.CARD}>
        <Header className={Classes.CALLOUT}>
          <ContentIcon />
          {formatMessage(intl)(R.prop('content.type', messages))}
        </Header>
        <Content>
          <RadioGroup onChange={e => setContentType(e.target.value)} selectedValue={contentType}>
            <Radio
              key="bothCsv"
              label={formatMessage(intl)(messages['export.data.metadata.csv'])}
              value="bothCsv"
            />
            <Radio
              key="dataCsv"
              label={formatMessage(intl)(messages['export.data.csv'])}
              value="dataCsv"
            />
            <Radio
              key="dataXml"
              label={formatMessage(intl)(messages['export.data.xml'])}
              value="dataXml"
            />
            <Radio
              key="metadataCsv"
              label={formatMessage(intl)(messages['export.metadata.csv'])}
              value="metadataCsv"
            />
          </RadioGroup>
        </Content>
      </Item>
      <StyledCheckbox
        checked={dataquery.isActive}
        label={formatMessage(intl)(messages['transfer.dataquery'])}
        onChange={checkDataquery}
      />
      <InputContainer hasDataQuery={dataquery.isActive}>
        <QueryInfo>{formatMessage(intl)(messages['export.dataquery.info'])}</QueryInfo>
        <Input
          onChange={changeDataquery}
          placeholder={formatMessage(intl)(messages['transfer.dataquery.placeholder'])}
          selectAllOnFocus={true}
          value={dataquery.value || ''}
        />
        {!isValidDataquery && (
          <InvalidDataqueryError>
            {formatMessage(intl)(messages['export.dataquery.metadata.invalid'])}
          </InvalidDataqueryError>
        )}
      </InputContainer>
    </Dialog>
  );
};

ExportDataDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  selection: PropTypes.array,
  exportCallback: PropTypes.func,
};

export default ExportDataDialog;
