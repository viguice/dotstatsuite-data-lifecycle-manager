import React from 'react';
import { useIntl } from 'react-intl';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import * as R from 'ramda';
import { Button, Classes, Dialog as BlueDialog } from '@blueprintjs/core';
import { formatMessage } from '../../modules/i18n';
import messages from '../messages';

const GDialog = glamorous(BlueDialog)({ width: '40% !important' });

const Dialog = ({ applyLabel, children, onApply, onClose, withFooter = true, ...rest }) => {
  const intl = useIntl();

  return (
    <GDialog {...rest} onClose={onClose}>
      <div className={Classes.DIALOG_BODY}>{children}</div>
      {withFooter && (
        <div className={Classes.DIALOG_FOOTER}>
          <div className={Classes.DIALOG_FOOTER_ACTIONS}>
            <Button onClick={onClose}>
              <span>{formatMessage(intl)(messages['action.cancel'])}</span>
            </Button>
            <Button
              className={Classes.INTENT_PRIMARY}
              disabled={!R.is(Function, onApply)}
              onClick={R.is(Function, onApply) ? onApply : () => {}}
            >
              <span>{applyLabel}</span>
            </Button>
          </div>
        </div>
      )}
    </GDialog>
  );
};

Dialog.propTypes = {
  applyLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  children: PropTypes.oneOfType([PropTypes.array, PropTypes.element]),
  onApply: PropTypes.func,
  onClose: PropTypes.func.isRequired,
  withFooter: PropTypes.bool,
};

export default Dialog;
