import React from 'react';
import RefreshIcon from '@material-ui/icons/Refresh';
import { IconButton } from '@material-ui/core';
import useTransferLogs from '../../../hooks/useTransferLogs';

const RefreshLogs = () => {
  const { refetch } = useTransferLogs();

  return (
    <IconButton size="small" aria-label="refresh" onClick={refetch}>
      <RefreshIcon />
    </IconButton>
  );
};

export default RefreshLogs;
