import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import Table from '@material-ui/core/Table';
import TableContainer from '@material-ui/core/TableContainer';
import TableBody from '@material-ui/core/TableBody';
import Header from './header';
import Log from '../log';

const headerCells = [
  { id: 'space', prop: 'destinationDataspace' },
  { id: 'request', prop: 'requestId' },
  { id: 'action', prop: 'action' },
  { id: 'user', prop: 'userEmail' },
  { id: 'structure', prop: 'artefact' },
  { id: 'submissionTime', prop: 'submissionTime' },
  { id: 'execution.status', prop: 'executionStatus' },
  { id: 'execution.outcome', prop: 'outcome' },
  { id: 'execution.start', prop: 'executionStart' },
  { id: 'execution.end', prop: 'executionEnd' },
];

const Logs = ({ logs, sort }) => {
  return (
    <TableContainer>
      <Table size="small" padding="none">
        <Header
          order={sort.order}
          orderBy={sort.orderBy}
          onSort={sort.handleSort}
          cells={headerCells}
        />
        <TableBody>
          {R.map(
            log => (
              <Log key={log.requestId} data={log} detailsSpan={R.length(headerCells)} />
            ),
            logs,
          )}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

Logs.propTypes = {
  logs: PropTypes.array,
  sort: PropTypes.object,
};

export default Logs;
