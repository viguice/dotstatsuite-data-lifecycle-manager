import React from 'react';
import * as R from 'ramda';
import { NonIdealState } from '@blueprintjs/core';
import { Wrapper } from '../../artefacts';
import { getSelectedInternalSpaces } from '../../../modules/filters/selectors';
import { useSelector } from 'react-redux';
import RefreshLogs from './refresh';
import { FormattedMessage } from '../../../modules/i18n';
import { Grid } from '@material-ui/core';

const BlankLogs = () => {
  const spaces = useSelector(getSelectedInternalSpaces);

  const messageKey = R.cond([
    [R.isEmpty, R.always('no.space')],
    [R.T, R.always('no.log')],
  ])(spaces);

  return (
    <Wrapper>
      <NonIdealState
        visual="square"
        title={<FormattedMessage id={`logs.list.blank.${messageKey}`} />}
      />
      {!R.isEmpty(spaces) && (
        <Grid container justifyContent="center">
          <Grid item>
            <RefreshLogs />
          </Grid>
        </Grid>
      )}
    </Wrapper>
  );
};

export default BlankLogs;
