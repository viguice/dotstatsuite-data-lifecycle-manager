import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { TableCellHeader as TableCell } from '../common/table-cell';
import { FormattedMessage } from '../../../modules/i18n';
import { TableSortLabel } from '@material-ui/core';

const Header = ({ cells = {}, order, orderBy, onSort = R.identity }) => {
  return (
    <TableHead>
      <TableRow>
        <TableCell />
        {R.map(cell => {
          return (
            <TableCell
              key={cell.id}
              sortDirection={orderBy === cell.prop ? order : false}
              style={{ whiteSpace: 'nowrap' }}
            >
              <TableSortLabel
                active={orderBy === cell.prop}
                direction={orderBy === cell.prop ? order : 'asc'}
                onClick={() => onSort(cell.prop)}
              >
                <FormattedMessage id={`logs.list.${cell.id}`} />
              </TableSortLabel>
            </TableCell>
          );
        }, cells)}
      </TableRow>
    </TableHead>
  );
};

Header.propTypes = {
  order: PropTypes.oneOf(['asc', 'desc']),
  orderBy: PropTypes.string,
  onSort: PropTypes.func,
  cells: PropTypes.array,
};

export default Header;
