import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import TableRow from '@material-ui/core/TableRow';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import ExecutionStatus from '../log/execution-status';
import ExecutionOutcome from '../log/execution-outcome';
import DateTime from '../common/date-time';
import { TableCell } from '../common/table-cell';
import Details from '../details';
import { useSelector } from 'react-redux';
import { getSpaces } from '../../../modules/config';
import Space from '../../common/space';
import { FormattedMessage } from '../../../modules/i18n';
import { getActionId } from '../list/utils';

const Log = ({ data, detailsSpan = 1 }) => {
  const [open, setOpen] = React.useState(false);
  const spaces = useSelector(getSpaces());
  const details = data?.logs;
  const hasDetails = !R.isEmpty(details);
  const space = spaces[data?.destinationDataspace];
  const hasSpace = !R.isNil(space);

  return (
    <React.Fragment>
      <TableRow hover>
        <TableCell>
          <IconButton
            size="small"
            onClick={() => setOpen(!open)}
            style={{ visibility: hasDetails }}
          >
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell>{hasSpace && <Space {...space} />}</TableCell>
        <TableCell>{data?.requestId}</TableCell>
        <TableCell>
          <FormattedMessage id={`logs.filters.action.${getActionId(data)}`} />
        </TableCell>
        <TableCell>{data?.userEmail}</TableCell>
        <TableCell>{data?.artefact}</TableCell>
        <TableCell>
          <DateTime isodate={data?.submissionTime} />
        </TableCell>
        <TableCell>
          <ExecutionStatus value={data?.executionStatus} />
        </TableCell>
        <TableCell>
          <ExecutionOutcome value={data?.outcome} />
        </TableCell>
        <TableCell>
          <DateTime isodate={data?.executionStart} />
        </TableCell>
        <TableCell>
          <DateTime isodate={data?.executionEnd} />
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ padding: 0 }} />
        <TableCell colSpan={detailsSpan}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Details data={details} />
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
};

Log.propTypes = {
  data: PropTypes.object,
  detailsSpan: PropTypes.number,
};

export default Log;
