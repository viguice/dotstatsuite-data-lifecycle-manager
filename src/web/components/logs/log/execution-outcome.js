import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { Tag, Intent } from '@blueprintjs/core';

const OUTCOMES = {
  Success: 'SUCCESS',
  Warning: 'WARNING',
  Error: 'DANGER',
  None: 'NONE',
};

const ExecutionOutcome = ({ value }) => {
  if (R.isNil(value) || R.isEmpty(value)) return null;

  const intent = OUTCOMES[value];
  if (R.isNil(intent)) return null;

  return <Tag intent={Intent[intent]}>{value}</Tag>;
};

ExecutionOutcome.propTypes = {
  value: PropTypes.string,
};

export default ExecutionOutcome;
