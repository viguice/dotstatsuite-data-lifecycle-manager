import React from 'react';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { TableCellDetailHeader as TableCell } from '../common/table-cell';
import { FormattedMessage } from '../../../modules/i18n';

// No map, not data driven with an array of object
// Declarative and explicit: easy to customize, easy to understand

const Header = () => {
  return (
    <TableHead>
      <TableRow>
        <TableCell>
          <FormattedMessage id="logs.detail.server" />
        </TableCell>
        <TableCell>
          <FormattedMessage id="logs.detail.date" />
        </TableCell>
        <TableCell>
          <FormattedMessage id="logs.detail.status" />
        </TableCell>
        <TableCell>
          <FormattedMessage id="logs.detail.message" />
        </TableCell>
      </TableRow>
    </TableHead>
  );
};

export default Header;
