import React from 'react';
import PropTypes from 'prop-types';
import { TableCellDetail as TableCell } from '../common/table-cell';
import TableRow from '@material-ui/core/TableRow';
import DateTime from '../common/date-time';

const Detail = ({ data }) => {
  return (
    <TableRow hover>
      <TableCell>{data?.server}</TableCell>
      <TableCell>
        <DateTime isodate={data?.date} />
      </TableCell>
      <TableCell>{data?.level}</TableCell>
      <TableCell style={{ overflowWrap: 'anywhere' }}>{data?.message}</TableCell>
    </TableRow>
  );
};

Detail.propTypes = {
  data: PropTypes.object,
};

export default Detail;
