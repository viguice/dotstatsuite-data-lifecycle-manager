import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { Checkbox, Classes } from '@blueprintjs/core';
import { injectIntl } from 'react-intl';
import { FormattedMessage, formatMessage } from '../../modules/i18n';
import { withUploadPath } from '../../modules/upload-data';
import glamorous from 'glamorous';
import messages from '../messages';

const Container = glamorous.div({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  marginBottom: 0,
  marginLeft: 0,
  marginRight: 0,
  marginTop: 10,
  width: '100%',
});

const Input = glamorous.div({
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'no-wrap',
  alignItems: 'center',
  justifyContent: 'space-between',
  '& span': {
    marginRight: 10,
  },
  '& input': {
    flexGrow: 1,
  },
  width: '100%',
});

const Path = ({ byPath, changeFilePath, filepath, intl, toggleByPath }) => (
  <Container>
    <Checkbox
      checked={byPath}
      label={<FormattedMessage id="upload.path.checkbox" />}
      onChange={toggleByPath}
    />
    {byPath ? (
      <Input>
        <FormattedMessage id="upload.path" />
        <input
          className={`${Classes.INPUT}`}
          onChange={e => changeFilePath(e.target.value)}
          placeholder={formatMessage(intl)(messages['upload.path.placeholder'])}
          type="text"
          value={filepath}
        />
      </Input>
    ) : null}
  </Container>
);

Path.propTypes = {
  byPath: PropTypes.bool,
  changeFilePath: PropTypes.func.isRequired,
  filepath: PropTypes.string,
  intl: PropTypes.object,
  toggleByPath: PropTypes.func.isRequired,
};

export default compose(withUploadPath, injectIntl)(Path);
