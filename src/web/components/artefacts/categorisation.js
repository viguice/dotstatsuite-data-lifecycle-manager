import React from 'react';
import PropTypes from 'prop-types';
import { Button, Classes, Dialog, Intent, Tree } from '@blueprintjs/core';
import { injectIntl } from 'react-intl';
import { formatMessage } from '../../modules/i18n';
import glamorous from 'glamorous';
import { renameProps, compose, withProps } from 'recompose';
import * as R from 'ramda';
import { withList } from '../list';
import { withCategorisation } from '../../modules/categorize';
import messages from '../messages';

const Footer = glamorous.div({
  display: 'flex',
  justifyContent: 'flex-end',
});
const adjustLabel = categories => {
  return R.map(category => ({
    ...category,
    label: `(${category.code}) ${category.label}`,
    childNodes: category.childNodes && adjustLabel(category.childNodes),
  }))(categories);
};
const CategorisationMenu = ({
  isOpenCategorisationMenu,
  categorize,
  closeArtefactCategorisation,
  intl,
  contents,
  ...rest
}) => {
  return (
    <Dialog
      isOpen={isOpenCategorisationMenu}
      onClose={closeArtefactCategorisation}
      title={formatMessage(intl)(messages['categorise.artefact'])}
      style={{ minWidth: '650px' }}
    >
      <div className={Classes.DIALOG_BODY}>
        <Tree {...rest} contents={adjustLabel(contents)} />
      </div>
      <Footer className={Classes.DIALOG_FOOTER}>
        <Button
          style={{ marginRight: '10px' }}
          disabled={false}
          intent={Intent.PRIMARY}
          onClick={categorize}
          text={formatMessage(intl)(messages['action.categorise'])}
        />
        <Button
          onClick={closeArtefactCategorisation}
          text={formatMessage(intl)(messages['action.close'])}
        />
      </Footer>
    </Dialog>
  );
};

CategorisationMenu.propTypes = {
  isOpenCategorisationMenu: PropTypes.bool,
  categorize: PropTypes.func,
  closeArtefactCategorisation: PropTypes.func,
};

export default compose(
  withCategorisation,
  renameProps({ categories: 'items' }),
  withList({ isTree: true }),
  withProps(({ items }) => ({ isBlank: R.isEmpty(items) })),
  renameProps({
    toggleCategory: 'onNodeClick',
    items: 'contents',
  }),
  withProps(({ expandToggleCategory }) => ({
    onNodeExpand: expandToggleCategory,
    onNodeCollapse: expandToggleCategory,
  })),
  injectIntl,
)(CategorisationMenu);
