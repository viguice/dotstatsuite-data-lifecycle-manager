import React from 'react';
import PropTypes from 'prop-types';
import { Button, Classes } from '@blueprintjs/core';
import { is } from 'ramda';

const MoreButton = ({ id, isActive, request }) => {
  if (is(Function, request)) {
    return (
      <Button
        className={Classes.MINIMAL}
        iconName="more"
        active={isActive}
        onClick={() => request(id)}
      />
    );
  }
  return null;
};

MoreButton.propTypes = {
  id: PropTypes.string,
  isActive: PropTypes.bool,
  request: PropTypes.func.isRequired,
};

export default MoreButton;
