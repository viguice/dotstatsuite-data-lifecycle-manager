import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import * as R from 'ramda';
import { useSelector } from 'react-redux';
import { compose, mapProps } from 'recompose';
import { Intent, Icon } from '@blueprintjs/core';
import DescriptionIcon from '@material-ui/icons/Description';
import AddIcon from '@material-ui/icons/AddToPhotos';
import { injectIntl } from 'react-intl';
import { withTransferArtefact } from '../../modules/transfer-artefact';
import { withTransferData } from '../../modules/transfer-data';
import DeleteForever from '@material-ui/icons/DeleteForever';
import AutorenewIcon from '@material-ui/icons/Autorenew';
import Category from '@material-ui/icons/Category';
import PowerIcon from '@material-ui/icons/Power';
import { formatMessage } from '../../modules/i18n';
import { getExportDataLog, getExportMetadataLog } from '../../modules/export-data/selectors';
import { withCategorisationState } from '../../modules/categorize';
import { ERROR, SUCCESS, WARNING } from '../../modules/common/constants';
import { getDsdLinkLog, getInitDataflowsLog } from '../../modules/defineMSD/selectors';
import { getArtefactIndexationLog } from '../../modules/sfsIndex/selectors';
import messages from '../messages';
import Dialog from './item-log-dialog';

const ActivateIcon = glamorous(PowerIcon)(
  {
    marginLeft: 4,
    marginRight: 4,
    fontSize: '20px !important',
  },
  ({ type }) => ({
    color: type === ERROR ? '#db3737' : 'green',
  }),
);

const IndexIcon = glamorous(AddIcon)(
  {
    marginLeft: 4,
    marginRight: 4,
    fontSize: '20px !important',
  },
  ({ type }) => ({
    color: type === ERROR ? '#db3737' : 'green',
  }),
);

const DeleteIcon = glamorous(DeleteForever)(
  {
    marginLeft: 4,
    marginRight: 4,
    fontSize: '20px !important',
  },
  ({ type }) => ({
    color: type === ERROR ? '#db3737' : 'orange',
  }),
);

const CategoryIcon = glamorous(Category)(
  {
    marginLeft: 4,
    marginRight: 4,
    fontSize: '20px !important',
  },
  ({ type }) => ({
    color: type === ERROR ? '#db3737' : type === SUCCESS ? 'green' : 'orange',
  }),
);

const MSDLinkIcon = glamorous(DescriptionIcon)(
  {
    marginLeft: 4,
    marginRight: 4,
    fontSize: '20px !important',
  },
  ({ type }) => ({
    color: type === ERROR ? '#db3737' : type === SUCCESS ? 'green' : 'orange',
  }),
);

const InitDataflowsIcon = glamorous(AutorenewIcon)(
  {
    marginLeft: 4,
    marginRight: 4,
    fontSize: '20px !important',
  },
  ({ type }) => ({
    color: type === ERROR ? '#db3737' : type === SUCCESS ? 'green' : 'orange',
  }),
);

const getIntent = R.pipe(
  R.prop('type'),
  R.when(R.equals(ERROR), R.always('DANGER')),
  R.flip(R.prop)(Intent),
);

const IconStyled = glamorous(Icon)({ marginLeft: 4, marginRight: 4 });

const getKey = R.pipe(R.prop('type'), R.toLower);

const ItemLogs = ({ artefactId, categorisationLogs, activateLog, intl, isNew, error, space }) => {
  const exportDataLog = useSelector(getExportDataLog(artefactId));
  const exportMetadataLog = useSelector(getExportMetadataLog(artefactId));
  const _msdLinkLog = useSelector(getDsdLinkLog(artefactId));
  const msdLinkLog =
    _msdLinkLog &&
    R.over(R.lensProp('log'), log => {
      if (R.isNil(log) || R.isNil(log.ref) || R.isEmpty(log.ref)) {
        return log;
      }
      return {
        ...log,
        message: formatMessage(intl)(R.prop('link.msd.error.already.linked', messages), {
          dsd: artefactId,
          msd: log.ref,
        }),
      };
    })(_msdLinkLog);
  const _initDfsLog = useSelector(getInitDataflowsLog(artefactId));
  const initDfsLog =
    _initDfsLog &&
    R.over(R.lensProp('logs'), logs => {
      if (R.isNil(logs) || R.isEmpty(logs)) {
        return logs;
      }
      const type = R.pipe(
        R.pluck('type'),
        R.uniq,
        R.ifElse(t => R.length(t) > 1, R.always(WARNING), R.head),
      )(logs);
      const message = R.pluck('message', logs);
      return { type, message };
    })(_initDfsLog);
  const sfsIndexLog = useSelector(getArtefactIndexationLog(artefactId));
  const getTrad = ({ key, labels }) => formatMessage(intl)(messages[key], labels);
  return (
    <div>
      {isNew ? <IconStyled iconName="add-to-artifact" intent={Intent.PRIMARY} /> : null}
      {!R.isNil(R.prop('delete', error)) ? (
        <Dialog
          id="itemLog-delete"
          title={getTrad({
            key: `artefact.delete.${getKey(error.delete)}`,
            labels: { label: R.path(['space', 'label'], error.delete) },
          })}
          log={error.delete}
        >
          <DeleteIcon type={R.path(['delete', 'type'], error)} />
        </Dialog>
      ) : null}
      {!R.isNil(R.prop('exportArtefact', error)) ? (
        <Dialog
          id="itemLog-exportArtefact"
          title={getTrad({ key: 'export.artefact' })}
          log={error.exportArtefact}
        >
          <IconStyled iconName="download" intent={getIntent(error.exportArtefact)} />
        </Dialog>
      ) : null}
      {!R.isNil(exportDataLog) ? (
        <Dialog id="itemLog-exportData" title={getTrad({ key: 'export.data' })} log={exportDataLog}>
          <IconStyled iconName="download" intent={getIntent(exportDataLog)} />
        </Dialog>
      ) : null}
      {!R.isNil(exportMetadataLog) ? (
        <Dialog
          id="itemLog-exportMetadata"
          title={getTrad({ key: 'export.metadata' })}
          log={exportMetadataLog}
        >
          <IconStyled iconName="download" intent={getIntent(exportMetadataLog)} />
        </Dialog>
      ) : null}
      {!R.isNil(R.prop('transferArtefact', error)) ? (
        <Dialog
          ariaLabel={`transfer.artefact.${getKey(error.transferArtefact)}`}
          log={error.transferArtefact}
          id="itemLog-transferArtefact"
          title={getTrad({
            key: `transfer.artefact.${getKey(error.transferArtefact)}`,
            labels: {
              source: R.path(['source', 'label'], error.transferArtefact),
              target: R.path(['target', 'label'], error.transferArtefact),
            },
          })}
        >
          <IconStyled iconName="swap-horizontal" intent={getIntent(error.transferArtefact)} />
        </Dialog>
      ) : null}
      {!R.isNil(R.prop('transferData', error)) ? (
        <Dialog
          ariaLabel={`transfer.data.${getKey(error.transferData)}`}
          log={error.transferData}
          id="itemLog-transferData"
          title={getTrad({
            key: `transfer.data.${getKey(error.transferData)}`,
            labels: {
              source: R.path(['source', 'label'], error.transferData),
              target: R.path(['target', 'label'], error.transferData),
            },
          })}
        >
          <IconStyled iconName="swap-horizontal" intent={getIntent(error.transferData)} />
        </Dialog>
      ) : null}
      {!R.isNil(categorisationLogs) ? (
        <Dialog
          id="itemLog-categorisation"
          title={getTrad({
            key: `artefact.categorisation.${getKey(categorisationLogs)}`,
          })}
          log={categorisationLogs}
        >
          <CategoryIcon type={categorisationLogs.type} />
        </Dialog>
      ) : null}
      {!R.isNil(msdLinkLog) ? (
        <Dialog
          id="msd-link"
          title={getTrad({
            key: `link.msd.${getKey(msdLinkLog.log)}`,
            labels: { label: R.prop('label', space) },
          })}
          log={msdLinkLog.log}
        >
          <MSDLinkIcon type={R.path(['log', 'type'], msdLinkLog)} />
        </Dialog>
      ) : null}
      {!R.isNil(initDfsLog) && !R.isNil(initDfsLog.logs) ? (
        <Dialog
          id="msd-link"
          title={getTrad({
            key: `init.dfs.${getKey(initDfsLog.logs)}`,
            labels: { label: R.prop('label', space) },
          })}
          log={initDfsLog.logs}
        >
          <InitDataflowsIcon type={R.path(['logs', 'type'], initDfsLog)} />
        </Dialog>
      ) : null}
      {!R.isNil(sfsIndexLog) ? (
        <Dialog
          id="sfs-index"
          title={formatMessage(intl)(R.prop(`sfs.index.${getKey(sfsIndexLog)}`, messages))}
          log={sfsIndexLog}
        >
          <IndexIcon type={R.prop('type', sfsIndexLog)} />
        </Dialog>
      ) : null}
      {!R.isNil(activateLog) ? (
        <Dialog
          id="activate"
          title={formatMessage(intl)(R.prop(`activate.dataflow.${getKey(activateLog)}`, messages))}
          log={activateLog}
        >
          <ActivateIcon type={R.prop('type', activateLog)} />
        </Dialog>
      ) : null}
    </div>
  );
};

ItemLogs.propTypes = {
  artefactId: PropTypes.string.isRequired,
  activateLog: PropTypes.object,
  categorisationLogs: PropTypes.object,
  intl: PropTypes.object,
  isNew: PropTypes.bool,
  error: PropTypes.object,
  space: PropTypes.object,
};

export default artefactId =>
  compose(
    withTransferArtefact(artefactId),
    mapProps(({ log, error, ...rest }) => ({
      error: R.assoc('transferArtefact', log, error || {}),
      ...rest,
    })),
    withTransferData(artefactId),
    mapProps(({ log, error, ...rest }) => ({
      error: R.assoc('transferData', log, error || {}),
      ...rest,
    })),
    withCategorisationState(artefactId),
    injectIntl,
  )(ItemLogs);
