import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { makeStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import { injectIntl } from 'react-intl';
import cx from 'classnames';
import i18nMessages from '../messages';

const useStyles = makeStyles(theme => ({
  messages: {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'wrap',
  },
  tooltip: {
    fontSize: 14,
    boxShadow: theme.shadows[2],
    maxWidth: '80vw',
  },
  WARNING: {
    backgroundColor: theme.palette.warning.light,
    color: 'black',
  },
  ERROR: {
    backgroundColor: theme.palette.error.main,
    color: 'white',
  },
  SUCCESS: {
    backgroundColor: theme.palette.success.dark,
    color: 'white',
  },
  popper: {
    opacity: 1,
  },
}));

export const ItemLog = ({ id, icon, intl, log, title }) => {
  const classes = useStyles();
  const type = R.prop('type', log);
  if (!type) return null;

  const messages = R.pipe(
    R.ifElse(
      R.always(R.has(log.statusKey, i18nMessages) && !R.isNil(intl)),
      ({ statusKey }) => intl.formatMessage(i18nMessages[statusKey]),
      R.prop('message'),
    ),
    R.when(R.complement(R.is)(Array), R.of),
  )(log);
  return (
    <Tooltip
      title={
        <div>
          <span>{title}</span>
          {R.isEmpty(messages) ? null : (
            <div>
              {R.addIndex(R.map)(
                (message, ind) => (
                  <p key={`${id}-${ind}`}>{message}</p>
                ),
                messages,
              )}
            </div>
          )}
        </div>
      }
      classes={{
        popper: classes.popper,
        tooltip: cx(classes[type], classes.tooltip),
      }}
      placement="left"
    >
      {icon}
    </Tooltip>
  );
};

ItemLog.propTypes = {
  id: PropTypes.string.isRequired,
  icon: PropTypes.element,
  intl: PropTypes.object,
  log: PropTypes.object,
  title: PropTypes.string,
};

export default injectIntl(ItemLog);
