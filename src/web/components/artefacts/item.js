import React, { useState } from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import { assoc, is, propOr, isNil } from 'ramda';
import { useSelector } from 'react-redux';
import { Checkbox, Classes, Colors, Icon, Spinner } from '@blueprintjs/core';
import DescriptionIcon from '@material-ui/icons/Description';
import AutorenewIcon from '@material-ui/icons/Autorenew';
import AddIcon from '@material-ui/icons/AddToPhotos';
import PowerIcon from '@material-ui/icons/Power';
import { FormattedMessage } from '../../modules/i18n';
import Space from '../common/space';
import { ItemActions } from './actions';
import itemLogs from './item-logs';
import MoreButton from './more-button';
import { DataflowDetails } from './dataflow-details';
import { compose, mapProps } from 'recompose';
import { withTransferArtefact } from '../../modules/transfer-artefact';
import { withTransferData } from '../../modules/transfer-data';
import { getIsExportingData } from '../../modules/export-data';
import { withLocale } from '../../modules/i18n';
import { withDataflowDetails } from '../../modules/dataflow-details';
import { withCategorisationState } from '../../modules/categorize';
import messages from '../messages';
import { getRequestArgs } from '@sis-cc/dotstatsuite-sdmxjs';
import Link from '@material-ui/core/Link';
import { withAuthHeader } from '../../modules/common/withauth';
import apiManager from '../../apiManager';
import { getDsdLinkLog, getInitDataflowsLog } from '../../modules/defineMSD/selectors';
import { ExportDataDialog } from '../dialog-menus';
import { getArtefactIsIndexing } from '../../modules/sfsIndex/selectors';
import { useActivateDataflows } from '../../hooks/useActivateDataflows';

const ActivateIcon = glamorous(PowerIcon)({
  marginLeft: 10,
  color: '#5c7080',
  fontSize: '20px !important',
});

const MSDLinkIcon = glamorous(DescriptionIcon)({
  marginLeft: 10,
  color: '#5c7080',
  fontSize: '20px !important',
});

const InitDfsIcon = glamorous(AutorenewIcon)({
  marginLeft: 10,
  color: '#5c7080',
  fontSize: '20px !important',
});

const IndexIcon = glamorous(AddIcon)({
  marginLeft: 10,
  color: '#5c7080',
  fontSize: '20px !important',
});

const StyledCheckbox = glamorous(Checkbox)({
  marginBottom: '0 !important',
});

const Container = glamorous.div({
  marginTop: 10,
  paddingTop: 10,
  borderTop: '1px solid #ccc',
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
});

const ItemStyled = glamorous.div({
  display: 'flex',
  flexDirection: 'column',
  width: '100%',
});

const StyledPending = glamorous.div({
  marginTop: 10,
  paddingTop: 10,
  borderTop: '1px solid #ccc',
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
});

const Resume = glamorous.div({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-between',
});

const Interface = glamorous.div({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
});

//--------------------------------------------------------------------------------------------------
export const IsFinal = ({ isFinal }) => {
  if (!isFinal) return null;

  return (
    <span>
      <span className={Classes.NAVBAR_DIVIDER}></span>
      <Icon iconName="tick" />
      <glamorous.Span marginLeft={5}>
        <FormattedMessage id="artefact.is.final" />
      </glamorous.Span>
    </span>
  );
};

IsFinal.propTypes = {
  isFinal: PropTypes.bool,
};

//--------------------------------------------------------------------------------------------------

const IconStyled = glamorous(Icon)({ marginLeft: 10, marginRight: 10 });
const SpinnerStyled = glamorous(Spinner)({ marginRight: 10 });

//--------------------------------------------------------------------------------------------------
const ItemPending = ({ iconName, message, children }) => {
  return (
    <StyledPending>
      <SpinnerStyled />
      {iconName ? <Icon iconName={iconName} style={{ color: Colors.GRAY1 }} /> : null}
      <glamorous.Span marginLeft={10}>{message}</glamorous.Span>
      {children}
    </StyledPending>
  );
};

ItemPending.propTypes = {
  iconName: PropTypes.string,
  message: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  children: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
};

//--------------------------------------------------------------------------------------------------
const ItemView = props => {
  const {
    id,
    isDeletable,
    label,
    type,
    code,
    version,
    isFinal,
    agencyId,
    space,
    isNew,
    error,
    handlers,
    isSelected,
    onClick,
    hasData,
    dataExplorerUrl,
    requestDetails,
    categories,
    observations,
    isDetailsExpanded,
    deleteDetails,
    isCategorizing,
    annotations,
    sdmxId,
    isActivating,
    activateLog,
  } = props;
  const transferArtefact = props.transferArtefact;
  const msdLinkLog = useSelector(getDsdLinkLog(id));
  const initDfsLog = useSelector(getInitDataflowsLog(id));
  const [isOpenExportDataMenu, setIsOpenExportDataMenu] = useState(false);
  const isIndexing = useSelector(getArtefactIsIndexing(id));
  const isExportingData = useSelector(getIsExportingData(id));

  const { activate } = useActivateDataflows([{ id, sdmxId, space, type }]);

  if (isIndexing) {
    return (
      <ItemPending message={<FormattedMessage id="sfs.is.indexing" />}>
        <IndexIcon />
      </ItemPending>
    );
  }

  if (isActivating) {
    return (
      <ItemPending message={<FormattedMessage id="activate.dataflow.is.activating" />}>
        <ActivateIcon />
      </ItemPending>
    );
  }

  if (propOr(false, 'isLinking', msdLinkLog)) {
    return (
      <ItemPending message={<FormattedMessage id="is.linking.msd" />}>
        <MSDLinkIcon />
      </ItemPending>
    );
  }

  if (propOr(false, 'isInitializing', initDfsLog)) {
    return (
      <ItemPending message={<FormattedMessage id="is.initializing.dfs" />}>
        <InitDfsIcon />
      </ItemPending>
    );
  }

  if (transferArtefact.isTransfering) {
    return (
      <ItemPending
        message={<FormattedMessage id="artefact.is.transfering" />}
        iconName="add-to-artifact"
      />
    );
  }

  if (isExportingData) {
    return (
      <ItemPending
        message={<FormattedMessage id="artefact.is.exporting.data" />}
        iconName="download"
      />
    );
  }

  if (isCategorizing) {
    return (
      <ItemPending message={<FormattedMessage id="artefact.categorising" />} iconName="download" />
    );
  }

  const transferData = props.transferData;
  if (transferData.isTransfering) {
    return (
      <ItemPending message={<FormattedMessage id="artefact.is.transfering.data" />}>
        <IconStyled iconName="arrow-right" style={{ color: Colors.GRAY1 }} />
        <Space {...transferData.destinationSpace} />
      </ItemPending>
    );
  }

  const ItemLogs = itemLogs(id);

  return (
    <Container>
      {isOpenExportDataMenu && (
        <ExportDataDialog onClose={() => setIsOpenExportDataMenu(false)} selection={[id]} />
      )}
      <StyledCheckbox checked={isSelected} onChange={() => handlers.toggleArtefact(id)} />
      <ItemStyled>
        <Resume>
          <glamorous.Div>
            <glamorous.Div marginBottom={10}>
              <Space
                {...space}
                label={
                  <FormattedMessage {...propOr({ id: type }, `artefact.type.${type}`, messages)} />
                }
              />
              <glamorous.Span marginLeft={10}>
                <Link onClick={onClick}>
                  {isNil(label) ? <FormattedMessage id="artefact.no.label" /> : label}
                </Link>
              </glamorous.Span>
            </glamorous.Div>
            <div>
              <span>[{code}]</span>
              <span className={Classes.NAVBAR_DIVIDER}></span>
              <span>[{version}]</span>
              <IsFinal isFinal={isFinal} />
              <span className={Classes.NAVBAR_DIVIDER}></span>
              <span>{agencyId}</span>
              {hasData ? <span className={Classes.NAVBAR_DIVIDER}></span> : null}
              {hasData && is(Function, requestDetails) ? (
                <MoreButton
                  id={id}
                  isActive={isDetailsExpanded}
                  request={isDetailsExpanded ? deleteDetails : requestDetails}
                />
              ) : null}
            </div>
          </glamorous.Div>
          <Interface>
            {hasData && !isNil(dataExplorerUrl) ? (
              <a href={dataExplorerUrl} target="_blank" rel="noopener noreferrer">
                <Icon
                  iconName="eye-open"
                  style={{
                    color: Colors.GRAY1,
                    marginLeft: 10,
                    marginRight: 10,
                  }}
                />
              </a>
            ) : null}
            <ItemLogs
              artefactId={id}
              isNew={isNew}
              error={error}
              space={space}
              activateLog={activateLog}
            />
            <ItemActions
              activate={activate}
              artefact={{ id, agencyId, code, space, type, version, annotations, isFinal, sdmxId }}
              artefactId={id}
              artefactDelete={handlers.artefactDelete}
              artefactExport={handlers.artefactExportStructure}
              dataExport={() => setIsOpenExportDataMenu(true)}
              hasData={hasData}
              isDeletable={isDeletable}
            />
          </Interface>
        </Resume>
        {hasData ? (
          <DataflowDetails
            categories={categories}
            id={id}
            details={[
              [
                <i key="dataflow.space">
                  <FormattedMessage id="dataflow.space" />:{' '}
                </i>,
                [space.id],
              ],
              [
                <i key="dataflow.flavours">
                  <FormattedMessage id="dataflow.flavours" />:{' '}
                </i>,
                [`${agencyId}:${code}(${version})`, `${agencyId}/${code}/${version}`],
              ],
            ]}
            isExpanded={isDetailsExpanded}
            observations={observations}
            requestDetails={requestDetails}
          />
        ) : null}
      </ItemStyled>
    </Container>
  );
};

ItemView.propTypes = {
  activateProps: PropTypes.object,
  dataExplorerUrl: PropTypes.string,
  hasData: PropTypes.bool,
  locale: PropTypes.string,
  id: PropTypes.string.isRequired,
  isDeletable: PropTypes.bool,
  exportData: PropTypes.func,
  onClick: PropTypes.func,
  label: PropTypes.string,
  type: PropTypes.string,
  code: PropTypes.string,
  version: PropTypes.string,
  isFinal: PropTypes.bool,
  agencyId: PropTypes.string,
  isNew: PropTypes.bool,
  error: PropTypes.object,
  handlers: PropTypes.object,
  space: PropTypes.shape({
    color: PropTypes.string,
  }).isRequired,
  isSelectable: PropTypes.bool,
  isSelected: PropTypes.bool,
  sdmxId: PropTypes.string,
  isActivating: PropTypes.bool,
  activateLog: PropTypes.object,
};

//--------------------------------------------------------------------------------------------------
const itemProxy = id =>
  compose(
    withLocale,
    withTransferArtefact(id),
    mapProps(({ agencyId, code, error, space, type, version, locale, ...rest }) => {
      const { url, headers } = getRequestArgs({
        identifiers: { agencyId, code, version },
        type,
        format: 'xml',
        datasource: assoc('url', space.endpoint, space),
        withUrn: false,
      });
      const onClick = () => {
        apiManager
          .get(url, { headers: withAuthHeader(space)(headers) })
          .then(response => response.data)
          .then(xml => {
            let blob = new Blob([xml], { type: 'text/xml' });
            let url = URL.createObjectURL(blob);
            window.open(url, '_blank');
            URL.revokeObjectURL(url);
          });
      };
      return {
        ...rest,
        agencyId,
        artefactError: error,
        code,
        hasData: type === 'dataflow',
        dataExplorerUrl:
          space.dataExplorerUrl && !isNil(space.dataExplorerUrl)
            ? `${space.dataExplorerUrl}/vis?locale=${locale}&dataflow[datasourceId]=${space.id}&dataflow[agencyId]=${agencyId}&dataflow[dataflowId]=${code}&dataflow[version]=${version}&hasDataAvailability=true`
            : null,
        isDeletable: !isNil(space.transferUrl),
        onClick,
        space,
        type,
        version,
      };
    }),
    mapProps(({ isTransfering, isUpdating, destinationSpace, ...rest }) => ({
      ...rest,
      transferArtefact: { isTransfering, isUpdating, destinationSpace },
    })),
    withTransferData(id),
    mapProps(({ isTransfering, isUpdating, destinationSpace, ...rest }) => ({
      ...rest,
      transferData: { isTransfering, isUpdating, destinationSpace },
    })),
    withDataflowDetails(id),
    withCategorisationState(id),
  )(ItemView);

//--------------------------------------------------------------------------------------------------
export const Item = ({ id, isDeleting, isExporting, ...rest }) => {
  if (isDeleting)
    return (
      <ItemPending message={<FormattedMessage id="artefact.is.deleting" />} iconName="trash" />
    );
  if (isExporting)
    return (
      <ItemPending
        message={<FormattedMessage id="artefact.is.exporting.structure" />}
        iconName="download"
      />
    );
  const ItemProxy = itemProxy(id);

  return <ItemProxy id={id} {...rest} />;
};

Item.propTypes = {
  id: PropTypes.string.isRequired,
  isDeleting: PropTypes.bool,
  isExporting: PropTypes.bool,
};
