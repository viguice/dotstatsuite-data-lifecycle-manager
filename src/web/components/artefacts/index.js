import React from 'react';
import PropTypes from 'prop-types';
import { compose, branch, renderComponent } from 'recompose';
import { Classes, Intent, NonIdealState, Spinner, Toast } from '@blueprintjs/core';
import classnames from 'classnames';
import glamorous from 'glamorous';
import * as R from 'ramda';
import { FormattedMessage } from '../../modules/i18n';
import { ERROR } from '../../modules/common/constants';
import { List, Spotlight, Status, Sort, PaginationBlock } from '../list';
import TransferDialog from './transfer-dialog';
import { Item } from './item';
import Selection from './selection';
import ArtefactRelativesDialog from './relatives';
import ArtefactCategorisationDialog from './categorisation';
import AuthDialog from './auth-dialog';
import { MSDsDialog } from './defineMsd';
import { useDispatch } from 'react-redux';
import { artefactDelete, artefactExportStructure } from '../../modules/artefacts/action-creators';

export const Wrapper = glamorous.div({
  display: 'flex',
  justifyContent: 'center',
  flexDirection: 'column',
  borderTop: '2px solid #999',
  paddingTop: 10,
});

const StyledNav = glamorous.nav({
  padding: '0 !important',
  boxShadow: 'none !important',
  zIndex: '0 !important',
  backgroundColor: 'transparent  !important',
});

const StyledList = glamorous.nav({
  borderBottom: '1px solid #ccc',
  marginBottom: 10,
  paddingBottom: 10,
});

const StyledToast = glamorous(Toast)({
  maxWidth: '100% !important',
  margin: '0 0 10px !important',
});

export const ArtefactsSpinner = () => {
  return (
    <Wrapper style={{ alignItems: 'center' }}>
      <Spinner className={Classes.LARGE} />
    </Wrapper>
  );
};

export const ArtefactDeleteLog = ({ log, resetLog }) => {
  const intent = Intent[log.type === ERROR ? 'DANGER' : log.type];
  const label = R.path(['space', 'label'], log);
  const title =
    log.type === ERROR ? (
      <FormattedMessage id="artefact.delete.error" values={{ label }} />
    ) : (
      <FormattedMessage id="artefact.delete.success" values={{ label }} />
    );
  return (
    <StyledToast
      intent={intent}
      onDismiss={() => resetLog(log.id)}
      timeout={0}
      message={
        <div>
          <div>{title}</div>
          {R.addIndex(R.map)((text, ind) => <div key={ind}>{text}</div>)(
            R.is(Array, log.message) ? log.message : [log.message],
          )}
        </div>
      }
    />
  );
};

ArtefactDeleteLog.propTypes = {
  log: PropTypes.object,
  resetLog: PropTypes.func,
};

export const ArtefactDeleteLogs = ({ deleteLog, resetLog }) => {
  return (
    <div>
      {R.map(
        log => (
          <ArtefactDeleteLog key={log.id} log={log} resetLog={resetLog} />
        ),
        deleteLog,
      )}
    </div>
  );
};

ArtefactDeleteLogs.propTypes = {
  deleteLog: PropTypes.array,
  resetLog: PropTypes.func,
};

export const ArtefactsBlank = ({ deleteLog, noSpaces, noTypes, resetLog }) => {
  let title = <FormattedMessage id="artefacts.no.artefact" />;
  if (noSpaces && noTypes) title = <FormattedMessage id="artefacts.no.selection" />;
  else if (noSpaces || noTypes) {
    title = noSpaces ? (
      <FormattedMessage id="artefacts.no.spaces" />
    ) : (
      <FormattedMessage id="artefacts.no.types" />
    );
  }
  return (
    <Wrapper>
      {!R.isNil(deleteLog) && !R.isEmpty(deleteLog) ? (
        <ArtefactDeleteLogs deleteLog={deleteLog} resetLog={resetLog} />
      ) : null}
      <ArtefactRelativesDialog />
      <AuthDialog />
      <MSDsDialog />
      <NonIdealState visual="square" title={title} />
    </Wrapper>
  );
};

ArtefactsBlank.propTypes = {
  deleteLog: PropTypes.array,
  noSpaces: PropTypes.bool,
  noTypes: PropTypes.bool,
  resetLog: PropTypes.func,
};

const withBlank = branch(({ items }) => R.isEmpty(items), renderComponent(ArtefactsBlank));
const ArtefactsList = compose(withBlank)(List);

export const Artefacts = props => {
  const dispatch = useDispatch();
  const handlers = {
    toggleArtefact: props.selection.toggleArtefact,
    artefactDelete: id => dispatch(artefactDelete(id)),
    artefactExportStructure: (...props) => dispatch(artefactExportStructure(...props)),
  };
  return (
    <Wrapper>
      {!R.isNil(props.deleteLog) ? (
        <ArtefactDeleteLogs deleteLog={props.deleteLog} resetLog={props.resetLog} />
      ) : null}
      <ArtefactRelativesDialog />
      <MSDsDialog />
      <ArtefactCategorisationDialog />
      <AuthDialog />
      <StyledNav className={classnames(Classes.NAVBAR)}>
        <div className={classnames(Classes.NAVBAR_GROUP, Classes.ALIGN_LEFT)}>
          <Spotlight
            {...props.spotlight}
            fieldChange={props.spotlightFieldChange}
            valueChange={props.spotlightValueChange}
          />
          <span className={Classes.NAVBAR_DIVIDER}></span>
          <Sort {...props.sort} fieldChange={props.sortFieldChange} />
        </div>
        <div className={classnames(Classes.NAVBAR_GROUP, Classes.ALIGN_RIGHT)}>
          <Status {...props.status} isFetching={props.isFetching} />
        </div>
        <TransferDialog deselect={props.selection.deselect} />
      </StyledNav>
      <div>
        <PaginationBlock pagination={props.pagination} pageChange={props.pageChange} />
        <Selection {...props.selection} />
        <StyledList>
          <ArtefactsList items={props.list.items} itemRenderer={Item} handlers={handlers} />
        </StyledList>
        <PaginationBlock {...props} />
      </div>
    </Wrapper>
  );
};
