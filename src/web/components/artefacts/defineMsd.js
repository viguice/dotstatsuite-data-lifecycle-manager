import React, { useState } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { useDispatch, useSelector } from 'react-redux';
import { useIntl } from 'react-intl';
import glamorous from 'glamorous';
import DescriptionIcon from '@material-ui/icons/Description';
import { Icon, Button, Classes, Dialog, Spinner, Intent, Tree } from '@blueprintjs/core';
import { getIsLoading, getIsOpen, getRefinedMsds } from '../../modules/defineMSD/selectors';
import { closeMSDDialog, linkAllMsd } from '../../modules/defineMSD/action-creators';
import { formatMessage } from '../../modules/i18n';
import messages from '../messages';

const GDialog = glamorous(Dialog)({ width: 'auto !important' });

const TreeWrapper = glamorous.div({
  maxHeight: 250,
  overflow: 'auto',
});

const Footer = glamorous.div({
  display: 'flex',
  justifyContent: 'flex-end',
});

const MSDLinkIcon = glamorous(DescriptionIcon)({
  color: '#5c7080',
  marginRight: 7,
  fontSize: '17px !important',
});

const MSD = ({ agencyId, code, intl, isFinal, label, version }) => (
  <div>
    <glamorous.Span>{label}</glamorous.Span>
    <span className={Classes.NAVBAR_DIVIDER}></span>
    <span>[{code}]</span>
    <span className={Classes.NAVBAR_DIVIDER}></span>
    <span>[{version}]</span>
    {isFinal && (
      <span>
        <span className={Classes.NAVBAR_DIVIDER}></span>
        <Icon iconName="tick" />
        <glamorous.Span marginLeft={5}>
          {formatMessage(intl)(messages['artefact.is.final'])}
        </glamorous.Span>
      </span>
    )}
    <span className={Classes.NAVBAR_DIVIDER}></span>
    <span>{agencyId}</span>
  </div>
);

MSD.propTypes = {
  agencyId: PropTypes.string,
  code: PropTypes.string,
  intl: PropTypes.object,
  isFinal: PropTypes.bool,
  label: PropTypes.string,
  version: PropTypes.string,
};

export const MSDsDialog = () => {
  const intl = useIntl();
  const dispatch = useDispatch();
  const isOpen = useSelector(getIsOpen);
  const isLoading = useSelector(getIsLoading);
  const onClose = () => dispatch(closeMSDDialog());
  const msds = useSelector(getRefinedMsds);
  const [selectedId, setSelectedId] = useState(null);
  const selected = R.find(R.propEq('id', selectedId), msds);
  const onLink = () => dispatch(linkAllMsd(selected));

  const onSelect = ({ id }) => (id === selectedId ? setSelectedId(null) : setSelectedId(id));
  return (
    <GDialog
      isOpen={isOpen}
      onClose={onClose}
      title={
        <div>
          <MSDLinkIcon />
          {formatMessage(intl)(messages['link.msd'])}
        </div>
      }
    >
      <div className={Classes.DIALOG_BODY}>
        {isLoading && <Spinner />}
        {!isLoading && !R.isEmpty(msds) && (
          <TreeWrapper>
            <Tree
              onNodeClick={onSelect}
              contents={R.map(
                msd => ({
                  id: msd.id,
                  depth: 0,
                  hasCaret: false,
                  isSelected: msd.id === selectedId,
                  label: <MSD key={msd.id} intl={intl} {...msd} />,
                }),
                msds,
              )}
            ></Tree>
          </TreeWrapper>
        )}
      </div>
      <Footer className={Classes.DIALOG_FOOTER}>
        {!isLoading && (
          <Button
            style={{ marginRight: '10px' }}
            disabled={R.isNil(selected)}
            intent={Intent.PRIMARY}
            onClick={onLink}
            text={formatMessage(intl)(messages['action.link'])}
          />
        )}
        <Button onClick={onClose} text={formatMessage(intl)(messages['action.cancel'])} />
      </Footer>
    </GDialog>
  );
};
