import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { compose, mapProps } from 'recompose';
import glamorous from 'glamorous';
import { injectIntl } from 'react-intl';
import { getRequestArgs } from '@sis-cc/dotstatsuite-sdmxjs';
import {
  Button,
  Checkbox,
  Classes,
  Dialog,
  Icon,
  Intent,
  Spinner,
} from '@blueprintjs/core';
import Card from '@material-ui/core/Card';
import DeleteForever from '@material-ui/icons/DeleteForever';
import RemoveCircle from '@material-ui/icons/RemoveCircle';
import ReportProblem from '@material-ui/icons/ReportProblem';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import { ItemLog } from './item-log';
import { withArtefactRelatives } from '../../modules/relatives';
import { formatMessage } from '../../modules/i18n';
import { ERROR, WARNING } from '../../modules/common/constants';
import { withAuthHeader } from '../../modules/common/withauth';
import { useGetUserRights } from '../../hooks/user-rights';
import { isArtefactDeleteAuthorized } from '../../lib/permissions';
import Space from '../common/space';
import messages from '../messages';
import apiManager from '../../apiManager';
import { getErrors } from '../../modules/relatives/selectors';

const useErrorLogsStyles = makeStyles(theme => ({
  card: {
    backgroundColor: theme.palette.error.main,
    color: 'white',
    marginBottom: 20,
    padding: 5,
  },
}));

const GDialog = glamorous(Dialog)({ width: 'auto !important' });

const DeleteIcon = glamorous(DeleteForever)(
  {
    fontSize: '20px !important',
  },
  ({ type }) => ({
    color: type === ERROR ? '#db3737' : 'green',
  }),
);

const Footer = glamorous.div({
  display: 'flex',
  justifyContent: 'space-between',
});

const ForbiddenIcon = glamorous(RemoveCircle)({
  marginRight: 8,
  fontSize: '18px !important',
});

const WarningIcon = glamorous(ReportProblem)({
  marginRight: 8,
  fontSize: '18px !important',
});

const Logs = glamorous.div({
  marginRight: 6,
});

const StyledCheckbox = glamorous(Checkbox)({
  marginBottom: '0 !important',
});

const ArtefactDetails = glamorous.div(
  {
    alignItems: 'center',
    display: 'flex',
    marginBottom: 5,
    color: '#0549ab',
  },
  ({ isMainArtefact }) => {
    if (isMainArtefact) {
      return { fontWeight: 'bold' };
    }
    return {};
  },
);

const ErrorLogs = ({ errors, intl }) => {
  const classes = useErrorLogsStyles();
  if (R.isNil(errors)) {
    return null;
  }
  const getErrorMessage = error => {
    if (error.operation === 'getChildren') {
      return formatMessage(intl)(
        R.prop('artefact.relatives.error.children', messages),
        {
          type: R.pathOr('artefact', ['artefact', 'type'], error),
          id: R.path(['artefact', 'sdmxId'], error),
        },
      );
    }
    if (error.operation === 'getParents') {
      return formatMessage(intl)(
        R.prop('artefact.relatives.error.parents', messages),
        {
          type: R.pathOr('artefact', ['artefact', 'type'], error),
          id: R.path(['artefact', 'sdmxId'], error),
        },
      );
    }
    if (error.operation === 'parseMsdReference') {
      return formatMessage(intl)(
        R.prop('artefact.relatives.error.msd.ref', messages),
        {
          ref: R.prop('msdRef', error),
          id: R.path(['artefact', 'sdmxId'], error),
        },
      );
    }
    return error.message;
  };
  return (
    <Card className={classes.card}>
      <b>{formatMessage(intl)(R.prop('artefact.relatives.error', messages))}</b>
      <br />
      {R.map(error => getErrorMessage(error), errors)}
    </Card>
  );
};
ErrorLogs.propTypes = {
  errors: PropTypes.array,
  intl: PropTypes.object,
};

const ArtefactDeleteState = artefact => {
  const {
    cleanUpLog,
    deleteLog,
    intl,
    isDeleteAuthorized,
    isDeleting,
    isSelected,
    nonSelectableLog,
    toggleArtefact,
    warningLog,
  } = artefact;
  if (!toggleArtefact) {
    return null;
  }
  if (isDeleting) {
    return <Spinner className={Classes.SMALL} />;
  }
  if (deleteLog) {
    return (
      <Logs>
        {cleanUpLog && (
          <ItemLog
            id="itemLog-cleanUpLog"
            icon={<DeleteIcon type={R.prop('type', cleanUpLog)} />}
            title={R.prop('title', cleanUpLog)}
            log={cleanUpLog}
          />
        )}
        <ItemLog
          id="itemLog-deleteLog"
          icon={<DeleteIcon type={R.prop('type', deleteLog)} />}
          title={R.prop('title', deleteLog)}
          log={deleteLog}
        />
      </Logs>
    );
  }
  if (warningLog) {
    return (
      <ItemLog
        id="itemLog-warningLog"
        icon={<WarningIcon style={{ color: 'orange' }} />}
        title={null}
        log={{ message: warningLog, type: WARNING }}
      />
    );
  }
  if (!isDeleteAuthorized) {
    return (
      <ItemLog
        id="itemLog-nonSelectableLog"
        icon={<ForbiddenIcon color="error" />}
        title={null}
        log={{
          message: formatMessage(intl)(
            R.prop('artefact.relative.non.authorized', messages),
          ),
          type: ERROR,
        }}
      />
    );
  }
  if (nonSelectableLog) {
    return (
      <ItemLog
        id="itemLog-nonSelectableLog"
        icon={<ForbiddenIcon color="error" />}
        title={null}
        log={{ message: nonSelectableLog, type: ERROR }}
      />
    );
  }
  return <StyledCheckbox checked={isSelected} onChange={toggleArtefact} />;
};

const Artefact = ({
  intl,
  id,
  isDeleteAuthorized,
  isMainArtefact,
  label,
  space,
  type,
  code,
  agencyId,
  version,
  isFinal,
  isSelected,
  isDeleting,
  nonSelectableLog,
  warningLog,
  children = [],
  toggleArtefact,
  parentIds,
  cleanUpLog,
  deleteLog,
  userRights,
  isError = false,
  mode,
}) => {
  const { url, headers } = getRequestArgs({
    identifiers: { agencyId, code, version },
    type,
    format: 'xml',
    datasource: R.assoc('url', space.endpoint, space),
    withUrn: false,
  });
  const onClick = () => {
    apiManager
      .get(url, { headers: withAuthHeader(space)(headers) })
      .then(response => response.data)
      .then(xml => {
        let blob = new Blob([xml], { type: 'text/xml' });
        let url = URL.createObjectURL(blob);
        window.open(url, '_blank');
        URL.revokeObjectURL(url);
      });
  };
  return (
    <div>
      <ArtefactDetails isMainArtefact={isMainArtefact}>
        <ArtefactDeleteState
          cleanUpLog={cleanUpLog}
          deleteLog={deleteLog}
          isDeleteAuthorized={isDeleteAuthorized}
          intl={intl}
          isDeleting={isDeleting}
          isSelected={isSelected}
          nonSelectableLog={nonSelectableLog}
          warningLog={warningLog}
          toggleArtefact={
            R.isNil(toggleArtefact)
              ? null
              : () => toggleArtefact({ id, isSelected, parentIds, children })
          }
        />
        {isError && mode === 'visualisation' && (
          <ItemLog
            id="itemLog-warningLog"
            icon={<WarningIcon style={{ color: 'orange' }} />}
            title={null}
            log={{
              message: formatMessage(intl)(
                R.prop('artefact.relative.msd.missing.warning', messages),
              ),
              type: WARNING,
            }}
          />
        )}
        <Space
          {...space}
          label={formatMessage(intl)(
            R.propOr({ id: type }, `artefact.type.${type}`, messages),
          )}
        />
        <glamorous.Span marginLeft={5}>
          {isError ? label : <Link onClick={onClick}>{label}</Link>}
        </glamorous.Span>
        <span className={Classes.NAVBAR_DIVIDER}></span>
        <span>[{code}]</span>
        <span className={Classes.NAVBAR_DIVIDER}></span>
        <span>[{version}]</span>
        {isFinal && (
          <span>
            <span className={Classes.NAVBAR_DIVIDER}></span>
            <Icon iconName="tick" />
            <glamorous.Span marginLeft={5}>
              {formatMessage(intl)(messages['artefact.is.final'])}
            </glamorous.Span>
          </span>
        )}
        <span className={Classes.NAVBAR_DIVIDER}></span>
        <span>{agencyId}</span>
      </ArtefactDetails>
      <glamorous.Div marginLeft={25}>
        {R.map(
          child => (
            <Artefact
              intl={intl}
              key={child.id}
              toggleArtefact={toggleArtefact}
              parentIds={R.append(id, parentIds)}
              isDeleteAuthorized={isArtefactDeleteAuthorized(child, userRights)}
              mode={mode}
              {...child}
            />
          ),
          children,
        )}
      </glamorous.Div>
    </div>
  );
};

const ArtefactRelatives = ({
  intl,
  artefactsTree,
  cancelRelativesDelete,
  deleteRelativesArtefacts,
  cleanArtefactRelatives,
  hasSelected,
  isOpenRelatives,
  isFetching,
  isDeletingArtefacts,
  isError,
  title,
  toggleArtefact,
  selectAll,
  deselectAll,
  mode,
}) => {
  const { isLoading, rights } = useGetUserRights({ queryKey: 'my-rights' });
  const errors = useSelector(getErrors);
  return (
    <GDialog
      iconName="diagram-tree"
      isOpen={isOpenRelatives}
      onClose={cleanArtefactRelatives}
      title={title}
    >
      <div className={Classes.DIALOG_BODY}>
        <ErrorLogs errors={errors} intl={intl} />
        {(isFetching || isLoading) && <Spinner />}
        {!isFetching &&
          isError &&
          (R.isNil(artefactsTree) || R.isEmpty(artefactsTree)) && (
            <span>
              {formatMessage(intl)(messages['artefact.relatives.error'])}
            </span>
          )}
        {!R.isNil(artefactsTree) &&
          !R.isEmpty(artefactsTree) &&
          R.map(
            artefact => (
              <Artefact
                intl={intl}
                key={artefact.id}
                parentIds={[]}
                toggleArtefact={toggleArtefact}
                userRights={rights}
                isDeleteAuthorized={isArtefactDeleteAuthorized(
                  artefact,
                  rights,
                )}
                mode={mode}
                {...artefact}
              />
            ),
            artefactsTree,
          )}
      </div>
      <Footer className={Classes.DIALOG_FOOTER}>
        {selectAll && deselectAll && !isFetching && (
          <Button
            onClick={hasSelected ? deselectAll : selectAll}
            text={
              hasSelected
                ? formatMessage(intl)(
                    messages['artefact.relatives.deselect.all'],
                  )
                : formatMessage(intl)(messages['artefact.relatives.select.all'])
            }
          />
        )}
        <div>
          {deleteRelativesArtefacts && !isFetching && (
            <Button
              style={{ marginRight: '10px' }}
              disabled={!hasSelected}
              intent={Intent.PRIMARY}
              onClick={deleteRelativesArtefacts}
              text={formatMessage(intl)(messages['action.delete'])}
            />
          )}
          {cancelRelativesDelete && !isFetching && (
            <Button
              onClick={
                isDeletingArtefacts
                  ? cancelRelativesDelete
                  : cleanArtefactRelatives
              }
              text={
                isDeletingArtefacts
                  ? formatMessage(intl)(messages['action.cancel'])
                  : formatMessage(intl)(messages['action.close'])
              }
            />
          )}
        </div>
      </Footer>
    </GDialog>
  );
};

ArtefactRelatives.propTypes = {
  artefactsTree: PropTypes.array,
  cancelRelativesDelete: PropTypes.func,
  cleanArtefactRelatives: PropTypes.func.isRequired,
  deleteRelativesArtefacts: PropTypes.func,
  hasSelected: PropTypes.bool,
  intl: PropTypes.object,
  isDeletingArtefacts: PropTypes.bool,
  isOpenRelatives: PropTypes.bool,
  isFetching: PropTypes.bool,
  isError: PropTypes.bool,
  mode: PropTypes.string,
  title: PropTypes.string,
  toggleArtefact: PropTypes.func,
  deselectAll: PropTypes.func,
  selectAll: PropTypes.func,
};

const getChildrenIds = R.pipe(
  R.propOr([], 'children'),
  R.filter(R.prop('isSelectable')),
  R.map(child => R.prepend(child.id, getChildrenIds(child))),
  R.unnest,
);

const getLogTitle = (intl, space) => log =>
  R.when(R.complement(R.isNil), log => {
    const { key } = log;
    return {
      ...log,
      title: formatMessage(intl)(R.prop(key, messages), {
        label: R.prop('label', space),
      }),
    };
  })(log);

const getNonSelectableLog = (intl, artefact) => {
  const {
    isSelectable,
    nonSelectableChild,
    sdmxParents = [],
    type,
    isMsdLinked,
    children = [],
    isError = false,
  } = artefact;
  if (isMsdLinked) {
    return formatMessage(intl)(
      R.prop('artefact.relative.dsd.warning', messages),
    );
  }
  if (
    type === 'metadatastructure' &&
    R.find(R.propEq('isMsdLinked', true), children)
  ) {
    return formatMessage(intl)(R.prop('artefact.relative.msd.dsds', messages));
  }
  if (type === 'metadatastructure' && isError) {
    return formatMessage(intl)(
      R.prop('artefact.relative.msd.missing.warning', messages),
    );
  }
  if (isSelectable) {
    return null;
  }
  if (!R.isNil(nonSelectableChild)) {
    return formatMessage(intl)(
      R.prop('artefact.relative.child.non.deletable', messages),
      {
        id: nonSelectableChild,
      },
    );
  }
  const [firstParents, rest] = R.splitAt(3, sdmxParents);
  const restCount = R.length(rest);
  const formattedParents = R.pipe(
    R.map(parent => `${parent.type} ${parent.sdmxId}`),
    R.join(', '),
  )(firstParents);
  return formatMessage(intl)(
    R.prop('artefact.relative.non.selectable', messages),
    {
      parents: formattedParents,
      others: restCount,
    },
  );
};

const getWarningLog = (intl, artefact) => {
  const { isSelectable, parentsInTree = [] } = artefact;
  if (isSelectable || R.isEmpty(parentsInTree)) {
    return null;
  }
  const formattedParents = R.pipe(
    R.map(parent => `${parent.type} ${parent.sdmxId}`),
    R.join(', '),
  )(parentsInTree);
  return formatMessage(intl)(R.prop('artefact.relative.dependency', messages), {
    parents: formattedParents,
    others: 0,
  });
};

const getLogsTitles = intl =>
  R.when(
    R.complement(R.isNil),
    R.map(artefact =>
      R.pipe(
        R.evolve({
          cleanUpLog: getLogTitle(intl, artefact.space),
          deleteLog: getLogTitle(intl, artefact.space),
          children: getLogsTitles(intl),
        }),
        R.assoc('nonSelectableLog', getNonSelectableLog(intl, artefact)),
        R.assoc('warningLog', getWarningLog(intl, artefact)),
      )(artefact),
    ),
  );

const getAllChildren = R.pipe(
  R.propOr([], 'children'),
  R.map(child => R.prepend(child, getAllChildren(child))),
  R.unnest,
);

export default compose(
  withArtefactRelatives,
  injectIntl,
  mapProps(
    ({
      artefactsTree,
      mode,
      deletingIds,
      deselectArtefacts,
      selectArtefacts,
      selection,
      intl,
      ...rest
    }) => {
      if (mode !== 'delete') {
        return {
          artefactsTree,
          title: formatMessage(intl)(messages['artefact.relatives']),
          intl,
          mode,
          ...R.omit(
            ['cancelRelativesDelete', 'deleteRelativesArtefacts'],
            rest,
          ),
        };
      }
      return {
        artefactsTree: getLogsTitles(intl)(artefactsTree),
        title: formatMessage(intl)(messages['artefact.delete.relatives']),
        hasSelected: !R.isEmpty(selection),
        isDeletingArtefacts: !R.isEmpty(deletingIds),
        deselectAll: () => {
          deselectArtefacts(R.values(selection));
        },
        selectAll: () => {
          const ids = R.pipe(
            R.reduce(
              (acc, artefact) =>
                R.unnest([...acc, artefact, getAllChildren(artefact)]),
              [],
            ),
            R.map(R.prop('id')),
          )(artefactsTree);
          selectArtefacts(ids);
        },
        toggleArtefact: artefact => {
          let ids = [];
          if (artefact.isSelected) {
            deselectArtefacts([artefact.id]);
          } else {
            ids = R.prepend(artefact.id, getChildrenIds(artefact));
            selectArtefacts(ids);
          }
        },
        intl,
        ...rest,
      };
    },
  ),
)(ArtefactRelatives);
