import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { formatMessage, FormattedMessage } from '../../modules/i18n';
import {
  Classes,
  Button,
  Popover,
  PopoverInteractionKind,
  Position,
  Menu,
  MenuItem,
} from '@blueprintjs/core';
import glamorous from 'glamorous';
import Category from '@material-ui/icons/Category';
import DescriptionIcon from '@material-ui/icons/Description';
import PowerIcon from '@material-ui/icons/Power';
import AddIcon from '@material-ui/icons/AddToPhotos';
import { connect, useDispatch, useSelector } from 'react-redux';
import { useIntl } from 'react-intl';
import { compose, mapProps, withProps } from 'recompose';
import { transferArtefact } from '../../modules/transfer-artefact';
import MenuReferencesSelector from './refs-selector';
import { openItemTransfer, openSelectionTransfer } from '../../modules/transfer-control';
import { getArtefactRelatives } from '../../modules/relatives';
import { artefactCategorisation, artefactsCategorisation } from '../../modules/categorize';
import { MenuReferencesTransfer } from './item-transfer';
import { fetchAllMSD } from '../../modules/defineMSD/action-creators';
import {
  isMSDLinkPossible as isArtefactMSDLinkPossible,
  isMSDLinkPossible,
} from '../../modules/defineMSD/utils';
import { sfsIndex, sfsIndexSelection } from '../../modules/sfsIndex/action-creators';
import {
  deleteArtefacts,
  exportArtefacts,
  transferArtefacts,
} from '../../modules/selection/action-creators';
import messages from '../messages';
import { getInternalSpaces } from '../../modules/config';
import { createStructuredSelector } from 'reselect';

const ActivateIcon = glamorous(PowerIcon)({
  color: '#5c7080',
  marginRight: 7,
  fontSize: '17px !important',
});

const CategoryIcon = glamorous(Category)({
  color: '#5c7080',
  marginRight: 7,
  fontSize: '17px !important',
});

const MSDLinkIcon = glamorous(DescriptionIcon)({
  color: '#5c7080',
  marginRight: 7,
  fontSize: '17px !important',
});

const IndexIcon = glamorous(AddIcon)({
  color: '#5c7080',
  marginRight: 7,
  fontSize: '17px !important',
});

const CategorisationItem = glamorous.div({
  display: 'flex',
  alignItems: 'center',
});

const StyledMenuItem = glamorous(MenuItem)({
  display: 'flex !important',
});

const StyledTooltipContent = glamorous.div({
  maxWidth: 250,
  padding: 5,
});

const MenuActions = props => {
  const {
    artefactId,
    categorisation,
    artefactDelete,
    artefactExport,
    artefactTransfer,
    artefactTransferProps,
    dataExport,
    dataTransfer,
    getArtefactRelatives,
    hasData,
    isDeletable,
    isMany,
    isMSDLinkPossible,
    msdLinkToggle,
    sfsIndex,
    activate,
    selection,
  } = props;
  const count = isMany ? 2 : 1;
  const intl = useIntl();
  const activateHelpMessage = formatMessage(intl)(messages['activate.dataflow.help']);

  return (
    <Menu>
      {artefactTransfer && (
        <StyledMenuItem
          text={<FormattedMessage {...messages['transfer.artefact']} values={{ count }} />}
          iconName="swap-horizontal"
        >
          <MenuReferencesTransfer {...artefactTransferProps} onTransfer={artefactTransfer} />
        </StyledMenuItem>
      )}
      {hasData && dataTransfer && (
        <StyledMenuItem
          iconName="swap-horizontal"
          onClick={() => dataTransfer(artefactId)}
          text={<FormattedMessage {...messages['transfer.data']} values={{ count }} />}
        />
      )}
      {categorisation && (
        <StyledMenuItem
          onClick={categorisation}
          text={
            <CategorisationItem>
              <CategoryIcon />
              <FormattedMessage {...messages['categorise.artefact']} values={{ count }} />
            </CategorisationItem>
          }
        />
      )}
      {isMSDLinkPossible && (
        <StyledMenuItem
          onClick={msdLinkToggle}
          text={
            <CategorisationItem>
              <MSDLinkIcon />
              <FormattedMessage {...messages['link.msd']} />
            </CategorisationItem>
          }
        />
      )}
      {!R.isNil(getArtefactRelatives) && (
        <StyledMenuItem
          iconName="diagram-tree"
          onClick={() => getArtefactRelatives({ artefactId, mode: 'visualisation' })}
          text={<FormattedMessage {...messages['view.artefact.relatives']} values={{ count }} />}
        />
      )}
      {isDeletable && R.isNil(getArtefactRelatives) && (
        <StyledMenuItem
          iconName="trash"
          onClick={() => artefactDelete(artefactId)}
          text={<FormattedMessage {...messages['artefact.delete']} values={{ count }} />}
        />
      )}
      {isDeletable && !R.isNil(getArtefactRelatives) && (
        <StyledMenuItem
          iconName="trash"
          text={<FormattedMessage {...messages['artefact.delete']} values={{ count }} />}
        >
          <StyledMenuItem
            text={<FormattedMessage id="delete.without.relatives" />}
            onClick={() => artefactDelete(artefactId)}
          />
          <StyledMenuItem
            text={<FormattedMessage id="delete.with.relatives" />}
            onClick={() => getArtefactRelatives({ artefactId, mode: 'delete' })}
          />
        </StyledMenuItem>
      )}
      <StyledMenuItem
        text={<FormattedMessage {...messages['export.artefact']} values={{ count }} />}
        iconName="download"
      >
        <MenuReferencesSelector
          onSelection={withReferences => artefactExport(artefactId, withReferences)}
        />
      </StyledMenuItem>
      {hasData && (
        <StyledMenuItem
          text={<FormattedMessage {...messages['export.data.metadata']} />}
          onClick={dataExport}
          iconName="download"
        />
      )}
      {sfsIndex && R.is(Function, sfsIndex) && (
        <StyledMenuItem
          onClick={() => sfsIndex(artefactId)}
          text={
            <CategorisationItem>
              <IndexIcon />
              <FormattedMessage {...messages['sfs.index']} />
            </CategorisationItem>
          }
        />
      )}
      {R.is(Function, activate) && (
        <Popover
          content={<StyledTooltipContent>{activateHelpMessage}</StyledTooltipContent>}
          interactionKind={PopoverInteractionKind.HOVER}
          position={isMany ? Position.RIGHT : Position.LEFT}
          useSmartArrowPositioning={true}
        >
          <StyledMenuItem
            onClick={() => activate(selection)}
            text={
              <CategorisationItem>
                <ActivateIcon />
                <FormattedMessage {...messages['activate.dataflow']} />
              </CategorisationItem>
            }
          />
        </Popover>
      )}
    </Menu>
  );
};

MenuActions.propTypes = {
  artefactId: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
  categorisation: PropTypes.func,
  artefactDelete: PropTypes.func.isRequired,
  artefactExport: PropTypes.func.isRequired,
  dataExport: PropTypes.func.isRequired,
  dataTransfer: PropTypes.func,
  getArtefactRelatives: PropTypes.func,
  hasData: PropTypes.bool,
  isDeletable: PropTypes.bool,
  menuArtefactTransfer: PropTypes.func,
  isMany: PropTypes.bool,
  sfsIndex: PropTypes.func,
  selection: PropTypes.array,
};

//--------------------------------------------------------------------------------------------------
const Actions = props => {
  return (
    <Popover content={<MenuActions {...props} />} position={Position.LEFT} inline>
      <div>
        <Button iconName="menu" className={Classes.MINIMAL} />
      </div>
    </Popover>
  );
};

Actions.propTypes = MenuActions.propTypes;

export const getIsInternalDataflows = artefacts => {
  return R.all(
    R.allPass([R.propEq('type', 'dataflow'), R.hasPath(['space', 'transferUrl'])]),
    artefacts,
  );
};

export const getIsIndexableDataflows = artefacts => {
  return R.all(R.allPass([R.propEq('type', 'dataflow'), R.path(['space', 'indexed'])]), artefacts);
};

export const SelectionActions = ({ activate, deselect, dataExport, selection, ids }) => {
  const dispatch = useDispatch();
  const spaces = useSelector(getInternalSpaces());
  const selectedSpaces = R.pipe(
    R.reduce((acc, art) => {
      const space = art.space;
      if (R.has(space.id, acc)) {
        return acc;
      }
      return R.assoc(space.id, space, acc);
    }, {}),
    R.values,
  )(selection);
  const isAllInternals = R.all(space => {
    const transferUrl = R.prop('transferUrl', space);
    return !R.isNil(transferUrl) && !R.isEmpty(transferUrl);
  }, selectedSpaces);

  const isSelMSDLinkPossible = isMSDLinkPossible(R.indexBy(R.prop('id'), selection));
  const isAllOnSameSpace = R.length(selectedSpaces) === 1;
  const isAllDataflows = R.pipe(
    R.find(art => art.type !== 'dataflow'),
    R.isNil,
  )(selection);
  const isIndexableDataflows = getIsIndexableDataflows(selection);
  const categorisation =
    isAllInternals && isAllOnSameSpace && isAllDataflows
      ? () => {
          dispatch(
            artefactsCategorisation({ ids, space: R.pipe(R.head, R.prop('space'))(selection) }),
          );
          deselect(ids);
        }
      : null;
  const sfsIndex =
    isIndexableDataflows && window.CONFIG.hasSfs
      ? () => {
          dispatch(sfsIndexSelection(ids));
          deselect(ids);
        }
      : null;
  const onDelete = () => {
    dispatch(deleteArtefacts(ids));
    deselect(ids);
  };
  const artefactExport = (ids, wR) => {
    dispatch(exportArtefacts(ids, wR));
    deselect(ids);
  };
  const msdLinkToggle = () => {
    dispatch(fetchAllMSD(selection));
    deselect(ids);
  };
  const dataTransfer = () => {
    dispatch(openSelectionTransfer('data')(R.values(ids)));
  };
  const destinationSpaces = R.omit(R.pluck('id', selectedSpaces), spaces);
  const artefactTransfer = R.isEmpty(destinationSpaces)
    ? null
    : (space, withReferences) => () => {
        dispatch(transferArtefacts(ids, space, withReferences));
        deselect(ids);
      };
  const artefactTransferProps = {
    destinationSpaces: R.values(destinationSpaces),
    sourceSpaces: selectedSpaces,
  };

  return (
    <Actions
      activate={activate}
      artefactId={ids}
      artefactDelete={onDelete}
      artefactExport={artefactExport}
      artefactTransfer={artefactTransfer}
      artefactTransferProps={artefactTransferProps}
      categorisation={categorisation}
      dataExport={dataExport}
      dataTransfer={R.isEmpty(destinationSpaces) ? null : dataTransfer}
      hasData={isAllDataflows}
      isDeletable={isAllInternals}
      isMany={true}
      isMSDLinkPossible={isSelMSDLinkPossible}
      msdLinkToggle={msdLinkToggle}
      sfsIndex={sfsIndex}
      selection={selection}
    />
  );
};
SelectionActions.propTypes = {
  activate: PropTypes.func,
  dataExport: PropTypes.func,
  ids: PropTypes.object,
  selection: PropTypes.array,
  deselect: PropTypes.func,
};

export const ItemActions = compose(
  connect(
    createStructuredSelector({
      spaces: getInternalSpaces(),
    }),
    {
      categorisation: artefactCategorisation,
      getArtefactRelatives,
      dataTransfer: openItemTransfer('data'),
      fetchAllMSD,
      sfsIndex,
      artefactTransfer: transferArtefact,
    },
  ),
  withProps(({ artefact, fetchAllMSD }) => ({
    isMSDLinkPossible: isArtefactMSDLinkPossible({ artefact }),
    msdLinkToggle: () => fetchAllMSD({ artefact }),
  })),
  mapProps(({ artefact, artefactTransfer, sfsIndex, categorisation, spaces, ...rest }) => {
    const isInternalDataflows = getIsInternalDataflows([artefact]);
    const isIndexableDataflows = getIsIndexableDataflows([artefact]);
    return {
      ...rest,
      artefact,
      categorisation: isInternalDataflows ? () => categorisation(artefact) : null,
      sfsIndex: window.CONFIG.hasSfs && isIndexableDataflows ? sfsIndex : null,
      artefactTransfer: (space, wR) => () => artefactTransfer(artefact.id, space, wR),
      artefactTransferProps: {
        sourceSpaces: [artefact.space],
        destinationSpaces: R.reject(space => space.id === artefact.space.id, R.values(spaces)),
      },
      selection: [artefact],
    };
  }),
)(Actions);
