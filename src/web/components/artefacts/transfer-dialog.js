import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import {
  Button,
  Checkbox,
  Classes,
  Dialog,
  EditableText,
  Colors,
} from '@blueprintjs/core';
import { compose, withProps } from 'recompose';
import glamorous from 'glamorous';
import { injectIntl } from 'react-intl';
import VerifiedUser from '@material-ui/icons/VerifiedUser';
import Space from '../common/space';
import { withTransferControl } from '../../modules/transfer-control';
import { formatMessage } from '../../modules/i18n';
import DataValidationForm, { Menu } from '../common/data-validation';
import { withDataValidation } from '../common/withDataValidation';
import messages from '../messages';

const GDialog = glamorous(Dialog)({ width: 'auto !important' });

const SpaceOption = glamorous.div({
  border: '1px solid black',
  boxSizing: 'border-box',
  padding: 10,
  alignItems: 'center',
  display: 'flex',
  flexDirection: 'row',
  marginRight: 15,
});

const Container = glamorous.div({
  marginBottom: 20,
});

const Spaces = glamorous.div({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'flex-start',
});

const WrapperCheckbox = glamorous.div({
  marginBottom: '25px',
});

const StyledCheckbox = glamorous(Checkbox)({
  lineHeight: '20px !important',
  marginBottom: '0px !important',
  minHeight: '20px !important',
  '& .pt-control-indicator': {
    top: '2px !important',
  },
});
const ValidationIcon = glamorous(VerifiedUser)({
  color: Colors.GRAY1,
});
const GTh = glamorous.th({ textAlign: 'center !important' });
const GTd = glamorous.td({ textAlign: 'center !important' });

const Input = glamorous(EditableText)({
  backgroundColor: 'white',
  minWidth: 100,
  height: 20,
});

const InputContainer = glamorous.div(
  {
    display: 'flex',
    flexDirection: 'column',
  },
  ({ hasDataQuery }) => {
    if (!hasDataQuery) {
      return { display: 'none' };
    }
    return {};
  },
);

const QueryInfo = glamorous.span({
  marginBottom: 10,
  marginTop: 10,
});

const Items = ({ intl, artefacts }) => (
  <table className={`${Classes.TABLE} ${Classes.TABLE_CONDENSED}`}>
    <thead>
      <tr>
        <GTh>
          <span>{formatMessage(intl)(messages['transfer.artefact.type'])}</span>
        </GTh>
        <GTh>
          <span>{formatMessage(intl)(messages['transfer.artefact.id'])}</span>
        </GTh>
        <GTh>
          <span>{formatMessage(intl)(messages['transfer.artefact.name'])}</span>
        </GTh>
        <GTh>
          <span>
            {formatMessage(intl)(messages['transfer.artefact.source'])}
          </span>
        </GTh>
      </tr>
    </thead>
    <tbody>
      {R.map(artefact => {
        const space = R.propOr({}, 'space', artefact);
        const type = R.prop('type', artefact);
        return (
          <tr key={R.prop('id', artefact)}>
            <GTd>
              <Space
                {...space}
                label={
                  <span>
                    {formatMessage(intl)(
                      R.propOr(
                        { id: `artefact.type.${type}` },
                        `artefact.type.${type}`,
                        messages,
                      ),
                    )}
                  </span>
                }
              />
            </GTd>
            <GTd>{R.prop('code', artefact)}</GTd>
            <GTd>{R.prop('label', artefact)}</GTd>
            <GTd>
              <Space {...space} />
            </GTd>
          </tr>
        );
      }, R.values(artefacts))}
    </tbody>
  </table>
);

Items.propTypes = {
  artefacts: PropTypes.array,
  intl: PropTypes.object.isRequired,
};

const DataQuery = ({
  artefactsCount,
  dataQuery,
  hasDataQuery,
  toggleHasDataQuery,
  updateDataQuery,
  intl,
}) => {
  if (artefactsCount !== 1) {
    return null;
  }
  return (
    <WrapperCheckbox>
      <StyledCheckbox
        checked={hasDataQuery}
        label={
          <span>{formatMessage(intl)(messages['transfer.dataquery'])}</span>
        }
        onChange={toggleHasDataQuery}
      />
      <InputContainer hasDataQuery={hasDataQuery}>
        <QueryInfo>
          <span>
            {formatMessage(intl)(messages['transfer.dataquery.info'])}
          </span>
        </QueryInfo>
        <Input
          onChange={updateDataQuery}
          placeholder={
            <span>
              {formatMessage(intl)(messages['transfer.dataquery.placeholder'])}
            </span>
          }
          value={R.isNil(dataQuery) ? '' : dataQuery}
        />
      </InputContainer>
    </WrapperCheckbox>
  );
};

DataQuery.propTypes = {
  artefactsCount: PropTypes.number,
  dataQuery: PropTypes.string,
  hasDataQuery: PropTypes.bool,
  toggleHasDataQuery: PropTypes.func.isRequired,
  updateDataQuery: PropTypes.func.isRequired,
  intl: PropTypes.object.isRequired,
};

const TransferDialog = props => {
  const {
    artefacts,
    closeTransfer,
    dataQuery,
    destinationSpace,
    hasDataQuery,
    isOpenMenu,
    spaces,
    toggleDestinationSpace,
    toggleHasDataQuery,
    transferType,
    updateDataQuery,
    intl,
    validation,
    selectionIds,
    deselect,
    transfer,
  } = props;

  const count = R.length(artefacts);
  if (!transferType || count === 0) return null;

  const validationLinks = {
    validation: formatMessage(intl)(messages['data.validation.info']),
    pit: formatMessage(intl)(messages['pit.transfer.info']),
  };

  const { resetValidationOptions } = validation;

  const onTransfer = () => {
    R.forEach(artefact => {
      transfer(artefact.id, destinationSpace, null, dataQuery, validation);
    }, artefacts);
    if (R.is(Function, resetValidationOptions)) {
      resetValidationOptions();
    }
    deselect(R.indexBy(R.identity, R.pluck('id', artefacts)));
  };

  return (
    <GDialog
      iconName="swap-horizontal"
      isOpen={isOpenMenu}
      onClose={closeTransfer}
      title={
        <span>
          {formatMessage(intl)(
            R.propOr(
              { id: `transfer.${transferType}` },
              `transfer.${transferType}`,
              messages,
            ),
            { count },
          )}
        </span>
      }
    >
      <div className={Classes.DIALOG_BODY}>
        <Container>
          <Items artefacts={artefacts} intl={intl} />
        </Container>
        <Container>
          <span>{formatMessage(intl)(messages['transfer.destination'])}</span>
          <Spaces>
            {R.map(space => {
              const spaceId = R.prop('id', space);
              return (
                <SpaceOption key={spaceId}>
                  <StyledCheckbox
                    checked={spaceId === R.prop('id', destinationSpace)}
                    onChange={() => toggleDestinationSpace(space)}
                  />
                  <Space {...space} />
                </SpaceOption>
              );
            }, R.values(spaces))}
          </Spaces>
        </Container>
        {transferType === 'data' ? (
          <DataQuery
            artefactsCount={R.length(R.keys(artefacts))}
            dataQuery={dataQuery}
            hasDataQuery={hasDataQuery}
            toggleHasDataQuery={toggleHasDataQuery}
            updateDataQuery={updateDataQuery}
            intl={intl}
          />
        ) : null}
        {transferType === 'data' ? (
          <Menu
            icon={<ValidationIcon />}
            labels={{
              title: formatMessage(props.intl)({ id: 'data.validations' }),
              basic: formatMessage(props.intl)({
                id: 'data.validation.default',
              }),
              advanced: formatMessage(props.intl)({
                id: 'data.validation.advanced',
              }),
              nochange: formatMessage(props.intl)({
                id: 'data.validation.nochange',
              }),
            }}
            link={validationLinks.validation}
            onChange={validation.onChangeValidationType}
            options={['basic', 'advanced', 'nochange']}
            value={validation.validationType}
          />
        ) : null}
        {transferType === 'data' ? (
          <DataValidationForm
            {...validation}
            links={validationLinks}
            intl={intl}
            title={formatMessage(intl)(
              R.prop('data.validation.menu.dialog', messages),
            )}
          />
        ) : null}
      </div>
      <div className={Classes.DIALOG_FOOTER}>
        <div className={Classes.DIALOG_FOOTER_ACTIONS}>
          <Button onClick={() => (closeTransfer(), deselect(selectionIds))}>
            <span>{formatMessage(intl)(messages['transfer.cancel'])}</span>
          </Button>
          <Button
            className={Classes.INTENT_PRIMARY}
            disabled={R.isNil(destinationSpace)}
            onClick={onTransfer}
          >
            <span>{formatMessage(intl)(messages['transfer.apply'])}</span>
          </Button>
        </div>
      </div>
    </GDialog>
  );
};

TransferDialog.propTypes = {
  artefacts: PropTypes.array,
  closeTransfer: PropTypes.func.isRequired,
  dataQuery: PropTypes.string,
  destinationSpace: PropTypes.object,
  hasDataQuery: PropTypes.bool,
  isOpenMenu: PropTypes.bool,
  transfer: PropTypes.func,
  spaces: PropTypes.array,
  toggleDestinationSpace: PropTypes.func.isRequired,
  toggleHasDataQuery: PropTypes.func.isRequired,
  transferType: PropTypes.string,
  updateDataQuery: PropTypes.func.isRequired,
  validation: PropTypes.object,
  intl: PropTypes.object.isRequired,
  selectionIds: PropTypes.array,
  deselect: PropTypes.func,
  artefactId: PropTypes.string,
};

export default compose(
  injectIntl,
  withTransferControl,
  withDataValidation,
  withProps(({ artefacts, transferData, spaces, deselect, validation }) => {
    const selectedSpaces = R.reduce(
      (acc, art) => {
        const space = art.space;
        if (R.has(space.id, acc)) {
          return acc;
        }
        return R.assoc(space.id, space, acc);
      },
      {},
      artefacts,
    );

    const hasSource = R.all(
      R.pipe(R.path(['space', 'transferUrl']), R.isNil, R.not),
      artefacts,
    );
    return {
      artefacts,
      transfer: transferData,
      hasSource,
      deselect,
      spaces: R.values(
        R.reject(space => R.has(space.id, selectedSpaces), spaces),
      ),
      validation: R.assoc('hasSource', hasSource, validation),
    };
  }),
)(TransferDialog);
