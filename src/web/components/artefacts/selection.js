import React, { useState } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { Button, Classes, Popover, Position, Menu, MenuItem } from '@blueprintjs/core';
import glamorous from 'glamorous';
import IndeterminateCheckBoxIcon from '@material-ui/icons/IndeterminateCheckBox';
import LibraryAddCheckIcon from '@material-ui/icons/LibraryAddCheck';
import FilterNoneIcon from '@material-ui/icons/FilterNone';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import { ExportDataDialog } from '../dialog-menus';
import { FormattedMessage } from '../../modules/i18n';
import { SelectionActions } from './actions';
import SelectAllIconSinglePage from '@material-ui/icons/CheckBox';
import DeselectAllIconSinglePage from '@material-ui/icons/CheckBoxOutlineBlank';
const SelectAllIcon = glamorous(LibraryAddCheckIcon)({
  color: '#5c7080',
  fontSize: '17px !important',
  marginRight: 7,
});

const SelectPageIcon = glamorous(CheckBoxIcon)({
  color: '#5c7080',
  fontSize: '17px !important',
  marginRight: 7,
});

const DeselectAllIcon = glamorous(FilterNoneIcon)({
  color: '#5c7080',
  fontSize: '17px !important',
  marginRight: 7,
});

const StyledMenuItem = glamorous(MenuItem)({
  display: 'flex !important',
});

const ItemContent = glamorous.div({
  display: 'flex',
  alignItems: 'center',
});

const AdvSelectionIcon = ({ hasPages, selection, selectAll, selectPage }) => {
  let Icon = IndeterminateCheckBoxIcon;
  if (R.isEmpty(selection)) {
    if (!hasPages) {
      Icon = DeselectAllIconSinglePage;
    } else {
      Icon = FilterNoneIcon;
    }
  } else if (R.isNil(selectAll)) {
    if (!hasPages) {
      Icon = SelectAllIconSinglePage;
    } else {
      Icon = LibraryAddCheckIcon;
    }
  } else if (R.isNil(selectPage) && hasPages) {
    Icon = CheckBoxIcon;
  }
  const StyledIcon = glamorous(Icon)({
    color: '#5c7080',
    fontSize: '17px !important',
    marginTop: 7,
  });
  return <StyledIcon />;
};
AdvSelectionIcon.propTypes = {
  hasPages: PropTypes.bool,
  selection: PropTypes.array,
  selectAll: PropTypes.func,
  selectPage: PropTypes.func,
};

const AdvSelectionMenu = ({ deselectAll, selectAll, selectPage, hasPages }) => (
  <Menu>
    {R.is(Function, selectAll) && (
      <StyledMenuItem
        onClick={selectAll}
        text={
          <ItemContent>
            {hasPages ? (
              <SelectAllIcon />
            ) : (
              <SelectAllIconSinglePage color="inherit" fontSize="small" />
            )}
            <FormattedMessage id="artefacts.select.all" />
          </ItemContent>
        }
      />
    )}
    {R.is(Function, selectPage) && (
      <StyledMenuItem
        onClick={selectPage}
        text={
          <ItemContent>
            <SelectPageIcon />
            <FormattedMessage id="artefacts.select.page" />
          </ItemContent>
        }
      />
    )}
    {R.is(Function, deselectAll) && (
      <StyledMenuItem
        onClick={deselectAll}
        text={
          <ItemContent>
            {hasPages ? (
              <DeselectAllIcon />
            ) : (
              <DeselectAllIconSinglePage color="inherit" fontSize="small" />
            )}
            <FormattedMessage id="artefacts.deselect.all" />
          </ItemContent>
        }
      />
    )}
  </Menu>
);

AdvSelectionMenu.propTypes = {
  deselectAll: PropTypes.func,
  selectAll: PropTypes.func,
  selectPage: PropTypes.func,
  hasPages: PropTypes.bool,
};

export const Selection = ({
  activate,
  deselect,
  deselectAll,
  hasPages,
  selectAll,
  selectPage,
  selection,
}) => {
  const selectionIds = R.pipe(R.pluck('id'), R.indexBy(R.identity))(selection);
  const [isOpenExportDataMenu, setIsOpenExportDataMenu] = useState(false);
  const [isAdvMenuOpen, setIsAdvMenuOpen] = useState(false);

  return (
    <div>
      {isOpenExportDataMenu && (
        <ExportDataDialog
          onClose={() => setIsOpenExportDataMenu(false)}
          selection={R.pluck('id', selection)}
          exportCallback={deselectAll}
        />
      )}
      <Popover
        content={
          <AdvSelectionMenu
            deselectAll={deselectAll}
            selectAll={selectAll}
            selectPage={selectPage}
            hasPages={hasPages}
          />
        }
        isOpen={isAdvMenuOpen}
        onClose={() => setIsAdvMenuOpen(false)}
        openOnTargetFocus={false}
        position={Position.BOTTOM}
        inline
      >
        <Button className={Classes.MINIMAL} onClick={() => setIsAdvMenuOpen(!isAdvMenuOpen)}>
          <AdvSelectionIcon
            hasPages={hasPages}
            selection={selection}
            selectAll={selectAll}
            selectPage={selectPage}
          />
        </Button>
      </Popover>
      {!R.isNil(selection) && !R.isEmpty(selection) && (
        <SelectionActions
          activate={R.length(selection) === 1 ? activate : null}
          deselect={deselect}
          selection={selection}
          ids={selectionIds}
          dataExport={() => setIsOpenExportDataMenu(true)}
        />
      )}
    </div>
  );
};

Selection.propTypes = {
  activate: PropTypes.func,
  deselect: PropTypes.func,
  deselectAll: PropTypes.func,
  hasPages: PropTypes.bool,
  selectAll: PropTypes.func,
  selectPage: PropTypes.func,
  selection: PropTypes.array,
};

export default Selection;
