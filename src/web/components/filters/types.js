import React from 'react';
import * as R from 'ramda';
import Item from './item';
import { withList, withSpotlight } from '../list';
import { withConfig } from '../../modules/config';
import { withFilters } from '../../modules/filters';
import { onlyUpdateForKeys, renameProps, compose, withProps } from 'recompose';
import { injectIntl } from 'react-intl';
import { formatMessage, FormattedMessage } from '../../modules/i18n';
import messages from '../messages';
import SpotlightList from './spotlight-list';

export default compose(
  injectIntl,
  withConfig,
  withFilters,
  onlyUpdateForKeys(['isFetching', 'typeToggle', 'typeClear', 'types']),
  renameProps({ typeToggle: 'onChangeHandler', typeClear: 'clearHandler', types: 'items' }),
  withProps(({ onChangeHandler, items, intl }) => ({
    items: R.map(
      item => ({
        ...item,
        label: formatMessage(intl)(
          R.propOr({ id: `artefact.type.${item.id}` }, `artefact.type.${item.id}`, messages),
        ),
      }),
      items,
    ),
    itemRenderer: Item,
    handlers: { onChangeHandler },
    isBlank: R.isEmpty(items),
    isOpen: true,
    title: <FormattedMessage id="artefacts.filters.types.title" />,
  })),
  withList(),
  withSpotlight([{ id: 'label', accessor: 'label', isSelected: true }]),
)(SpotlightList);
