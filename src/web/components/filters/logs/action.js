import React from 'react';
import * as R from 'ramda';
import { Checkbox, FormControlLabel } from '@material-ui/core';
import { useIntl } from 'react-intl';
import { FormattedMessage, formatMessage } from '../../../modules/i18n';
import Filter from '../filter';
import { useDispatch, useSelector } from 'react-redux';
import { getRequestId, getSelectedActionIds } from '../../../modules/logs/selectors';
import { toggleActionId } from '../../../modules/logs/action-creators';
import { ACTION_IDS } from '../../../modules/logs/model';

const ActionFilter = () => {
  const intl = useIntl();
  const dispatch = useDispatch();
  const requestId = useSelector(getRequestId);
  const selectedActionIds = useSelector(getSelectedActionIds);

  const actionIds = R.defaultTo(ACTION_IDS, window?.SETTINGS?.logs?.actionIds);

  const changeHandler = id => {
    dispatch(toggleActionId(id));
  };

  return (
    <Filter
      title={<FormattedMessage id="logs.filters.action.title" />}
      isOpen
      isDisabled={!!requestId}
    >
      {R.map(
        id => (
          <FormControlLabel
            key={id}
            style={{ width: '90%' }} // keep it simple with MUI, not 100% to pesky scrollbar while collapsing
            control={
              <Checkbox
                size="small"
                checked={selectedActionIds.has(id)}
                onChange={() => changeHandler(id)}
                color="primary"
              />
            }
            label={formatMessage(intl)({ id: `logs.filters.action.${id}` })}
          />
        ),
        actionIds,
      )}
    </Filter>
  );
};

export default ActionFilter;
