import React, { useEffect, useState } from 'react';
import * as R from 'ramda';
import { Button, InputAdornment, TextField } from '@material-ui/core';
import { defineMessages, useIntl } from 'react-intl';
import { FormattedMessage, formatMessage } from '../../../modules/i18n';
import Filter from '../filter';
import { useDispatch, useSelector } from 'react-redux';
import { getRequestId } from '../../../modules/logs/selectors';
import { changeRequestId } from '../../../modules/logs/action-creators';

const messages = defineMessages({
  help: { id: 'logs.filters.request.help' },
  placeholder: { id: 'logs.filters.request.placeholder' },
});

export const RequestFilter = () => {
  const intl = useIntl();
  const dispatch = useDispatch();
  const requestId = useSelector(getRequestId);

  const [localRequestId, setLocalRequestId] = useState();
  useEffect(() => {
    setLocalRequestId(requestId);
  }, [requestId]);

  const changeHandler = id => {
    dispatch(changeRequestId(id));
  };

  return (
    <Filter
      title={<FormattedMessage id="logs.filters.request.title" />}
      clearHandler={() => changeHandler()}
      isOpen
    >
      <TextField
        fullWidth
        size="small"
        type="number"
        variant="outlined"
        helperText={formatMessage(intl)(messages.help)}
        placeholder={formatMessage(intl)(messages.placeholder)}
        value={R.defaultTo('', localRequestId)} // avoid the (un)controlled issue
        onChange={e => setLocalRequestId(e.target.value)}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <Button
                edge="end"
                color="secondary"
                size="small"
                variant="contained"
                onClick={() => changeHandler(localRequestId)}
              >
                <FormattedMessage id="logs.filters.request.apply" />
              </Button>
            </InputAdornment>
          ),
        }}
      />
    </Filter>
  );
};

export default RequestFilter;
