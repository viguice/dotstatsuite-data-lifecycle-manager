import React from 'react';
import * as R from 'ramda';
import { Radio, FormControlLabel } from '@material-ui/core';
import { useIntl } from 'react-intl';
import { FormattedMessage, formatMessage } from '../../../modules/i18n';
import Filter from '../filter';
import { useDispatch, useSelector } from 'react-redux';
import { getRequestId, getExecutionOutcomeId } from '../../../modules/logs/selectors';
import { changeExecutionOutcomeId } from '../../../modules/logs/action-creators';
import { EXECUTION_OUTCOME_IDS } from '../../../modules/logs/model';

const ExecutionOutcomeFilter = () => {
  const intl = useIntl();
  const dispatch = useDispatch();
  const requestId = useSelector(getRequestId);
  const executionOutcomeId = useSelector(getExecutionOutcomeId);

  const ids = R.defaultTo(EXECUTION_OUTCOME_IDS, window?.SETTINGS?.logs?.executionOutcomeIds);

  const changeHandler = id => {
    dispatch(changeExecutionOutcomeId(id));
  };

  return (
    <Filter
      title={<FormattedMessage id="logs.filters.execution.outcome.title" />}
      isOpen
      isDisabled={!!requestId}
      clearHandler={() => changeHandler()}
    >
      {R.map(
        id => (
          <FormControlLabel
            key={id}
            style={{ width: '90%' }} // keep it simple with MUI, not 100% to pesky scrollbar while collapsing
            control={
              <Radio
                size="small"
                checked={id === executionOutcomeId}
                onChange={() => changeHandler(id)}
                color="primary"
              />
            }
            label={formatMessage(intl)({ id: `logs.filters.execution.outcome.${id}` })}
          />
        ),
        ids,
      )}
    </Filter>
  );
};

export default ExecutionOutcomeFilter;
