import React from 'react';
import * as R from 'ramda';
import { Radio, FormControlLabel } from '@material-ui/core';
import { useIntl } from 'react-intl';
import { FormattedMessage, formatMessage } from '../../../modules/i18n';
import Filter from '../filter';
import { useDispatch, useSelector } from 'react-redux';
import { getRequestId, getExecutionStatusId } from '../../../modules/logs/selectors';
import { changeExecutionStatusId } from '../../../modules/logs/action-creators';
import { EXECUTION_STATUS_IDS } from '../../../modules/logs/model';

const ExecutionStatusFilter = () => {
  const intl = useIntl();
  const dispatch = useDispatch();
  const requestId = useSelector(getRequestId);
  const executionStatusIds = useSelector(getExecutionStatusId);

  const ids = R.defaultTo(EXECUTION_STATUS_IDS, window?.SETTINGS?.logs?.executionStatusIds);

  const changeHandler = id => {
    dispatch(changeExecutionStatusId(id));
  };

  return (
    <Filter
      title={<FormattedMessage id="logs.filters.execution.status.title" />}
      isOpen
      isDisabled={!!requestId}
      clearHandler={() => changeHandler()}
    >
      {R.map(
        id => (
          <FormControlLabel
            key={id}
            style={{ width: '90%' }} // keep it simple with MUI, not 100% to pesky scrollbar while collapsing
            control={
              <Radio
                size="small"
                checked={id === executionStatusIds}
                onChange={() => changeHandler(id)}
                color="primary"
              />
            }
            label={formatMessage(intl)({ id: `logs.filters.execution.status.${id}` })}
          />
        ),
        ids,
      )}
    </Filter>
  );
};

export default ExecutionStatusFilter;
