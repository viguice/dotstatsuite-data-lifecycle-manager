import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { useDispatch } from 'react-redux';
import SpotlightList from '../../spotlight-list';
import { RadioItem } from '../../item';
import { withList, withSpotlight } from '../../../list/index';
import useTransferLogs from '../../../../hooks/useTransferLogs';

export const SpotlightExclusive = ({
  title,
  currentIdHandler,
  idAccessor,
  isDisabled,
  itemRenderer = RadioItem,
  isSelectedHandler,
}) => {
  const dispatch = useDispatch();
  const { isLoading, logs } = useTransferLogs();

  const items = useMemo(() => {
    return R.pipe(R.uniqBy(idAccessor), R.reject(R.pipe(idAccessor, R.isNil)))(logs);
  }, [logs]);

  const changeHandler = id => {
    dispatch(currentIdHandler(id));
  };

  const EnhancedSpotlightList = R.compose(
    withList(),
    withSpotlight([{ id: idAccessor, accessor: idAccessor, isSelected: true }]),
  )(SpotlightList);

  return (
    <EnhancedSpotlightList
      title={title}
      clearHandler={() => changeHandler()}
      isBlank={R.isEmpty(items)}
      isFetching={isLoading}
      isDisabled={isDisabled}
      isOpen
      items={items}
      itemRenderer={itemRenderer}
      handlers={{ onChangeHandler: changeHandler }}
      accessors={{
        id: idAccessor,
        label: idAccessor,
        isSelected: R.pipe(idAccessor, isSelectedHandler),
      }}
    />
  );
};

SpotlightExclusive.propTypes = {
  title: PropTypes.node,
  currentIdHandler: PropTypes.func,
  idAccessor: PropTypes.func,
  isDisabled: PropTypes.bool,
  itemRenderer: PropTypes.func,
  isSelectedHandler: PropTypes.func,
};

export default SpotlightExclusive;
