import React from 'react';
import { FormattedMessage } from '../../../modules/i18n';
import spaces from '../spaces';
import ActionFilter from './action';
import RequestFilter from './request';
import StructureFilter from './structure';
import UserFilter from './user';
import SubmissionFilter from './submission';
import ExecutionStatusFilter from './execution-status';
import ExecutionOutcomeFilter from './execution-outcome';

const InternalSpaces = spaces('internalSpaces');

const Filters = () => {
  return (
    <div>
      <InternalSpaces title={<FormattedMessage id="artefacts.filters.spaces.title" />} />
      <RequestFilter />
      <ActionFilter />
      <StructureFilter />
      <SubmissionFilter />
      <UserFilter />
      <ExecutionStatusFilter />
      <ExecutionOutcomeFilter />
    </div>
  );
};

Filters.propTypes = {};

export default Filters;
