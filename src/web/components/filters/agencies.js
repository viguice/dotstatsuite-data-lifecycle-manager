import React from 'react';
import Filter from './filter';
import { withAgencies } from '../../modules/agencies';
import { withFilters } from '../../modules/filters';
import { Tree } from '@blueprintjs/core';
import { onlyUpdateForKeys, renameProps, compose, withProps } from 'recompose';
import { isEmpty, map } from 'ramda';
import glamorous from 'glamorous';
import { Spotlight, withSpotlight, withList } from '../list';
import { FormattedMessage } from '../../modules/i18n';
import { Position, Tooltip } from '@blueprintjs/core';
export const AgenciesTree = compose(
  renameProps({
    agencyToggle: 'onNodeClick',
    agencyExpandNode: 'onNodeExpand',
    agencyCollapseNode: 'onNodeCollapse',
    items: 'contents',
  }),
)(Tree);

const AgenciesTreeWrapper = glamorous.div({
  borderTop: '1px dashed #ccc',
  '& .pt-tree-node-content': {
    minHeight: 34,
  },
});

const TreeWrapper = glamorous.div({
  maxHeight: 250,
  overflow: 'auto',
});

const SpotlightWrapper = glamorous.div({
  paddingTop: 10,
  paddingBottom: 10,
});

export const Agencies = props => {
  const addToolTip = agencies => {
    return map(agency => ({
      ...agency,
      label: (
        <Tooltip
          content={`(${agency.code}) ${agency.name}`}
          position={Position.BOTTOM}
        >
          {`(${agency.code}) ${agency.name}`}
        </Tooltip>
      ),
      childNodes: agency.childNodes && addToolTip(agency.childNodes),
    }))(agencies);
  };
  return (
    <Filter
      {...props}
      title={<FormattedMessage id="artefacts.filters.agencies.title" />}
    >
      {() => (
        <div>
          <AgenciesTreeWrapper>
            <SpotlightWrapper>
              <Spotlight
                {...props.spotlight}
                fieldChange={props.spotlightFieldChange}
                valueChange={props.spotlightValueChange}
              />
            </SpotlightWrapper>
            <TreeWrapper>
              <AgenciesTree {...props} items={addToolTip(props.list.items)} />
            </TreeWrapper>
          </AgenciesTreeWrapper>
        </div>
      )}
    </Filter>
  );
};

export default compose(
  withFilters,
  withAgencies,
  onlyUpdateForKeys(['isFetching', 'agencyToggle', 'agencyClear', 'agencies']),
  renameProps({ agencies: 'items', agencyClear: 'clearHandler' }),
  withList({ isTree: true }),
  withSpotlight([
    { id: 'label', accessor: 'label', isSelected: true },
    { id: 'code', accessor: 'code', isSelected: true },
  ]),
  withProps(({ items }) => ({ isBlank: isEmpty(items), isOpen: true })),
)(Agencies);
