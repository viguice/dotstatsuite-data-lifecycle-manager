import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { Checkbox } from '@blueprintjs/core';
import glamorous from 'glamorous';
import { Radio, FormControlLabel } from '@material-ui/core';

const itemPropTypes = {
  id: PropTypes.string,
  label: PropTypes.string,
  children: PropTypes.element,
  isSelected: PropTypes.bool,
  handlers: PropTypes.objectOf(PropTypes.func),
  accessors: PropTypes.objectOf(PropTypes.func),
};

const StyledCheckbox = glamorous(Checkbox)({
  minHeight: '20px !important',
  lineHeight: '20px !important',
  '& .pt-control-indicator': {
    top: '2px !important',
  },
});

const CheckBoxItem = ({ children, handlers, accessors, ...item }) => {
  const idAccessor = R.defaultTo(R.prop('id'), R.prop('id', accessors));
  const labelAccessor = R.defaultTo(R.prop('label'), R.prop('label', accessors));
  const isSelectedAccessor = R.defaultTo(R.prop('isSelected'), R.prop('isSelected', accessors));

  return (
    <StyledCheckbox
      onChange={() => handlers.onChangeHandler(idAccessor(item))}
      checked={!!isSelectedAccessor(item)}
    >
      {children ? children : labelAccessor(item)}
    </StyledCheckbox>
  );
};

CheckBoxItem.propTypes = itemPropTypes;

export default CheckBoxItem; // backward compatible without a sweat

export const RadioItem = ({ children, handlers, accessors, ...item }) => {
  const idAccessor = R.defaultTo(R.prop('id'), R.prop('id', accessors));
  const labelAccessor = R.defaultTo(R.prop('label'), R.prop('label', accessors));
  const isSelectedAccessor = R.defaultTo(R.prop('isSelected'), R.prop('isSelected', accessors));

  return (
    <FormControlLabel
      style={{ width: '90%' }} // keep it simple with MUI, not 100% to pesky scrollbar while collapsing
      control={
        <Radio
          size="small"
          checked={isSelectedAccessor(item)}
          onChange={() => handlers.onChangeHandler(idAccessor(item))}
          color="primary"
        />
      }
      label={children ? children : labelAccessor(item)}
    />
  );
};

RadioItem.propTypes = itemPropTypes;
