import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import glamorous from 'glamorous';
import { Classes, Button } from '@blueprintjs/core';
import { map, pick } from 'ramda';

const ActionsStyled = glamorous.div({
  display: 'flex !important',
  justifyContent: 'center',
});

const Actions = ({ actions = [] }) => {
  return (
    <ActionsStyled className={cx(Classes.BUTTON_GROUP, Classes.MINIMAL)}>
      {map(
        action => (
          <Button
            className={Classes.MINIMAL}
            key={action.id}
            {...pick(['disabled', 'iconName', 'intent', 'loading', 'onClick'], action)}
          >
            {action.label}
          </Button>
        ),
        actions,
      )}
    </ActionsStyled>
  );
};

Actions.propTypes = {
  actions: PropTypes.arrayOf(
    PropTypes.shape({
      disabled: PropTypes.bool,
      iconName: PropTypes.string,
      id: PropTypes.string.isRequired,
      intent: PropTypes.number,
      loading: PropTypes.bool,
      onClick: PropTypes.func.isRequired,
      label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    }),
  ),
};

export default Actions;
