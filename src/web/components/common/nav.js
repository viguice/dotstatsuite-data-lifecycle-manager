import React from 'react';
import PropTypes from 'prop-types';
import { partition, has, reject, propEq, pipe, identity, reverse } from 'ramda';
import Menu from './menu';
import Lang from './lang';
import Me from './me';
import { Classes } from '@blueprintjs/core';
import classnames from 'classnames';
import { withRouter, NavLink } from 'react-router-dom';

import glamorous from 'glamorous';
import { useGetUserRights } from '../../hooks/user-rights';
import { hasManagePermission } from '../../lib/permissions';
import { useSelector } from 'react-redux';
import { getIsRtl } from '../../modules/i18n/selectors';

const NavStyled = glamorous.nav({
  height: '60px !important',
  backgroundColor: '#0965C1 !important',
  display: 'flex !important',
  alignItems: 'center',
  justifyContent: 'space-between',
  boxShadow: 'none !important',
  '& img': {
    maxHeight: 48,
  },
});

const Nav = ({ routes }) => {
  const isRtl = useSelector(getIsRtl);
  const [_explicitRoutes, implicitRoutes] = pipe(
    partition(route => has('translationKey', route)),
    isRtl ? reverse : identity,
  )(routes);
  const { rights } = useGetUserRights({ queryKey: 'my-rights' });
  const canManage = hasManagePermission(rights || []);
  const explicitRoutes = pipe(
    reject(propEq('name', 'myRights')),
    canManage ? identity : reject(propEq('name', 'allRights')),
    isRtl ? reverse : identity,
  )(_explicitRoutes);
  return (
    <NavStyled className={classnames('nav', Classes.NAVBAR, Classes.FIXED_TOP, Classes.DARK)}>
      <div className={classnames(Classes.NAVBAR_GROUP)}>
        <NavLink className={classnames(Classes.NAVBAR_HEADING)} to="/" exact>
          <img src={window.SETTINGS.assets.logo} alt="logo" />
        </NavLink>
      </div>
      <div className={classnames(Classes.NAVBAR_GROUP)}>
        <Menu routes={implicitRoutes} />
        <span className={Classes.NAVBAR_DIVIDER}></span>
        <Menu routes={explicitRoutes} />
        <span className={Classes.NAVBAR_DIVIDER}></span>
        <Me />
        <span className={Classes.NAVBAR_DIVIDER}></span>
        <Lang />
      </div>
    </NavStyled>
  );
};

Nav.propTypes = {
  routes: PropTypes.array,
};

export default withRouter(Nav);
