import React from 'react';
import PropTypes from 'prop-types';
import Nav from './nav';
import Footer from './footer';
import glamorous from 'glamorous';
//import background from '../assets/images/symphony.png';

const StyledLayout = glamorous.div({
  // sticky footer -> https://philipwalton.com/articles/normalizing-cross-browser-flexbox-bugs/
  display: 'flex',
  // careful to borders, they don't count in height and introduce pesky scrollbars
  // hence the 4 pixel (without the subtract, an empty page have a scollbar)
  height: 'calc(100vh - 4px)',
  flexDirection: 'column',

  // fixed nav
  paddingTop: 60,
});

const StyledContent = glamorous.div({
  padding: 15,
  //backgroundColor: '#F4F7F7',
  //backgroundImage: `url(${background})`,

  // sticky footer -> https://philipwalton.com/articles/normalizing-cross-browser-flexbox-bugs/
  flex: '1 0 auto',
});

const Layout = ({ children, routes }) => {
  return (
    <StyledLayout className="layout">
      <Nav routes={routes} />
      <StyledContent className="content">{React.Children.toArray(children)}</StyledContent>
      <Footer />
    </StyledLayout>
  );
};

Layout.propTypes = {
  children: PropTypes.node,
  routes: PropTypes.array,
};

export default Layout;
