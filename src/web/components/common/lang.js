import React from 'react';
import PropTypes from 'prop-types';
import { withLocale } from '../../modules/i18n';
import * as R from 'ramda';
import { FormattedMessage } from '../../modules/i18n';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { withStyles } from '@material-ui/core/styles';
import messages from '../messages';

const styles = () => ({
  btn: {
    textTransform: 'none',
    fontSize: 12,
  },
});

class Lang extends React.Component {
  state = {
    anchorEl: null,
  };

  handleOpen = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  handleItem = id => {
    this.handleClose();
    this.props.changeLocale(id);
  };

  render = () => {
    const { anchorEl } = this.state;

    return (
      <div>
        <Button
          aria-owns={anchorEl ? 'locales-menu' : undefined}
          aria-haspopup="true"
          onClick={this.handleOpen}
          color="inherit"
          size="small"
          className={this.props.classes.btn}
        >
          <FormattedMessage {...R.propOr({ id: this.props.locale }, this.props.locale, messages)} />
        </Button>
        <Menu
          id="locales-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          {R.map(
            ({ id }) => (
              <MenuItem key={id} onClick={() => this.handleItem(id)}>
                <FormattedMessage {...R.propOr({ id }, id, messages)} />
              </MenuItem>
            ),
            this.props.availableLocales,
          )}
        </Menu>
      </div>
    );
  };
}

Lang.propTypes = {
  availableLocales: PropTypes.array,
  locale: PropTypes.string,
  changeLocale: PropTypes.func,
};

export default withStyles(styles)(withLocale(Lang));
