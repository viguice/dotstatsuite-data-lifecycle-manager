import React from 'react';
import PropTypes from 'prop-types';
import { withLocale } from '../../modules/i18n';
import { FormattedMessage } from '../../modules/i18n';
import { onlyUpdateForKeys, compose } from 'recompose';

const Locale = ({ locale }) => {
  return (
    <span>
      <FormattedMessage id="locale.current" />: {locale}
    </span>
  );
};

Locale.propTypes = {
  locale: PropTypes.string,
};

export default compose(onlyUpdateForKeys(['locale']), withLocale)(Locale);
