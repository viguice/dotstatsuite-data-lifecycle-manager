import React from 'react';
import PropTypes from 'prop-types';
import { map } from 'ramda';
import { NavLink } from 'react-router-dom';
import { Classes } from '@blueprintjs/core';
import Hidden from '@material-ui/core/Hidden';
import classnames from 'classnames';
import { FormattedMessage } from '../../modules/i18n';
import glamorous from 'glamorous';
import messages from '../messages';
import { Tooltip } from '@sis-cc/dotstatsuite-visions';

const StyledNavLink = glamorous(NavLink)({
  marginLeft: 5,
  marginRight: 5,
  color: 'white !important',
  '&.pt-active': {
    color: 'white !important',
    background: 'rgba(255, 255, 255, 0.2) !important',
  },
  '::before': {
    color: 'white !important',
  },
  '&.pt-button': {
    fontSize: 12,
  },
});

const Menu = ({ routes }) => {
  return (
    <div className={classnames('menu')}>
      {map(({ name, path, translationKey, disabled, icon }) => {
        const link = (
          <StyledNavLink
            to={path}
            activeClassName={Classes.ACTIVE}
            exact
            className={classnames(Classes.BUTTON, Classes.MINIMAL, Classes.iconClass(icon), {
              [Classes.DISABLED]: !!disabled,
            })}
          >
            <Hidden mdDown>
              {translationKey ? <FormattedMessage {...messages[translationKey]} /> : null}
            </Hidden>
          </StyledNavLink>
        );
        return (
          <React.Fragment key={name}>
            <Hidden lgUp>
              <Tooltip
                interactive
                placement="bottom"
                title={translationKey ? <FormattedMessage {...messages[translationKey]} /> : ''}
              >
                {link}
              </Tooltip>
            </Hidden>
            <Hidden mdDown>{link}</Hidden>
          </React.Fragment>
        );
      }, routes)}
    </div>
  );
};

Menu.propTypes = {
  routes: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      path: PropTypes.string.isRequired,
      icon: PropTypes.string,
      disabled: PropTypes.bool,
      translationKey: PropTypes.string,
    }),
  ),
};

export default Menu;
