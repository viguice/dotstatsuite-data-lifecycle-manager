import React from 'react';
import { useIntl } from 'react-intl';
import { useHistory } from 'react-router-dom';
import { Button, Classes, IconClasses, Popover, Position, NonIdealState } from '@blueprintjs/core';
import glamorous from 'glamorous';
import { formatMessage } from '../../modules/i18n';
import messages from '../messages';
import { useOidc } from '../../lib/oidc';
import { getUser } from '../../modules/oidc/selectors';
import { useSelector } from 'react-redux';

const NonIdealStateStyled = glamorous(NonIdealState)({
  padding: 15,
});

const ButtonStyled = glamorous(Button)({
  color: 'white !important',
  '&.pt-active': {
    color: 'white !important',
    background: 'rgba(255, 255, 255, 0.2) !important',
  },
  '::before': {
    color: 'white !important',
  },
  '&.pt-button': {
    fontSize: 12,
  },
});

const Me = () => {
  const intl = useIntl();
  const auth = useOidc();
  const user = useSelector(getUser);
  const history = useHistory();

  if (!user) {
    return (
      <ButtonStyled
        iconName={IconClasses.PERSON}
        text={formatMessage(intl)(messages['users.none'])}
        className={Classes.MINIMAL}
      />
    );
  }

  const handleMyPermissionsClick = () => {
    history.push('my-rights');
  };

  const userName = `${user.given_name} ${user.family_name}`;
  return (
    <Popover
      target={
        <ButtonStyled iconName={IconClasses.PERSON} text={userName} className={Classes.MINIMAL} />
      }
      content={
        <NonIdealStateStyled
          description={
            <div>
              <p>{user.email}</p>
              <Button iconName="shield" onClick={handleMyPermissionsClick}>
                <span>{formatMessage(intl)(messages['route.my.rights'])}</span>
              </Button>
              <Button iconName="log-out" onClick={() => auth.signOut()}>
                <span>{formatMessage(intl)(messages['logout'])}</span>
              </Button>
            </div>
          }
        />
      }
      position={Position.BOTTOM}
      inheritDarkTheme={false}
    />
  );
};

export default Me;
