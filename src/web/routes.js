import { compose, reduce, toPairs, find, filter, prop } from 'ramda';
import ManageStructure from './pages/manage-structure';
import UploadArtefact from './pages/upload-artefact';
import UploadData from './pages/upload-data';
import Dump from './pages/dump';
import AllRightsList from './pages/all-rights';
import MyRightsList from './pages/my-rights';
import Logs from './pages/logs';

const routes = {
  manageStructure: {
    path: '/',
    component: ManageStructure,
    exact: true,
    default: true,
    name: 'home',
    icon: 'home',
  },
  uploadArtefact: {
    path: '/upload-artefact',
    component: UploadArtefact,
    exact: true,
    name: 'uploadArtefact',
    icon: 'folder-shared',
    translationKey: 'route.upload.structure',
  },
  uploadData: {
    path: '/upload-data',
    component: UploadData,
    exact: true,
    name: 'uploadData',
    icon: 'document-share',
    translationKey: 'route.upload.data',
  },
  logs: {
    path: '/logs',
    component: Logs,
    exact: true,
    name: 'logs',
    icon: 'timeline-events',
    translationKey: 'route.logs',
  },
  allRights: {
    path: '/all-rights',
    component: AllRightsList,
    exact: true,
    name: 'allRights',
    icon: 'shield',
    translationKey: 'route.all.rights',
  },
  dumpDownload: {
    path: '/dump',
    component: Dump,
    exact: true,
    name: 'dumpDownload',
    icon: 'cloud-download',
    translationKey: 'route.dump',
  },
  myRights: {
    path: '/my-rights',
    component: MyRightsList,
    exact: true,
    name: 'myRights',
    icon: 'shield',
    translationKey: 'route.my.rights',
  },
};

const exportedRoutes = compose(
  reduce((acc, [name, r]) => [...acc, { ...r, name }], []),
  toPairs,
  filter(prop('path')),
)(routes);

export const getDefault = () => find(route => route.default, exportedRoutes);

export const getRouteByName = name => routes[name] && { ...routes[name], name };

export const getPathByName = (name, param) => {
  const path = prop('path', getRouteByName(name));
  return param ? `${path.replace(':id', param)}` : path;
};

export default {
  getRoutes: () => exportedRoutes,
  getDefault,
  getRouteByName,
  getPathByName,
};
