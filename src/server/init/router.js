const helmet = require('helmet');
import cors from 'cors';
import compression from 'compression';
import { crypto } from 'jsrsasign';
import path from 'path';
import express from 'express';
import bodyParser from 'body-parser';
import { healthcheckConnector } from '../services/healthcheck/connector';
import apiConnector from '../services/api/connector';
import errorHandler from '../middlewares/errors';
import tenant from '../middlewares/tenant';
import helmetCSP from '../middlewares/helmet-csp';
import ssr from '../ssr';
import { HTTPError } from '../utils/errors';

const isTest = process.env.NODE_ENV === 'test';

const checkTenant = (req, _, next) => {
  if (!req.member) return next(new HTTPError(400, 'Tenant required'));
  next();
};

const init = ctx => {
  const app = express();
  app.disable('x-powered-by');

  const {
    services: { healthcheck, api },
    configProvider,
  } = ctx;

  app.use(cors());
  app.use(bodyParser.json({ limit: '50mb' }));
  app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
  app.get('/robots.txt', (_, res) =>
    res.sendFile(path.resolve(__dirname, '../robots.txt')),
  );
  app.use(compression());
  app.use(express.static(path.join(__dirname, '../../../public')));
  app.use(express.static(path.join(__dirname, '../../../build')));
  app.use(tenant(configProvider));
  app.use((_, res, next) => {
    res.locals.cspNonce = crypto.Util.getRandomHexOfNbytes(16).toString('hex');
    next();
  });
  if (!isTest) {
    app.use(helmet({ crossOriginOpenerPolicy: false }));
    app.use(helmetCSP(configProvider));
  }
  app.use((_, res, next) => {
    res.setHeader(
      'Permissions-Policy',
      'camera=(), fullscreen=(), microphone=(), payment=()',
    );
    next();
  });
  app.get('/api/healthcheck', healthcheckConnector(healthcheck));
  app.use('/api', apiConnector(api));
  app.use(checkTenant, ssr(ctx));
  app.use(errorHandler);

  return Promise.resolve({ ...ctx, app });
};

module.exports = init;
