const errorHandler = require('../errors');
const { HTTPError } = require('../../utils/errors');

describe('errors middleware', () => {
  describe('when called without an error', () => {
    it('should call next callback', () => {
      const next = jest.fn();
      errorHandler(undefined, undefined, undefined, next);
      expect(next).toHaveBeenCalled();
    });
  });

  describe('when called with an error', () => {
    it('should send an error', () => {
      let sentCode;
      const CODE = 42;
      const res = {
        status: code => {
          sentCode = code;
          return { json: () => {} };
        },
      };
      errorHandler(new HTTPError(CODE), undefined, res);
      expect(sentCode).toEqual(CODE);
    });
  });
});
