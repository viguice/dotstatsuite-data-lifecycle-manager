import { omit, propOr, map, pipe, values, pluck, indexBy, identity, has } from 'ramda';
import debug from '../debug';

const getScopedMember = (tenant, scopeId) => {
  const member = omit(['spaces', 'datasources', 'scopes'], tenant);
  const indexedSpaceIds = pipe(
    values,
    pluck('dataSpaceId'),
    indexBy(identity),
  )(tenant.datasources || {});
  const scope = tenant?.scopes?.[scopeId];
  if (!scope) return member;
  member.scope = omit(['spaces'], scope);
  member.scope.id = scopeId;

  member.scope.spaces = [];
  member.scope.spaces = map(scopedSpace => {
    const space = tenant.spaces?.[scopedSpace.id];
    if (!space) return space;
    return { ...space, ...scopedSpace, indexed: has(scopedSpace.id, indexedSpaceIds) };
  }, propOr([], 'spaces', scope));

  return member;
};

const getTenant = (configProvider, defaultTenant) => async (req, res, next) => {
  try {
    const t0 = new Date();
    const tenantSlug = req.query.tenant || req.headers['x-tenant'];
    const [tenantId, scopeId = 'dlm'] = tenantSlug ? tenantSlug.split(':') : [null, 'dlm'];
    const tenant = await configProvider.getTenant(tenantId);
    if (!tenant) return next();
    req.member = getScopedMember(tenant, scopeId);
    if (tenantSlug)
      debug.info(
        `load member '${tenant?.id}' with scope '${req.member?.scope?.id}' in ${new Date() -
          t0} ms`,
      );
    else if (tenant)
      debug.info(
        `load default member '${tenant?.id}' with scope '${req.member?.scope?.id}' in ${new Date() -
          t0} ms`,
      );
    next();
  } catch (err) {
    next(err);
  }
};

module.exports = getTenant;
