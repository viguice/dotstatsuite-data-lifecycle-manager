import initHealthcheck from './healthcheck';
import initApi from './api';
import { initResources } from '../utils';

const ressources = [initHealthcheck, initApi];

export default initResources(ressources);
