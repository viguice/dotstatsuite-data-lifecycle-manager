import evtX from 'evtx';
import initSfs from './sfs';
import { initServices } from '../../utils';
import { getUser } from './utils';
import debug from '../../debug';

const services = [initSfs];

const init = ctx => {
  const api = evtX(ctx)
    .configure(initServices(services))
    .before(getUser);
  debug.info('api services up');
  return Promise.resolve({ ...ctx, services: { ...ctx.services, api } });
};

export default init;
