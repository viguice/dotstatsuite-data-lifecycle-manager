import * as R from 'ramda';

const prepareTags = path => {
  return R.pipe(
    R.path(path),
    R.ifElse(R.is(Array), R.identity, R.always([])),
    R.without(['*', "'unsafe-eval'"]),
  );
};

const extractDomainsFromTags = R.map(domain => {
  const url = new URL(domain);
  return `${url.protocol}//${url.host}`;
});

export const getDomains = ({ path, keepTag } = {}) => {
  return R.pipe(
    prepareTags(path),
    R.ifElse(R.always(keepTag), R.identity, extractDomainsFromTags),
    R.uniq,
  );
};

const getAllDomains = ({ key, keepTag } = {}) => (
  settings = {},
  scope = {},
) => {
  return R.uniq(
    R.concat(
      getDomains({ path: ['app', key], keepTag })(settings),
      getDomains({ path: [key], keepTag })(scope),
    ),
  );
};

export const getScripts = getAllDomains({ key: 'scriptTags' });
export const getScriptsTags = getAllDomains({
  key: 'scriptTags',
  keepTag: true,
});
export const getLinks = getAllDomains({ key: 'linkTags' });
export const getLinkTags = getAllDomains({ key: 'linkTags', keepTag: true });
