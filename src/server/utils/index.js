import { reduce } from 'ramda';

export const initResources = resources => ctx =>
  reduce((acc, initFn) => acc.then(initFn), Promise.resolve(ctx), resources);
export const initServices = services => evtx =>
  reduce((acc, service) => acc.configure(service), evtx, services);
